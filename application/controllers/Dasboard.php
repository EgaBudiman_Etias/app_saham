<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dasboard extends MX_Controller {

	public function index()
	{
		$portofoliosaham=$this->db->query("
			SELECT 'total' AS total,COALESCE(SUM(saldo_saham),0) AS saldo_saham,COALESCE(SUM(saldo_saham2),0) AS saldo_saham2 FROM (
				(SELECT 'saldo-awal' AS ket,COALESCE(SUM(shal_rata_rata_harga* shal_jumlah_lot*100),0) AS saldo_saham,'0' AS saldo_saham2
				FROM saldo_awal_saham
				JOIN saham ON sham_id=shal_sham_id 
				JOIN akun ON akun_id=shal_akun_id 
				JOIN sekuritas ON akun_seku_id=seku_id
				WHERE akun_is_deleted='1' and shal_is_deleted='1'
				)
				UNION	
				(SELECT	'pembelian' AS ket ,COALESCE(sum(dbli_netto),0) as saldo_saham,COALESCE(SUM(pmbl_grand_tot)) AS saldo_saham2
				FROM pembelian
				JOIN det_pembelian on pmbl_id=dbli_pmbl_id
				WHERE pmbl_is_deleted='1'
				GROUP BY dbli_pmbl_id)
							
				UNION		
				(SELECT	'penjualan' AS ket ,COALESCE(sum(dpjl_netto)*-1,0) as saldo_saham,COALESCE(SUM(pnjl_grand_tot)) AS saldo_saham2
				FROM penjualan	
				JOIN det_penjualan on dpjl_pnjl_id=pnjl_id
				WHERE pnjl_is_deleted='1'
				Group by dpjl_pnjl_id
				)
			) AS td
		")->row();
		
		$saldokas=$this->db->query("
			SELECT seku_nama,akun_no_sid,
			coalesce(nominal_awal,0) as awal,
			coalesce(nominal_setoran,0) as setoran,
			coalesce(nominal_penarikan,0)as penarikan,
			coalesce(nominal_pembelian,0) as pembelian,
			coalesce(nominal_penjualan,0) as penjualan,
			coalesce(nominal_deviden,0) as deviden
			FROM akun	
			JOIN sekuritas ON akun_seku_id = seku_id
			LEFT JOIN (
				SELECT
						COALESCE(SUM(swal_jumlah_saldo),
						0) AS nominal_awal,
						swal_akun_id AS akun_awal,
						'saldo awal' AS uang_jns_transaksi
					FROM
						saldo_awal
					
					WHERE
						swal_is_deleted = '1'
					Group by swal_akun_id
			) AS awal ON awal.akun_awal=akun_id
			LEFT JOIN (
				SELECT
					COALESCE(SUM(uang_nominal),
					0) AS nominal_setoran,
					uang_akun_id AS akun_setoran,
					uang_jns_transaksi 
				FROM
					keuangan
				JOIN setoran ON uang_transaksi_id = stor_id
				WHERE
					uang_jns_transaksi = 'setoran' AND stor_is_deleted = '1'
				Group by uang_akun_id
			) AS setoran ON setoran.akun_setoran=akun_id
			LEFT JOIN(
				SELECT
					COALESCE(SUM(uang_nominal),
					0) AS nominal_penarikan,
					uang_akun_id,
					uang_jns_transaksi
				FROM
					keuangan
				JOIN penarikan ON uang_transaksi_id = trik_id
				WHERE
					uang_jns_transaksi = 'penarikan' AND trik_is_deleted = '1'
				Group by uang_akun_id
			) AS penarikan ON penarikan.uang_akun_id=akun_id
			LEFT JOIN(
				SELECT
					COALESCE(SUM(uang_nominal),
					0) AS nominal_pembelian,
					uang_akun_id,
					uang_jns_transaksi
				FROM
					keuangan
				JOIN pembelian ON uang_transaksi_id = pmbl_id
				WHERE
					uang_jns_transaksi = 'pembelian' AND pmbl_is_deleted = '1'
				Group by uang_akun_id
			) AS pembelian ON pembelian.uang_akun_id=akun_id

			LEFT JOIN(
			SELECT
					COALESCE(SUM(uang_nominal),
					0) AS nominal_penjualan,
					uang_akun_id,
					uang_jns_transaksi
				FROM
					keuangan
				JOIN penjualan ON uang_transaksi_id = pnjl_id
				WHERE
					uang_jns_transaksi = 'penjualan' AND pnjl_is_deleted = '1'
				Group by uang_akun_id
			) AS penjualan ON penjualan.uang_akun_id=akun_id
			LEFT JOIN(
				SELECT
					COALESCE(SUM(uang_nominal),
					0) AS nominal_deviden,
					uang_akun_id,
					uang_jns_transaksi
				FROM
					keuangan
				JOIN deviden ON uang_transaksi_id = devi_id
				WHERE
					uang_jns_transaksi = 'deviden' AND devi_is_deleted = '1'
				Group by uang_akun_id
			) AS deviden ON deviden.uang_akun_id=akun_id
			WHERE akun_is_deleted='1'
		
		")->result();
		$totalsaldokas=0;
		foreach($saldokas as $s){
			$totalsaldokas+=$s->awal+$s->setoran+$s->penarikan+$s->pembelian+$s->penjualan+$s->deviden;
		}
		
		$akun=$this->db->query("SELECT * FROM akun JOIN sekuritas on akun_seku_id=seku_id where akun_is_deleted='1'")->result();
		$hasil=array();
		$hasilpembelian=array();
		$hasilpenjualan=array();
		foreach($akun as $a){
			$tranakun=$this->db->query("
				SELECT dt.*,sham_kode FROM (
					(SELECT '' as id,'1.saldo-awal' AS ket,shal_tgl_saldo_awal AS tgl,shal_akun_id AS akun,shal_sham_id AS saham,shal_jumlah_lot AS lot,shal_rata_rata_harga AS harga,shal_total AS netto
					FROM saldo_awal_saham
					JOIN akun ON shal_akun_id=akun_id
					WHERE shal_akun_id='$a->akun_id' AND akun_is_deleted='1'
					)
					UNION
					(SELECT pmbl_id,'2.pembelian',pmbl_tgl_beli,pmbl_akun_id,dbli_sham_id,dbli_jumlah_lot,dbli_harga_shares,dbli_netto
					FROM det_pembelian
					JOIN pembelian ON dbli_pmbl_id=pmbl_id
					WHERE pmbl_is_deleted='1' AND pmbl_akun_id='$a->akun_id'
					)
					UNION
					(SELECT pnjl_id,'3.penjualan',pnjl_tgl_jual,pnjl_akun_id,dpjl_sham_id,dpjl_jumlah_lot,dpjl_harga_shares,dpjl_netto
					FROM det_penjualan
					JOIN penjualan ON dpjl_pnjl_id=pnjl_id
					WHERE pnjl_is_deleted='1' AND pnjl_akun_id='$a->akun_id'
					)
				) AS dt
				JOIN saham on saham=sham_id
				ORDER BY saham,tgl,ket ASC

			")->result();
			$harga_rata_rata=0;
			$jumlah_lot=0;
			$netto=0;
			$saham_sebelumnya="";
			$saham_skr="";
			$gl=0;
			$totalgl=0;
			$n=1;
			foreach($tranakun as $t){
				if($n==1){
					$saham_skr=$t->saham;
					$saham_sebelumnya=$t->saham;
				}else{
					$saham_sebelumnya=$t->saham;
					$saham_skr=$t->saham;
				}
				if($saham_skr==$saham_sebelumnya){
					
					if($t->ket!='3.penjualan'){
						$jumlah_lot+=$t->lot;
						$netto+=$t->netto;
						// echo $t->ket.'-'.$t->akun.'-'.$t->saham.'-'.$netto."<br>";
						if($jumlah_lot!=0){
							$harga_rata_rata=$netto/($jumlah_lot*100);
							$harga_rata_rata=round($harga_rata_rata,2);
						}
						else{
							$harga_rata_rata=0;
						}
						
						$gl=0;
						if($t->ket=='2.pembelian'){
							$hasilpembelian[]=array(
								'ket'=>$t->ket,
								'tgl'=>$t->tgl,
								'akun'=>$t->akun,
								'saham'=>$t->saham,
								'saham_kode'=>$t->sham_kode,
								'lot'=>$t->lot,
								'harga'=>$t->harga,
								'netto'=>$t->netto,
								'harga_rata_rata'=>$harga_rata_rata,
								'gl'=>$gl,
							);
						}
					}
					else{
						$gl=($t->netto)-($harga_rata_rata*$t->lot*100);
						$hasilpenjualan[]=array(
							'ket'=>$t->ket,
							'tgl'=>$t->tgl,
							'akun'=>$t->akun,
							'saham'=>$t->saham,
							'saham_kode'=>$t->sham_kode,
							'lot'=>$t->lot,
							'harga'=>$t->harga,
							'netto'=>$t->netto,
							'harga_rata_rata'=>$harga_rata_rata,
							'gl'=>$gl,
						);
					}
					$hasil[]=array(
						'ket'=>$t->ket,
						'tgl'=>$t->tgl,
						'akun'=>$t->akun,
						'saham'=>$t->saham,
						'saham_kode'=>$t->sham_kode,
						'lot'=>$t->lot,
						'harga'=>$t->harga,
						'netto'=>$t->netto,
						'harga_rata_rata'=>$harga_rata_rata,
						'gl'=>$gl,
					);
				}else{
					$harga_rata_rata=0;
					$jumlah_lot=0;
					$netto=0;
					$saham_sebelumnya="";
					$saham_skr="";
					$gl=0;
				}
				$saham_sebelumnya=$saham_skr;
				$totalgl+=$gl;
				$n++;
			};
		};
		
		$hasil2=array();
		foreach($akun as $ha){
			
			$sahamperakun=$this->db->query("
				SELECT dt.*,sham_kode,COALESCE(SUM(netto),0) AS total FROM (
					(SELECT '1.saldo-awal' AS ket,shal_tgl_saldo_awal AS tgl,shal_akun_id AS akun,shal_sham_id AS saham,shal_jumlah_lot AS lot,shal_rata_rata_harga AS harga,shal_total AS netto
					FROM saldo_awal_saham
					JOIN akun ON shal_akun_id=akun_id
					WHERE shal_akun_id='$ha->akun_id' AND akun_is_deleted='1'
					)
					UNION
					(SELECT '2.pembelian',pmbl_tgl_beli,pmbl_akun_id,dbli_sham_id,dbli_jumlah_lot,dbli_harga_shares,dbli_netto
					FROM det_pembelian
					JOIN pembelian ON dbli_pmbl_id=pmbl_id
					WHERE pmbl_is_deleted='1' AND pmbl_akun_id='$ha->akun_id'
					)
					UNION
					(SELECT '3.penjualan',pnjl_tgl_jual,pnjl_akun_id,dpjl_sham_id,dpjl_jumlah_lot,dpjl_harga_shares,dpjl_netto*-1
					FROM det_penjualan
					JOIN penjualan ON dpjl_pnjl_id=pnjl_id
					WHERE pnjl_is_deleted='1' AND pnjl_akun_id='$ha->akun_id'
					)
				) AS dt
				JOIN saham ON sham_id=saham
				GROUP BY saham
				ORDER BY saham,tgl,ket ASC
			")->result();
			$hasil2[]=array(
				'seku_nama'=>$ha->seku_nama,
				'akun_no_sid'=>$ha->akun_no_sid,
				'akun_kode_nasabah'=>$ha->akun_kode_nasabah,
				'saham'=>$sahamperakun,
			);
		}
		$tglskr=date('Y-m-d');
        $tglblnlalu = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-1 month" ) );
		$penjualan30=$this->db->query("
			SELECT * FROM keuangan_saham 
			JOIN penjualan on kshm_transaksi_id=pnjl_id
			JOIN saham on kshm_sham_id=sham_id
			where kshm_tgl_transaksi>='".$tglblnlalu."' and kshm_tgl_transaksi<='".$tglskr."' and kshm_jns_transaksi='penjualan' and pnjl_is_deleted='1';
		");
		$totalgainloss=$this->db->query("
			SELECT coalesce(sum(kshm_gl),0) as total FROM keuangan_saham 
			JOIN penjualan on kshm_transaksi_id=pnjl_id
			JOIN saham on kshm_sham_id=sham_id
			where kshm_jns_transaksi='penjualan' and pnjl_is_deleted='1';
		")->row()->total;
		$this->load->view('templates/site_tpl', array (
			'content' => 'dashboard',
			'portofoliosaham'=>rupiah2($portofoliosaham->saldo_saham),
			'saldokas'=>rupiah2($totalsaldokas),
			'gainlos'=>rupiah2($totalgainloss),
			'akunsaham'=>$hasil2,
			'penjualan'=>$penjualan30->result(),
			'pembelian'=>$hasilpembelian,
			'saldokasakun'=>$saldokas,
			'all'=>$hasil,
		));
	}
	
}
