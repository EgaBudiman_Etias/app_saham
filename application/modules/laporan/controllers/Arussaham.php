<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Arussaham extends MX_Controller {

	public function index()
	{
		$this->load->view('templates/site_tpl', array (
			'content' => 'arus_saham_index',
		));
	}

	public function datatable($akun,$saham,$dari,$hingga)
	{
		
		$data=$this->getData2($akun,$saham,$dari,$hingga);
        $response=array(
            'aaData'=>$data,
           
        );
        echo json_encode($response);
	}

	public function getData(){
        $akun_id=$this->input->post('akun_id');
		$saham_id=$this->input->post('saham_id');
        $dari_tanggal=$this->input->post('dari_tanggal');
        $hingga_tanggal=$this->input->post('hingga_tanggal');
        $hasil=$this->getData2($akun_id,$saham_id,$dari_tanggal,$hingga_tanggal);
		$n=1;
		$saldosblm=0;
		$saldomutasi=0;
		
        foreach ($hasil as $h) {
			if($n==1){
				
				$saldosblm=$h['total_awal'];
			}
			$saldomutasi+=$h['jumlah_lot2']*$h['harga2']*100;
		}
		$saldoakhir=$saldosblm+$saldomutasi;
		$hasil=array(
			'saldoawal'=>rupiah2($saldosblm),
			'saldomutasi'=>rupiah2($saldomutasi),
			'saldoakhir'=>rupiah2($saldoakhir),
		);
        echo json_encode($hasil);
    }

	public function getData2($akun,$saham,$dari,$hingga){
		$qsaldoawalsblm="
				SELECT *,coalesce(sum(kshm_jumlah),0) as total_lot,coalesce(sum(kshm_harga_netto),0) as total_netto FROM(
					(SELECT '1' as urut,kshm_tgl_transaksi,kshm_sham_id,kshm_akun_id,
					kshm_jns_transaksi,kshm_transaksi_id,
					kshm_jumlah,kshm_jumlah*100 as kshm_shares,
					kshm_harga_rata_rata,kshm_harga_netto
					FROM keuangan_saham 
					JOIN akun on akun_id=kshm_akun_id
					WHERE akun_is_deleted='1' and kshm_akun_id='".$akun."' and kshm_sham_id='".$saham."' and kshm_jns_transaksi ='saldoawal')

					UNION

					(SELECT '2',kshm_tgl_transaksi,kshm_sham_id,kshm_akun_id,
					kshm_jns_transaksi,kshm_transaksi_id,
					kshm_jumlah,kshm_jumlah*100 as kshm_shares,
					kshm_harga_rata_rata,kshm_harga_netto
					FROM keuangan_saham 
					JOIN pembelian on (pmbl_akun_id=kshm_akun_id AND pmbl_id=kshm_transaksi_id)
					WHERE pmbl_is_deleted='1' and kshm_akun_id='".$akun."' and kshm_sham_id='".$saham."' and kshm_jns_transaksi ='pembelian' and kshm_tgl_transaksi<'".$dari."')

					UNION

					(SELECT '3',kshm_tgl_transaksi,kshm_sham_id,kshm_akun_id,
					kshm_jns_transaksi,kshm_transaksi_id,
					kshm_jumlah,kshm_jumlah*100 as kshm_shares,
					kshm_harga_rata_rata,kshm_harga_netto
					FROM keuangan_saham 
					JOIN penjualan on (pnjl_akun_id=kshm_akun_id AND pnjl_id=kshm_transaksi_id)
					WHERE pnjl_is_deleted='1' and kshm_akun_id='".$akun."' and kshm_sham_id='".$saham."' and kshm_jns_transaksi ='penjualan' and kshm_tgl_transaksi<'".$dari."')
					
				) as dt order by kshm_tgl_transaksi,urut
				";
		$awal=$this->db->query($qsaldoawalsblm)->row();
		$hasil=array();
		$total_lot=$awal->total_lot;
		$total_netto=$awal->total_netto;
		$netto_awal=$awal->total_netto;
		$harga_rataan=0;
		if($total_lot!=0){
			$harga_rataan=$total_netto/($total_lot*100);
		}
		$hasil[]=array(
			'no'=>'1',
			'tanggal'=>$dari,
			'uraian'=>'Saldo Awal',
			'jumlah_lot'=>angka($awal->total_lot),
			'jumlah_shares'=>angka($awal->total_lot*100),
			'saldo_lot'=>angka($awal->total_lot),
			'saldo_shares'=>angka($awal->total_lot*100),
			'harga'=>rupiah3($harga_rataan),
			'harga_net'=>rupiah3($awal->total_netto),
			'saldo_saham'=>rupiah3($total_netto),
			'avg_before'=>'Rp0',
			'avg_after'=>rupiah3($harga_rataan),

			'jumlah_lot2'=>$awal->total_lot,
			'jumlah_shares2'=>$awal->total_lot*100,
			'saldo_lot2'=>$total_lot,
			'saldo_shares2'=>$total_lot*100,
			'harga2'=>$harga_rataan,
			'harga_net2'=>$awal->total_netto,
			'saldo_saham2'=>$total_netto,
			'avg_before2'=>0,
			'avg_after2'=>$harga_rataan,

			'total_awal'=>$total_netto,
			'tanda'=>"plus",
		);
		$qmutasi="
			SELECT * FROM(
					
					(SELECT '2' as urut,kshm_tgl_transaksi,kshm_sham_id,kshm_akun_id,
					kshm_jns_transaksi,kshm_transaksi_id,
					kshm_jumlah,kshm_jumlah*100 as kshm_shares,
					kshm_harga_rata_rata,kshm_harga_netto
					FROM keuangan_saham 
					JOIN pembelian on (pmbl_akun_id=kshm_akun_id AND pmbl_id=kshm_transaksi_id)
					WHERE pmbl_is_deleted='1' and kshm_sham_id='".$saham."' and kshm_jns_transaksi ='pembelian' and kshm_tgl_transaksi>'".$dari."' and kshm_tgl_transaksi<'".$hingga."')

					UNION

					(SELECT '3',kshm_tgl_transaksi,kshm_sham_id,kshm_akun_id,
					kshm_jns_transaksi,kshm_transaksi_id,
					kshm_jumlah,kshm_jumlah*100 as kshm_shares,
					kshm_harga_rata_rata,kshm_harga_netto
					FROM keuangan_saham 
					JOIN penjualan on (pnjl_akun_id=kshm_akun_id AND pnjl_id=kshm_transaksi_id)
					WHERE pnjl_is_deleted='1' and kshm_sham_id='".$saham."' and kshm_jns_transaksi ='penjualan' and kshm_tgl_transaksi>'".$dari."' and kshm_tgl_transaksi<'".$hingga."')
					
				) as dt order by kshm_tgl_transaksi,urut
		";
		$mutasi=$this->db->query($qmutasi)->result();
		$no=2;
		
		$harga_rata_rata=$harga_rataan;
		$jumlah_lot=0;
		$netto=0;
		$saham_sebelumnya="";
		$saham_skr="";
		$gl=0;
		$totalgl=0;
		$harga_rata_rata_after=0;
		$tanda="";
		$harga_net=0;
		foreach ($mutasi as $h) {
			//2=pembelian
			if($h->urut=='2'){
				$total_lot+=$h->kshm_jumlah;
				$total_netto+=$h->kshm_harga_netto;
				if($total_lot!=0){
					$harga_rata_rata_after=$total_netto/($total_lot*100);
					$harga_rata_rata_after=round($harga_rata_rata_after,2);
				}
				$harga_net=$h->kshm_harga_netto;
			}
			if($h->urut=='3'){
				$total_lot+=$h->kshm_jumlah;
				$total_netto+=$harga_rata_rata*$h->kshm_jumlah*100;
				// $total_netto+=$h->kshm_harga_netto;
				$harga_net=$harga_rata_rata*$h->kshm_jumlah*100;
				$harga_rata_rata_after=$harga_rata_rata;
			}
			if($h->kshm_harga_netto<0){
				$tanda="min";
			}else{
				$tanda="plus";
			}
			if($harga_net<0){
				$harga='('.rupiah2($harga_net*-1).')';
			}else{
				$harga=rupiah2($harga_net);
			}
			if($h->kshm_jumlah<0){
				$jumlah_lot='('.angka($h->kshm_jumlah*-1).')';
				$jumlah_shares='('.angka($h->kshm_jumlah*-100).')';
			}else{
				$jumlah_lot=angka($h->kshm_jumlah);
				$jumlah_shares=angka($h->kshm_jumlah*100);
			}
			$hasil[]=array(
				'no'=>$no,
				'tanggal'=>$h->kshm_tgl_transaksi,
				'uraian'=>$h->kshm_jns_transaksi,
				'jumlah_lot'=>$jumlah_lot,
				'jumlah_shares'=>$jumlah_shares,
				'saldo_lot'=>angka($total_lot),
				'saldo_shares'=>angka($total_lot*100),
				'harga'=>rupiah3($h->kshm_harga_rata_rata),
				'harga_net'=>$harga,
				'saldo_saham'=>rupiah3($total_netto),
				'avg_before'=>rupiah3($harga_rata_rata),
				'avg_after'=>rupiah3($harga_rata_rata_after),

				'jumlah_lot2'=>$h->kshm_jumlah,
				'jumlah_shares2'=>$h->kshm_jumlah*100,
				'saldo_lot2'=>$total_lot,
				'saldo_shares2'=>$total_lot*100,
				'harga2'=>$h->kshm_harga_rata_rata,
				'harga_net2'=>$harga_net,
				'saldo_saham2'=>$total_netto,
				'avg_before2'=>$harga_rata_rata,
				'avg_after2'=>$harga_rata_rata_after,

				'total_awal'=>$netto_awal,
				'tanda'=>$tanda,
			);
			$harga_rata_rata=$harga_rata_rata_after;
			$no++;
		}
		return $hasil;
        
	}
}