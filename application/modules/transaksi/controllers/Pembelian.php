<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembelian extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->form_validation->CI =& $this;
	}
	public function index()
	{
		$this->load->view('templates/site_tpl', array (
			'content' => 'pembelian_index',
			
		));
	}
	
	public function datatable()
	{
		$draw = $this->input->post('draw');
		$offset = $this->input->post('start');
		$num_rows = $this->input->post('length');
		$order_index = $_POST['order'][0]['column'];
		$order_by = $_POST['columns'][$order_index]['data'];
		$order_direction = $_POST['order'][0]['dir'];
		$keyword = $_POST['search']['value'];
		$akun_id=$this->input->post('akun_id');
		$tgl=$this->input->post('tgl');

		$no_sid=$this->input->post('no_sid');
		$no_ksei=$this->input->post('no_ksei');
		$dari_pembelian=$this->input->post('dari_pembelian');
		$hingga_pembelian=$this->input->post('hingga_pembelian');

		$bindings = array("%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%");
		
		$base_sql = "
			from pembelian
			JOIN akun on(pmbl_akun_id=akun_id)
            JOIN sekuritas on(akun_seku_id=seku_id)
            where
				pmbl_is_deleted = '1'
				and (
					seku_nama like ?
                    or akun_no_sid like ?
					or pmbl_kode like ?
					or pmbl_tgl_beli like ?
					or akun_kode like ?
					or seku_nama like ?
					or akun_no_sid like ?
					or akun_no_ksei_kpei like ?
					or pmbl_grand_tot like ?
				)
		";
		if($akun_id!=""){
			$base_sql.=" AND pmbl_akun_id='$akun_id' ";
		};
		if($tgl!=""){
			$base_sql.=" AND pmbl_tgl_beli='$tgl' ";
		};
		if($no_sid!=""){
			$base_sql.=" AND akun_no_sid='$no_sid' ";
		};
		if($no_ksei!=""){
			$base_sql.=" AND akun_no_ksei_kpei='$no_ksei' ";
		};
		if($dari_pembelian!=""){
			$base_sql.=" AND pmbl_grand_tot>='$dari_pembelian' ";
		};
		
		if($hingga_pembelian!="" && $hingga_pembelian!="0"){
			$base_sql.=" AND pmbl_grand_tot<='$hingga_pembelian' ";
		};
		$data_sql = "
			select
				pembelian.*,seku_nama,akun_no_sid,akun_no_ksei_kpei,akun_kode
				, row_number() over (
					order by
						{$order_by} {$order_direction}
						, pmbl_id {$order_direction}
				  ) as nomor
			{$base_sql}
			order by
				{$order_by} {$order_direction}
				, pmbl_id {$order_direction}
			limit {$offset}, {$num_rows}
		";
                    
		$src = $this->db->query($data_sql, $bindings);
		// echo $this->db->last_query();
        // die();
		$count_sql = "
			select count(*) AS total
			{$base_sql}
		";
		$total_records = $this->db->query($count_sql, $bindings)->row('total');
		
		$data = array();
		
		foreach ($src->result() as $row) {
			$data[] = array (
				'seku_nama' => $row->seku_nama,
				'akun_no_sid' => $row->akun_no_sid,
				'akun_no_ksei_kpei' => $row->akun_no_ksei_kpei,
				'pmbl_akun_id' => $row->pmbl_akun_id,
				'pmbl_tgl_beli' => $row->pmbl_tgl_beli,
                'pmbl_grand_tot' => rupiah2($row->pmbl_grand_tot),
                'pmbl_id'=>$row->pmbl_id,
				'akun_kode'=>$row->akun_kode,
                'no'=>$row->nomor,
				'pmbl_kode'=>$row->pmbl_kode
			);
		}
		
		$response = array (
			'draw' => intval($draw),
			'iTotalRecords' => $src->num_rows(),
			'iTotalDisplayRecords' => $total_records,
			'aaData' => $data,
		);
		
		echo json_encode($response);
	}
	
	private function _form($aksi = 'tambah', $data = null)
	{
		if ($this->session->flashdata('data_form')) {
			$data = $this->session->flashdata('data_form');
		}
		
		$this->load->view('templates/site_tpl', array (
			'content' => 'pembelian_form',
			'url_aksi' => site_url("/transaksi/pembelian/{$aksi}-data"),
			'data' => $data,
			'aksi'=>$aksi,
		));
	}
	
	public function tambah()
	{
		$this->_form();
	}
	
	public function ubah($id = '')
	{
		$this->load->model('transaksi/pembelian_model');
		if ( ! $this->agent->referrer()) {
			show_404();
		}
		$src = $this->db
			->from('pembelian')
			->where('pmbl_id', $id)
			->get();
		
		if ($src->num_rows() == 0) {
			show_404();
		}
		
		$data = $src->row();
		
		$akun_id=$data->pmbl_akun_id;
		$data->saldo=$this->pembelian_model->carisaldoakun($akun_id)->nominal;
		$detail = $this->db
			->select('*')
			->from('det_pembelian')
			->join('pembelian','dbli_pmbl_id=pmbl_id')
			->join('saham ', 'dbli_sham_id = sham_id')
			->join('akun','pmbl_akun_id=akun_id')
			->join('sekuritas','akun_seku_id=seku_id')
			->where(array('dbli_pmbl_id'=> $id))
			->order_by('dbli_id')
			->get();
		
		$i = 0;
		
		foreach ($detail->result() as $p) {
			$data->saham['dbli_sham_id'][$i] = $p->dbli_sham_id;
			$data->saham['dbli_jumlah_lot'][$i] = $p->dbli_jumlah_lot;
			$data->saham['dbli_jumlah_shares'][$i] = $p->dbli_jumlah_shares;
			$data->saham['dbli_harga_shares'][$i] = $p->dbli_harga_shares;
			$data->saham['dbli_bruto'][$i] = $p->dbli_bruto;
			$data->saham['dbli_fee_pembelian_persen'][$i] = $p->seku_broker_fee_pembelian;
			$data->saham['dbli_fee_pembelian'][$i] = $p->dbli_fee_pembelian;
			$data->saham['dbli_netto'][$i] = $p->dbli_netto;
			$data->saham['dbli_jumlah_lot_saat_ini'][$i] = $this->pembelian_model->carisaldosaham($akun_id,$p->dbli_sham_id);
			$i++;
		}
		
		$this->_form('ubah', $data);
	}

	public function cek_saldo($str){
		if(strip_num_separator($str)<strip_num_separator($this->input->post('pmbl_grand_tot'))){
			$this->form_validation->set_message('cek_saldo', '<b>{field} </b>Tidak Mencukupi');
            return FALSE;
		}else{
			return TRUE;
		}
		
		
	}
	
	private function _data_form()
	{
		$validasi = array (
			array (
				'field' => 'pmbl_tgl_beli',
				'label' => 'Tanggal Pembelian',
				'rules' => 'required',
			),
			array (
				'field' => 'pmbl_akun_id',
				'label' => 'Akun',
				'rules' => 'required',
			),
			array (
				'field' => 'pmbl_grand_tot',
				'label' => 'Grand Total',
				'rules' => '',
			),
			array (
				'field' => 'pmbl_kode',
				'label' => 'Kode Pembelian',
				'rules' => '',
			),
			array (
				'field' => 'saldo_rdn',
				'label' => 'Saldo',
				'rules' => 'callback_cek_saldo',
			),
		);
		
		$this->form_validation->set_rules($validasi);
		
		if ($this->form_validation->run()) {
			
			$kunci_data = array (
				'pmbl_akun_id',
				'pmbl_tgl_beli',
				'pmbl_grand_tot|number',
				'pmbl_kode'
			);
			
			return data_post($kunci_data);
		}
		else {
			
			$this->session->set_flashdata('status_simpan', 'tidak_lengkap');
			$this->session->set_flashdata('validation_errors', validation_errors());
			$this->session->set_flashdata('data_form', (object) $this->input->post());
			redirect($this->agent->referrer());
		}
	}
	private function _data_detail()
	{
		$saham = $this->input->post('saham');
		
		$detail = array();
		
		foreach ($saham['dbli_sham_id'] as $i => $dbli_sham_id) {
			$detail[] = array (
				'dbli_sham_id' => $saham['dbli_sham_id'][$i],
				'dbli_jumlah_lot' => strip_num_separator($saham['dbli_jumlah_lot'][$i]),
				'dbli_jumlah_shares' => strip_num_separator($saham['dbli_jumlah_shares'][$i]),
				'dbli_harga_shares' => strip_num_separator($saham['dbli_harga_shares'][$i]),
				'dbli_bruto' => strip_num_separator($saham['dbli_bruto'][$i]),
				'dbli_fee_pembelian' => strip_num_separator($saham['dbli_fee_pembelian'][$i]),
				'dbli_netto' => strip_num_separator($saham['dbli_netto'][$i]),
			);
		}
		
		return $detail;
	}
	private function _data_detail_keuangan_saham()
	{
		$saham = $this->input->post('saham');
		
		$detail = array();
		
		foreach ($saham['dbli_sham_id'] as $i => $dbli_sham_id) {
			$detail[] = array (
				'kshm_sham_id' => $saham['dbli_sham_id'][$i],
				// 'kshm_tgl_transaksi'=>date('Y-m-d'),
				'kshm_jns_transaksi' => 'pembelian',
				'kshm_jumlah' => strip_num_separator($saham['dbli_jumlah_lot'][$i]),
				'kshm_harga_rata_rata' => strip_num_separator($saham['dbli_harga_shares'][$i]),
				'kshm_harga_netto' => strip_num_separator($saham['dbli_netto'][$i]),
			);
		}
		
		return $detail;
	}
	public function tambah_data()
	{
        
		$data = $this->_data_form();
		$detail = $this->_data_detail();
		$detailsaham=$this->_data_detail_keuangan_saham();
		if ($data != null && $detail!=null) {
			$data['pmbl_created_by'] = $data['pmbl_updated_by'] = session_pengguna('peng_id');
            $data['pmbl_created_time']=$data["pmbl_updated_time"] =date('Y-m-d H:i:s');
			$this->db->trans_begin();
			$this->db->insert('pembelian', $data);

			$id_pembelian = $this->db->insert_id();

			foreach ($detail as $i => $p){
				$detail[$i]['dbli_pmbl_id'] = $id_pembelian;
			}
			$this->db->insert_batch('det_pembelian', $detail);

			//insert ke saham
			foreach ($detailsaham as $i => $p){
				$detailsaham[$i]['kshm_transaksi_id'] = $id_pembelian;
				$detailsaham[$i]['kshm_akun_id'] = $data['pmbl_akun_id'];
				$detailsaham[$i]['kshm_tgl_transaksi'] = $data['pmbl_tgl_beli'];
				// 'kshm_tgl_transaksi'=>date('Y-m-d'),
				$detailsaham[$i]['kshm_created_by'] = session_pengguna('peng_id');
				$detailsaham[$i]['kshm_created_time'] = date('Y-m-d H:i:s');
			}
			$this->db->insert_batch('keuangan_saham', $detailsaham);
			//insert ke keuangan
			$datakeuangan=array(
				'uang_akun_id'=>$data['pmbl_akun_id'],
				'uang_tgl'=>$data['pmbl_tgl_beli'],
				'uang_jns_transaksi'=>'pembelian',
				'uang_transaksi_id'=>$id_pembelian,
				'uang_nominal'=>$data['pmbl_grand_tot']*-1,
				'uang_created_by'=>session_pengguna('peng_id'),
				'uang_created_time'=>date('Y-m-d H:i:s'),
			);
			$this->db->insert('keuangan', $datakeuangan);

			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				$this->session->set_flashdata('status_simpan', 'gagal');
			}
			else{
				$this->db->trans_commit();
				$this->session->set_flashdata('status_simpan', 'ok');
			}
		}
		
		redirect($this->agent->referrer());
	}
	
	public function ubah_data()
	{
		$data = $this->_data_form();
		$detail = $this->_data_detail();
		$detailsaham=$this->_data_detail_keuangan_saham();
		if ($data != null && $detail!=null) {
			$data['pmbl_updated_by'] = session_pengguna('peng_id');
            $data["pmbl_updated_time"] =date('Y-m-d H:i:s');
			$id_pembelian=$this->input->post('pmbl_id');
			
			$this->db->trans_begin();
			$this->db->update('pembelian', $data,array('pmbl_id'=>$id_pembelian));
			$this->db->delete('det_pembelian', array('dbli_pmbl_id' => $id_pembelian));

			foreach ($detail as $i => $p){
				$detail[$i]['dbli_pmbl_id'] = $id_pembelian;
			}
			
			$this->db->insert_batch('det_pembelian', $detail);

			//insert ke saham
			foreach ($detailsaham as $i => $p){
				$detailsaham[$i]['kshm_transaksi_id'] = $id_pembelian;
				$detailsaham[$i]['kshm_tgl_transaksi'] = $data['pmbl_tgl_beli'];
				$detailsaham[$i]['kshm_created_by'] = session_pengguna('peng_id');
				$detailsaham[$i]['kshm_created_time'] = date('Y-m-d H:i:s');
				$detailsaham[$i]['kshm_akun_id'] = $data['pmbl_akun_id'];
			}
			$this->db->delete('keuangan_saham', array('kshm_transaksi_id' => $id_pembelian,'kshm_jns_transaksi'=>'pembelian'));
			$this->db->insert_batch('keuangan_saham', $detailsaham);
			//insert ke keuangan
			$datakeuangan=array(
				'uang_akun_id'=>$data['pmbl_akun_id'],
				'uang_tgl'=>$data['pmbl_tgl_beli'],
				'uang_jns_transaksi'=>'pembelian',
				'uang_transaksi_id'=>$id_pembelian,
				'uang_nominal'=>$data['pmbl_grand_tot']*-1,
				'uang_created_by'=>session_pengguna('peng_id'),
				'uang_created_time'=>date('Y-m-d H:i:s'),
			);
			$this->db->delete('keuangan', array('uang_transaksi_id' => $id_pembelian,'uang_jns_transaksi'=>'pembelian'));
			$this->db->insert('keuangan', $datakeuangan);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				$this->session->set_flashdata('status_simpan', 'gagal');
			}
			else{
				$this->db->trans_commit();
				$this->session->set_flashdata('status_simpan', 'ok');
			}
		}
		redirect(site_url('/transaksi/pembelian'));
	}
	
    public function delete($pmbl_id){
        $data['pmbl_updated_time'] = date('Y-m-d H:i:s');
        $data['pmbl_updated_by'] = session_pengguna('peng_id');
        $data['pmbl_is_deleted'] = "0";
        $where = array('pmbl_id' => $pmbl_id);
        
        $this->db->update('pembelian', $data, $where);
        redirect(site_url('/transaksi/pembelian'));
    }
}
