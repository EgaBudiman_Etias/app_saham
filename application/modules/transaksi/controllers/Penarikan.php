<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penarikan extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->form_validation->CI =& $this;
	}

	public function index()
	{
		$this->load->view('templates/site_tpl', array (
			'content' => 'penarikan_index',
		));
	}
	
	public function datatable()
	{
		$draw = $this->input->post('draw');
		$offset = $this->input->post('start');
		$num_rows = $this->input->post('length');
		$order_index = $_POST['order'][0]['column'];
		$order_by = $_POST['columns'][$order_index]['data'];
		$order_direction = $_POST['order'][0]['dir'];
		$keyword = $_POST['search']['value'];
		$bank_id=$this->input->post('bank_id');
		$seku_id=$this->input->post('seku_id');
		$tgl=$this->input->post('tgl');
		$dari_pembelian=$this->input->post('dari_pembelian');
		$hingga_pembelian=$this->input->post('hingga_pembelian');
		$bindings = array("%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%");
		
		$base_sql = "
			from penarikan
			JOIN akun on(trik_akun_id=akun_id)
			JOIN bank on (bank_id=trik_bank_id)
			JOIN sekuritas on(akun_seku_id=seku_id)
			where
				trik_is_deleted = '1'
				and (
					trik_tgl like ?
					or bank_nama like ?
					or akun_no_sid like ?
					or trik_kode like ?
					or seku_nama like ?
					or akun_no_rekening_rdn like ?
					or akun_bank_rdn like ?
					or trik_nominal like ?
				)
		";
		if($bank_id!=""){
			$base_sql.=" AND trik_bank_id='$bank_id' ";
		};
		if($seku_id!=""){
			$base_sql.=" AND akun_seku_id='$seku_id' ";
		};
		if($tgl!=""){
			$base_sql.=" AND trik_tgl='$tgl' ";
		};
		if($hingga_pembelian!='0'){
			if($dari_pembelian!=""){
				$base_sql.=" AND trik_nominal>='$dari_pembelian' ";
			};
			if($hingga_pembelian!=""){
				$base_sql.=" AND trik_nominal<='$hingga_pembelian' ";
			};
		}
		
		$data_sql = "
			select
				penarikan.*,bank_nama,akun_no_sid,akun_kode_nasabah,akun_bank_rdn as bank_tujuan,seku_nama,
				akun_no_rekening_rdn as rek_tujuan,trik_kode
				, row_number() over (
					order by
						{$order_by} {$order_direction}
						, trik_tgl {$order_direction}
				) as nomor
			{$base_sql}
			order by
				{$order_by} {$order_direction}
				, trik_tgl {$order_direction}
			limit {$offset}, {$num_rows}
		";
                    
		$src = $this->db->query($data_sql, $bindings);
		// echo $this->db->last_query();
        // die();
		$count_sql = "
			select count(*) AS total
			{$base_sql}
		";
		$total_records = $this->db->query($count_sql, $bindings)->row('total');
		
		$data = array();
		
		foreach ($src->result() as $row) {
			$data[] = array (
				'trik_id' => $row->trik_id,
				'trik_tgl' => $row->trik_tgl,
				'trik_akun_id' => $row->trik_akun_id,
				'trik_bank_id' => $row->trik_bank_id,
				'trik_no_ref' => $row->trik_no_ref,
				'trik_nominal' => rupiah2($row->trik_nominal),
                'akun_no_sid' => $row->akun_no_sid,
                'akun_kode_nasabah' => $row->akun_kode_nasabah,
                'bank_tujuan'=>$row->bank_tujuan,
				'bank_nama'=>$row->bank_nama,
				'seku_nama'=>$row->seku_nama,
				'rek_tujuan'=>$row->rek_tujuan,
				'trik_kode'=>$row->trik_kode,
                'no'=>$row->nomor,
			);
		}
		
		$response = array (
			'draw' => intval($draw),
			'iTotalRecords' => $src->num_rows(),
			'iTotalDisplayRecords' => $total_records,
			'aaData' => $data,
		);
		
		echo json_encode($response);
	}
	
	private function _form($aksi = 'tambah', $data = null)
	{
		if ($this->session->flashdata('data_form')) {
			$data = $this->session->flashdata('data_form');
		}
		
		$this->load->view('templates/site_tpl', array (
			'content' => 'penarikan_form',
			'url_aksi' => site_url("/transaksi/penarikan/{$aksi}-data"),
			'data' => $data,
		));
	}
	
	public function tambah()
	{
		$this->_form();
	}
	
	public function ubah($id = '')
	{
		if ( ! $this->agent->referrer()) {
			show_404();
		}
		
		$src = $this->db
			->from('penarikan')
			->where('trik_is_deleted', '1')
			->where('trik_id', $id)
			->get();
		
		if ($src->num_rows() == 0) {
			show_404();
		}
		
		$this->_form('ubah', $src->row());
	}
	public function cekTarik($str){
        $saldo_akun=$this->input->post('saldo_akun');
		if ($str>$saldo_akun) {
            $this->form_validation->set_message('cekTarik', '<b>{field} </b> Penarikan melebihi Saldo Akun');
            return FALSE;
        } else {
            return TRUE;
        }
	}
	private function _data_form()
	{
		$validasi = array (
			array (
				'field' => 'trik_tgl',
				'label' => 'Tanggal Penarikan',
				'rules' => 'required',
			),
			array (
				'field' => 'trik_kode',
				'label' => 'Kode Penarikan',
				'rules' => 'required',
			),
			array (
				'field' => 'trik_akun_id',
				'label' => 'Akun Nasabah',
				'rules' => 'required',
			),
			array (
				'field' => 'trik_bank_id',
				'label' => 'Bank Tujuan',
				'rules' => 'required',
			),
            array (
				'field' => 'trik_no_ref',
				'label' => 'Nomor Referensi',
				'rules' => '',
			),
            array (
				'field' => 'trik_nominal',
				'label' => 'Nominal',
				'rules' => 'required|callback_cekTarik',
			),
		);
		
		$this->form_validation->set_rules($validasi);
		
		if ($this->form_validation->run()) {
			
			$kunci_data = array (
				'trik_tgl',
				'trik_kode',
				'trik_akun_id',
				'trik_bank_id',
                'trik_no_ref',
                'trik_nominal|number',
			);
			
			return data_post($kunci_data);
		}
		else {
			$this->session->set_flashdata('status_simpan', 'tidak_lengkap');
			$this->session->set_flashdata('validation_errors', validation_errors());
			$this->session->set_flashdata('data_form', (object) $this->input->post());
			return null;
		}
	}
	
	public function tambah_data()
	{
		$data = $this->_data_form();
		
		if ($data != null) {
			$data['trik_created_by'] = $data['trik_updated_by'] = session_pengguna('peng_id');
            $data['trik_created_time']=$data["trik_updated_time"] =date('Y-m-d H:i:s');
			$this->db->trans_begin();
			$this->db->insert('penarikan', $data);
			$insert_id = $this->db->insert_id();
			$datakeuangan=array(
				'uang_akun_id'=>$data['trik_akun_id'],
				'uang_tgl'=>$data['trik_tgl'],
				'uang_jns_transaksi'=>'penarikan',
				'uang_transaksi_id'=>$insert_id,
				'uang_nominal'=>$data['trik_nominal']*-1,
				'uang_created_by'=>session_pengguna('peng_id'),
				'uang_created_time'=>date('Y-m-d H:i:s'),
			);
			$this->db->insert('keuangan', $datakeuangan);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				$this->session->set_flashdata('status_simpan', 'gagal');
			}
			else{
				$this->db->trans_commit();
				$this->session->set_flashdata('status_simpan', 'ok');
			}
			
		}
		
		redirect(site_url('/transaksi/penarikan/tambah'));
	}
	
	public function ubah_data()
	{
		$data = $this->_data_form();
		
		if ($data != null) {
			$data['trik_updated_time'] = date('Y-m-d H:i:s');
			$data['trik_updated_by'] = session_pengguna('peng_id');
			$trik_id=$this->input->post('trik_id');
			$trik_nominal=$this->input->post('trik_nominal_awal');
			$where = array('trik_id' => $trik_id);
			//update setoran
			$this->db->trans_begin();
			$this->db->update('penarikan', $data, $where);

			//keuangan
			$cekpenarikan=$this->db->get_where('penarikan',array('trik_id'=>$trik_id))->row();
			$trik_nominal_ubah=$cekpenarikan->trik_nominal;
			$selisih=$trik_nominal_ubah-$trik_nominal;

			$this->db->delete('keuangan', array('uang_transaksi_id' => $trik_id,'uang_jns_transaksi'=>'penarikan'));
			$datakeuangan=array(
				'uang_akun_id'=>$data['trik_akun_id'],
				'uang_tgl'=>$data['trik_tgl'],
				'uang_jns_transaksi'=>'penarikan',
				'uang_transaksi_id'=>$trik_id,
				'uang_nominal'=>$data['trik_nominal']*-1,
				'uang_created_by'=>session_pengguna('peng_id'),
				'uang_created_time'=>date('Y-m-d H:i:s'),
			);
			
			$this->db->insert('keuangan', $datakeuangan);

			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				$this->session->set_flashdata('status_simpan', 'gagal');
			}
			else{
				$this->db->trans_commit();
				$this->session->set_flashdata('status_simpan', 'ok');
			}
			
		}
		
		redirect(site_url('/transaksi/penarikan/ubah/'.$trik_id));
	}
	
    public function delete($trik_id){
        $data['trik_updated_time'] = date('Y-m-d H:i:s');
        $data['trik_updated_by'] = session_pengguna('peng_id');
        $data['trik_is_deleted'] = "0";
        $where = array('trik_id' => $trik_id);
        
		$this->db->trans_begin();
		//update status setoran jadi 0
		
        $this->db->update('penarikan', $data, $where);

		//ceksetoran
		$ceksetoran=$this->db->get_where('penarikan',array('trik_id'=>$trik_id))->row();
		$datakeuangan=array(
			'uang_akun_id'=>$ceksetoran->trik_akun_id,
			'uang_tgl'=>date('Y-m-d'),
			'uang_jns_transaksi'=>'epenarikan',
			'uang_transaksi_id'=>$trik_id,
			'uang_nominal'=>$ceksetoran->trik_nominal,
			'uang_created_by'=>session_pengguna('peng_id'),
			'uang_created_time'=>date('Y-m-d H:i:s'),
		);
		//insert ke tabel keuangan
		$this->db->insert('keuangan', $datakeuangan);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		}
		else{
			$this->db->trans_commit();
		}
        redirect(site_url('/transaksi/penarikan'));
    }
}
