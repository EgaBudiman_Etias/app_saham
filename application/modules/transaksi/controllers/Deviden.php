<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Deviden extends MX_Controller {

	public function index()
	{
		$this->load->view('templates/site_tpl', array (
			'content' => 'deviden_index',
			
		));
	}
	
	public function datatable()
	{
		$draw = $this->input->post('draw');
		$offset = $this->input->post('start');
		$num_rows = $this->input->post('length');
		$order_index = $_POST['order'][0]['column'];
		$order_by = $_POST['columns'][$order_index]['data'];
		$order_direction = $_POST['order'][0]['dir'];
		$keyword = $_POST['search']['value'];
		$akun_id=$this->input->post('akun_id');
		$tgl=$this->input->post('tgl');
		$no_sid=$this->input->post('no_sid');
		$no_ksei=$this->input->post('no_ksei');
		$dari_pembelian=$this->input->post('dari_pembelian');
		$hingga_pembelian=$this->input->post('hingga_pembelian');
		$bindings = array("%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%");
		
		$base_sql = "
			from deviden
			JOIN akun on(devi_akun_id=akun_id)
            JOIN sekuritas on(akun_seku_id=seku_id)
            where
				devi_is_deleted = '1'
				and (
					seku_nama like ?
                    or akun_no_sid like ?
					or devi_kode like ?
					or devi_tgl_bagi like ?
					or akun_kode like ?
					or akun_no_ksei_kpei like ?
					or devi_grand_tot like ?
				)
		";
		if($akun_id!=""){
			$base_sql.=" AND devi_akun_id='$akun_id' ";
		};
		if($tgl!=""){
			$base_sql.=" AND devi_tgl_bagi='$tgl' ";
		};
		if($no_sid!=""){
			$base_sql.=" AND akun_no_sid='$no_sid' ";
		};
		if($no_ksei!=""){
			$base_sql.=" AND akun_no_ksei_kpei='$no_ksei' ";
		};
		if($hingga_pembelian!="0"){
			if($dari_pembelian!=""){
				$base_sql.=" AND devi_grand_tot>='$dari_pembelian' ";
			};
			if($hingga_pembelian!=""){
				$base_sql.=" AND devi_grand_tot<='$hingga_pembelian' ";
			};
		}
		$data_sql = "
			select
				deviden.*,seku_nama,akun_no_sid,akun_no_ksei_kpei,akun_kode,devi_kode
				, row_number() over (
					order by
						{$order_by} {$order_direction}
						, devi_id {$order_direction}
				  ) as nomor
			{$base_sql}
			order by
				{$order_by} {$order_direction}
				, devi_id {$order_direction}
			limit {$offset}, {$num_rows}
		";
                    
		$src = $this->db->query($data_sql, $bindings);
		$q= $this->db->last_query();
        
		$count_sql = "
			select count(*) AS total
			{$base_sql}
		";
		$total_records = $this->db->query($count_sql, $bindings)->row('total');
		
		$data = array();
		
		foreach ($src->result() as $row) {
			$data[] = array (
				'seku_nama' => $row->seku_nama,
				'akun_no_sid' => $row->akun_no_sid,
				'akun_no_ksei_kpei' => $row->akun_no_ksei_kpei,
				'devi_akun_id' => $row->devi_akun_id,
				'devi_tgl_bagi' => $row->devi_tgl_bagi,
                'devi_grand_tot' => rupiah2($row->devi_grand_tot),
                'devi_id'=>$row->devi_id,
                'no'=>$row->nomor,
				'devi_kode'=>$row->devi_kode,
				'akun_kode'=>$row->akun_kode,
			);
		}
		
		$response = array (
			'draw' => intval($draw),
			'iTotalRecords' => $src->num_rows(),
			'iTotalDisplayRecords' => $total_records,
			'aaData' => $data,
			
		);
		
		echo json_encode($response);
	}
	
	private function _form($aksi = 'tambah', $data = null)
	{
		if ($this->session->flashdata('data_form')) {
			$data = $this->session->flashdata('data_form');
		}
		$src = $this->db
			->select('*')
			->from('konfigurasi')
			->where('konf_kode', 'pajak_deviden')
			->get()->row()->konf_nilai;
		$this->load->view('templates/site_tpl', array (
			'content' => 'deviden_form',
			'url_aksi' => site_url("/transaksi/deviden/{$aksi}-data"),
			'data' => $data,
			'aksi'=>$aksi,
			'pajak'=>$src,
		));
	}
	
	public function tambah()
	{
		$this->_form();
	}
	
	public function ubah($id = '')
	{
		if ( ! $this->agent->referrer()) {
			show_404();
		}
		$src = $this->db
			->from('deviden')
			->where('devi_id', $id)
			->get();
		
		if ($src->num_rows() == 0) {
			show_404();
		}
		
		$data = $src->row();

		$detail = $this->db
			->select('*')
			->from('det_deviden')
			->join('deviden','ddev_devi_id=devi_id')
			->join('saham ', 'ddev_sham_id = sham_id')
			->join('akun','devi_akun_id=akun_id')
			->join('sekuritas','akun_seku_id=seku_id')
			
			->where(array('ddev_devi_id'=> $id))
			->order_by('ddev_id')
			->get();
		
		$i = 0;
		$pajak=$this->db->from('konfigurasi')->where('konf_kode','pajak_deviden')->get()->row()->konf_nilai;
		foreach ($detail->result() as $p) {
			$data->saham['ddev_sham_id'][$i] = $p->ddev_sham_id;
			$data->saham['ddev_jumlah_lot'][$i] = $p->ddev_jumlah_lot;
			$data->saham['ddev_jumlah_shares'][$i] = $p->ddev_jumlah_shares;
			$data->saham['ddev_deviden_shares'][$i] = $p->ddev_deviden_shares;
			$data->saham['ddev_bruto'][$i] = $p->ddev_bruto;
			$data->saham['ddev_pajak_deviden_persen'][$i] = $pajak;
			$data->saham['ddev_pajak_deviden'][$i] = $p->ddev_pajak_deviden;
			$data->saham['ddev_netto'][$i] = $p->ddev_netto;
			$i++;
		}
		
		$this->_form('ubah', $data);
	}
	
	private function _data_form()
	{
		$validasi = array (
			array (
				'field' => 'devi_tgl_bagi',
				'label' => 'Tanggal Pembagian',
				'rules' => 'required',
			),
			array (
				'field' => 'devi_akun_id',
				'label' => 'Akun',
				'rules' => 'required',
			),
			array (
				'field' => 'devi_grand_tot',
				'label' => 'Grand Total',
				'rules' => '',
			),
			array (
				'field' => 'devi_kode',
				'label' => 'Kode Deviden',
				'rules' => '',
			),
		);
		
		$this->form_validation->set_rules($validasi);
		
		if ($this->form_validation->run()) {
			
			$kunci_data = array (
				'devi_akun_id',
				'devi_tgl_bagi',
				'devi_grand_tot|number',
				'devi_kode'
			);
			
			return data_post($kunci_data);
		}
		else {
			
			$this->session->set_flashdata('status_simpan', 'tidak_lengkap');
			$this->session->set_flashdata('validation_errors', validation_errors());
			$this->session->set_flashdata('data_form', (object) $this->input->post());
			redirect($this->agent->referrer());
		}
	}
	private function _data_detail()
	{
		$saham = $this->input->post('saham');
		
		$detail = array();
		
		foreach ($saham['ddev_sham_id'] as $i => $ddev_sham_id) {
			$detail[] = array (
				'ddev_sham_id' => $saham['ddev_sham_id'][$i],
				'ddev_jumlah_lot' => strip_num_separator($saham['ddev_jumlah_lot'][$i]),
				'ddev_jumlah_shares' => strip_num_separator($saham['ddev_jumlah_shares'][$i]),
				'ddev_deviden_shares' => strip_num_separator($saham['ddev_deviden_shares'][$i]),
				'ddev_bruto' => strip_num_separator($saham['ddev_bruto'][$i]),
				'ddev_pajak_deviden' => strip_num_separator($saham['ddev_pajak_deviden'][$i]),
				'ddev_netto' => strip_num_separator($saham['ddev_netto'][$i]),
			);
		}
		
		return $detail;
	}
	private function _data_detail_keuangan_saham()
	{
		$saham = $this->input->post('saham');
		
		$detail = array();
		
		foreach ($saham['ddev_sham_id'] as $i => $ddev_sham_id) {
			$detail[] = array (
				'kshm_sham_id' => $saham['ddev_sham_id'][$i],
				'kshm_tgl_transaksi'=>date('Y-m-d'),
				'kshm_jns_transaksi' => 'deviden',
				'kshm_jumlah' => strip_num_separator($saham['ddev_jumlah_lot'][$i]),
				'kshm_deviden_rata_rata' => strip_num_separator($saham['ddev_deviden_shares'][$i]),
				'kshm_deviden_netto' => strip_num_separator($saham['ddev_netto'][$i]),
			);
		}
		
		return $detail;
	}
	public function tambah_data()
	{
        
		$data = $this->_data_form();
		$detail = $this->_data_detail();
       
		// $detailsaham=$this->_data_detail_keuangan_saham();
		if ($data != null && $detail!=null) {
			$data['devi_created_by'] = $data['devi_updated_by'] = session_pengguna('peng_id');
            $data['devi_created_time']=$data["devi_updated_time"] =date('Y-m-d H:i:s');
			$this->db->trans_begin();
			$this->db->insert('deviden', $data);

			$id_deviden = $this->db->insert_id();

			foreach ($detail as $i => $p){
				$detail[$i]['ddev_devi_id'] = $id_deviden;
			}
			$this->db->insert_batch('det_deviden', $detail);

			//insert ke keuangan
			$datakeuangan=array(
				'uang_akun_id'=>$data['devi_akun_id'],
				'uang_tgl'=>$data['devi_tgl_bagi'],
				'uang_jns_transaksi'=>'deviden',
				'uang_transaksi_id'=>$id_deviden,
				'uang_nominal'=>$data['devi_grand_tot'],
				'uang_created_by'=>session_pengguna('peng_id'),
				'uang_created_time'=>date('Y-m-d H:i:s'),
			);
			$this->db->insert('keuangan', $datakeuangan);

			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				$this->session->set_flashdata('status_simpan', 'gagal');
			}
			else{
				$this->db->trans_commit();
				$this->session->set_flashdata('status_simpan', 'ok');
			}
		}
		
		redirect($this->agent->referrer());
	}
	
	public function ubah_data()
	{
		$data = $this->_data_form();
		$detail = $this->_data_detail();
		
		if ($data != null && $detail!=null) {
			$data['devi_updated_by'] = session_pengguna('peng_id');
            $data["devi_updated_time"] =date('Y-m-d H:i:s');
			$id_deviden=$this->input->post('devi_id');
			
			$this->db->trans_begin();
			$this->db->update('deviden', $data,array('devi_id'=>$id_deviden));
			$this->db->delete('det_deviden', array('ddev_devi_id' => $id_deviden));

			foreach ($detail as $i => $p){
				$detail[$i]['ddev_devi_id'] = $id_deviden;
			}
			
			$this->db->insert_batch('det_deviden', $detail);

			
			//insert ke keuangan
			$datakeuangan=array(
				'uang_akun_id'=>$data['devi_akun_id'],
				'uang_tgl'=>$data['devi_tgl_bagi'],
				'uang_jns_transaksi'=>'deviden',
				'uang_transaksi_id'=>$id_deviden,
				'uang_nominal'=>$data['devi_grand_tot'],
				'uang_created_by'=>session_pengguna('peng_id'),
				'uang_created_time'=>date('Y-m-d H:i:s'),
			);
			$this->db->delete('keuangan', array('uang_transaksi_id' => $id_deviden,'uang_jns_transaksi'=>'deviden'));
			$this->db->insert('keuangan', $datakeuangan);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				$this->session->set_flashdata('status_simpan', 'gagal');
			}
			else{
				$this->db->trans_commit();
				$this->session->set_flashdata('status_simpan', 'ok');
			}
		}
		redirect(site_url('/transaksi/deviden'));
	}
	
    public function delete($devi_id){
        $data['devi_updated_time'] = date('Y-m-d H:i:s');
        $data['devi_updated_by'] = session_pengguna('peng_id');
        $data['devi_is_deleted'] = "0";
        $where = array('devi_id' => $devi_id);
        
        $this->db->update('deviden', $data, $where);
        redirect(site_url('/transaksi/deviden'));
    }
}
