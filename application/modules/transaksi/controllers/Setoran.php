<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setoran extends MX_Controller {

	public function index()
	{
		$this->load->view('templates/site_tpl', array (
			'content' => 'setoran_index',
		));
	}
	
	public function datatable()
	{
		$draw = $this->input->post('draw');
		$offset = $this->input->post('start');
		$num_rows = $this->input->post('length');
		$order_index = $_POST['order'][0]['column'];
		$order_by = $_POST['columns'][$order_index]['data'];
		$order_direction = $_POST['order'][0]['dir'];
		$keyword = $_POST['search']['value'];
		$bank_id=$this->input->post('bank_id');
		$seku_id=$this->input->post('seku_id');
		$tgl=$this->input->post('tgl');
		$dari_pembelian=$this->input->post('dari_pembelian');
		$hingga_pembelian=$this->input->post('hingga_pembelian');
		$bindings = array("%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%");
		
		$base_sql = "
			from setoran
            JOIN akun on(stor_akun_id=akun_id)
			JOIN bank on (bank_id=stor_bank_id)
			JOIN sekuritas on(akun_seku_id=seku_id)
            where
				stor_is_deleted = '1'
				and (
					stor_tgl like ?
                    or bank_nama like ?
					or stor_no_ref like ?
                    or akun_no_sid like ?
					or seku_nama like ?
					or akun_no_rekening_rdn like ?
					or akun_bank_rdn like ?
					or akun_no_sid like ?
					or stor_nominal like ?
					or stor_kode like ?
				)
			";
		if($bank_id!=""){
			$base_sql.=" AND stor_bank_id='$bank_id' ";
		};
		if($seku_id!=""){
			$base_sql.=" AND akun_seku_id='$seku_id' ";
		};
		if($tgl!=""){
			$base_sql.=" AND stor_tgl='$tgl' ";
		};
		if($dari_pembelian!=""){
			$base_sql.=" AND stor_nominal>='$dari_pembelian' ";
		};
		if($hingga_pembelian!="" && $hingga_pembelian!='0'){
			$base_sql.=" AND stor_nominal<='$hingga_pembelian' ";
		};
		
		
		$data_sql = "
			select
				setoran.*,bank_nama,akun_no_sid,akun_kode_nasabah,akun_bank_rdn as bank_tujuan,seku_nama,akun_no_rekening_rdn as rek_tujuan
				, row_number() over (
					order by
						{$order_by} {$order_direction}
						, stor_tgl {$order_direction}
				  ) as nomor
			{$base_sql}
			order by
				{$order_by} {$order_direction}
				, stor_tgl {$order_direction}
			limit {$offset}, {$num_rows}
		";
                    
		$src = $this->db->query($data_sql, $bindings);
		// echo $this->db->last_query();
        // die();
		$count_sql = "
			select count(*) AS total
			{$base_sql}
		";
		$total_records = $this->db->query($count_sql, $bindings)->row('total');
		
		$data = array();
		
		foreach ($src->result() as $row) {
			$data[] = array (
				'stor_id' => $row->stor_id,
				'stor_tgl' => $row->stor_tgl,
				'stor_kode'=>$row->stor_kode,
				'stor_akun_id' => $row->stor_akun_id,
				'stor_bank_id' => $row->stor_bank_id,
				'stor_no_ref' => $row->stor_no_ref,
				'stor_nominal' => rupiah2($row->stor_nominal),
                'akun_no_sid' => $row->akun_no_sid,
                'akun_kode_nasabah' => $row->akun_kode_nasabah,
                'bank_tujuan'=>$row->bank_tujuan,
				'bank_nama'=>$row->bank_nama,
				'seku_nama'=>$row->seku_nama,
				'rek_tujuan'=>$row->rek_tujuan,
                'no'=>$row->nomor,
			);
		}
		
		$response = array (
			'draw' => intval($draw),
			'iTotalRecords' => $src->num_rows(),
			'iTotalDisplayRecords' => $total_records,
			'aaData' => $data,
			'util'=>array('bank_id'=>$bank_id,'seku_id'=>$seku_id)
		);
		
		echo json_encode($response);
	}
	
	private function _form($aksi = 'tambah', $data = null)
	{
		if ($this->session->flashdata('data_form')) {
			$data = $this->session->flashdata('data_form');
		}
		
		$this->load->view('templates/site_tpl', array (
			'content' => 'setoran_form',
			'url_aksi' => site_url("/transaksi/setoran/{$aksi}-data"),
			'data' => $data,
		));
	}
	
	public function tambah()
	{
		$this->_form();
	}
	
	public function ubah($id = '')
	{
		if ( ! $this->agent->referrer()) {
			show_404();
		}
		
		$src = $this->db
			->from('setoran')
			->where('stor_is_deleted', '1')
			->where('stor_id', $id)
			->get();
		
		if ($src->num_rows() == 0) {
			show_404();
		}
		
		$this->_form('ubah', $src->row());
	}
	
	private function _data_form()
	{
		$validasi = array (
			array (
				'field' => 'stor_tgl',
				'label' => 'Tanggal Setoran',
				'rules' => 'required',
			),
			array (
				'field' => 'stor_akun_id',
				'label' => 'Akun Nasabah',
				'rules' => 'required',
			),
			array (
				'field' => 'stor_bank_id',
				'label' => 'Bank Asal',
				'rules' => 'required',
			),
            array (
				'field' => 'stor_no_ref',
				'label' => 'Nomor Referensi',
				'rules' => '',
			),
			array (
				'field' => 'stor_kode',
				'label' => 'Kode Setoran',
				'rules' => 'required',
			),
            array (
				'field' => 'stor_nominal',
				'label' => 'Nominal',
				'rules' => 'required',
			),
		);
		
		$this->form_validation->set_rules($validasi);
		
		if ($this->form_validation->run()) {
			
			$kunci_data = array (
				'stor_tgl',
				'stor_akun_id',
				'stor_bank_id',
                'stor_no_ref',
                'stor_nominal|number',
				'stor_kode'
			);
			
			return data_post($kunci_data);
		}
		else {
			$this->session->set_flashdata('status_simpan', 'tidak_lengkap');
			$this->session->set_flashdata('validation_errors', validation_errors());
			$this->session->set_flashdata('data_form', (object) $this->input->post());
			return null;
		}
	}
	
	public function tambah_data()
	{
		$data = $this->_data_form();
		
		if ($data != null) {
			$data['stor_created_by'] = $data['stor_updated_by'] = session_pengguna('peng_id');
            $data['stor_created_time']=$data["stor_updated_time"] =date('Y-m-d H:i:s');
			
			$this->db->trans_begin();
			//insert ke tabel setoran
			$this->db->insert('setoran', $data);
			$insert_id = $this->db->insert_id();

			$datakeuangan=array(
				'uang_akun_id'=>$data['stor_akun_id'],
				'uang_tgl'=>$data['stor_tgl'],
				'uang_jns_transaksi'=>'setoran',
				'uang_transaksi_id'=>$insert_id,
				'uang_nominal'=>$data['stor_nominal'],
				'uang_created_by'=>session_pengguna('peng_id'),
				'uang_created_time'=>date('Y-m-d H:i:s'),
			);
			//insert ke tabel keuangan
			$this->db->insert('keuangan', $datakeuangan);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				$this->session->set_flashdata('status_simpan', 'gagal');
			}
			else{
				$this->db->trans_commit();
				$this->session->set_flashdata('status_simpan', 'ok');
			}
		}
		
		redirect(site_url('/transaksi/setoran/tambah'));
	}
	
	public function ubah_data()
	{
		$data = $this->_data_form();
		
		if ($data != null) {
			$data['stor_updated_time'] = date('Y-m-d H:i:s');
			$data['stor_updated_by'] = session_pengguna('peng_id');
			$stor_id=$this->input->post('stor_id');
			$stor_nominal=$this->input->post('stor_nominal_awal');
			$where = array('stor_id' => $stor_id);
			//update setoran
			$this->db->trans_begin();
			$this->db->update('setoran', $data, $where);

			//keuangan
			$ceksetoran=$this->db->get_where('setoran',array('stor_id'=>$stor_id))->row();
			$stor_nominal_ubah=$ceksetoran->stor_nominal;
			$selisih=$stor_nominal_ubah-$stor_nominal;

			$this->db->delete('keuangan', array('uang_transaksi_id' => $stor_id,'uang_jns_transaksi'=>'setoran'));
			$datakeuangan=array(
				'uang_akun_id'=>$data['stor_akun_id'],
				'uang_tgl'=>$data['stor_tgl'],
				'uang_jns_transaksi'=>'setoran',
				'uang_transaksi_id'=>$stor_id,
				'uang_nominal'=>$data['stor_nominal'],
				'uang_created_by'=>session_pengguna('peng_id'),
				'uang_created_time'=>date('Y-m-d H:i:s'),
			);
			$this->db->insert('keuangan', $datakeuangan);

			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				$this->session->set_flashdata('status_simpan', 'gagal');
			}
			else{
				$this->db->trans_commit();
				$this->session->set_flashdata('status_simpan', 'ok');
			}
			
		}
		
		redirect(site_url('/transaksi/setoran/ubah/'.$stor_id));
	}
	
    public function delete($stor_id){
        $data['stor_updated_time'] = date('Y-m-d H:i:s');
        $data['stor_updated_by'] = session_pengguna('peng_id');
        $data['stor_is_deleted'] = "0";
        $where = array('stor_id' => $stor_id);
        
		$this->db->trans_begin();
		//update status setoran jadi 0
		
        $this->db->update('setoran', $data, $where);

		//ceksetoran
		$ceksetoran=$this->db->get_where('setoran',array('stor_id'=>$stor_id))->row();
		// $datakeuangan=array(
		// 	'uang_akun_id'=>$ceksetoran->stor_akun_id,
		// 	'uang_tgl'=>date('Y-m-d'),
		// 	'uang_jns_transaksi'=>'esetoran',
		// 	'uang_transaksi_id'=>$stor_id,
		// 	'uang_nominal'=>$ceksetoran->stor_nominal * -1,
		// 	'uang_created_by'=>session_pengguna('peng_id'),
		// 	'uang_created_time'=>date('Y-m-d H:i:s'),
		// );
		
		//insert ke tabel keuangan
		// $this->db->insert('keuangan', $datakeuangan);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		}
		else{
			$this->db->trans_commit();
		}
        redirect(site_url('/transaksi/setoran'));
    }
}
