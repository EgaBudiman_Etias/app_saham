<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan extends MX_Controller {

	public function index()
	{
		$this->load->view('templates/site_tpl', array (
			'content' => 'penjualan_index',
			
		));
	}
	
	public function datatable()
	{
		$draw = $this->input->post('draw');
		$offset = $this->input->post('start');
		$num_rows = $this->input->post('length');
		$order_index = $_POST['order'][0]['column'];
		$order_by = $_POST['columns'][$order_index]['data'];
		$order_direction = $_POST['order'][0]['dir'];
		$keyword = $_POST['search']['value'];
		$akun_id=$this->input->post('akun_id');
		$tgl=$this->input->post('tgl');
		$no_sid=$this->input->post('no_sid');
		$no_ksei=$this->input->post('no_ksei');
		$dari_penjualan=$this->input->post('dari_penjualan');
		$hingga_penjualan=$this->input->post('hingga_penjualan');
		$bindings = array("%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%");
		
		$base_sql = "
			from penjualan
			JOIN akun on(pnjl_akun_id=akun_id)
            JOIN sekuritas on(akun_seku_id=seku_id)
            where
				pnjl_is_deleted = '1'
				and (
					seku_nama like ?
                    or akun_no_sid like ?
					or akun_no_ksei_kpei like ?
					or pnjl_tgl_jual like ?
					or pnjl_grand_tot like ?
					or akun_kode like ?
					or pnjl_kode like ?
				)
		";
		if($akun_id!=""){
			$base_sql.=" AND pnjl_akun_id='$akun_id' ";
		};
		if($tgl!=""){
			$base_sql.=" AND pnjl_tgl_jual='$tgl' ";
		};
		if($no_sid!=""){
			$base_sql.=" AND akun_no_sid='$no_sid' ";
		};
		if($no_ksei!=""){
			$base_sql.=" AND akun_no_ksei_kpei='$no_ksei' ";
		};
		if($hingga_penjualan!="0"){
			if($dari_penjualan!=""){
				$base_sql.=" AND pnjl_grand_tot>='$dari_penjualan' ";
			};
			if($hingga_penjualan!=""){
				$base_sql.=" AND pnjl_grand_tot<='$hingga_penjualan' ";
			};
		}
		
		$data_sql = "
			select
				penjualan.*,seku_nama,akun_no_sid,akun_no_ksei_kpei,akun_kode
				, row_number() over (
					order by
						{$order_by} {$order_direction}
						, pnjl_id {$order_direction}
				  ) as nomor
			{$base_sql}
			order by
				{$order_by} {$order_direction}
				, pnjl_id {$order_direction}
			limit {$offset}, {$num_rows}
		";
                    
		$src = $this->db->query($data_sql, $bindings);
		// echo $this->db->last_query();
        // die();
		$count_sql = "
			select count(*) AS total
			{$base_sql}
		";
		$total_records = $this->db->query($count_sql, $bindings)->row('total');
		
		$data = array();
		
		foreach ($src->result() as $row) {
			$data[] = array (
				'seku_nama' => $row->seku_nama,
				'akun_no_sid' => $row->akun_no_sid,
				'akun_no_ksei_kpei' => $row->akun_no_ksei_kpei,
				'pnjl_akun_id' => $row->pnjl_akun_id,
				'pnjl_tgl_jual' => $row->pnjl_tgl_jual,
                'pnjl_grand_tot' => rupiah2($row->pnjl_grand_tot),
                'pnjl_id'=>$row->pnjl_id,
                'no'=>$row->nomor,
				'akun_kode'=>$row->akun_kode,
				'pnjl_kode'=>$row->pnjl_kode,
			);
		}
		
		$response = array (
			'draw' => intval($draw),
			'iTotalRecords' => $src->num_rows(),
			'iTotalDisplayRecords' => $total_records,
			'aaData' => $data,
		);
		
		echo json_encode($response);
	}
	
	private function _form($aksi = 'tambah', $data = null)
	{
		if ($this->session->flashdata('data_form')) {
			$data = $this->session->flashdata('data_form');
		}
		$src = $this->db
			->select('*')
			->from('konfigurasi')
			->where('konf_kode', 'pajak_penjualan')
			->get()->row()->konf_nilai;
		$this->load->view('templates/site_tpl', array (
			'content' => 'penjualan_form',
			'url_aksi' => site_url("/transaksi/penjualan/{$aksi}-data"),
			'data' => $data,
			'aksi'=>$aksi,
			'pajak'=>$src,
		));
	}
	
	public function tambah()
	{
		$this->_form();
	}
	
	public function ubah($id = '')
	{
		$this->load->model('transaksi/pembelian_model');
		if ( ! $this->agent->referrer()) {
			show_404();
		}
		$src = $this->db
			->from('penjualan')
			->where('pnjl_id', $id)
			->get();
		
		if ($src->num_rows() == 0) {
			show_404();
		}
		
		$data = $src->row();
		$akun_id=$data->pnjl_akun_id;
		$data->saldo=$this->pembelian_model->carisaldoakun($akun_id)->nominal;
		$detail = $this->db
			->select('*')
			->from('det_penjualan')
			->join('penjualan','dpjl_pnjl_id=pnjl_id')
			->join('saham ', 'dpjl_sham_id = sham_id')
			->join('akun','pnjl_akun_id=akun_id')
			->join('sekuritas','akun_seku_id=seku_id')
			
			->where(array('dpjl_pnjl_id'=> $id))
			->order_by('dpjl_id')
			->get();
		
		$i = 0;
		
		foreach ($detail->result() as $p) {
			$data->saham['dpjl_sham_id'][$i] = $p->dpjl_sham_id;
			$data->saham['dpjl_jumlah_lot'][$i] = $p->dpjl_jumlah_lot;
			$data->saham['dpjl_jumlah_shares'][$i] = $p->dpjl_jumlah_shares;
			$data->saham['dpjl_harga_shares'][$i] = $p->dpjl_harga_shares;
			$data->saham['dpjl_bruto'][$i] = $p->dpjl_bruto;
			$data->saham['dpjl_fee_penjualan_persen'][$i] = $p->seku_broker_fee_penjualan;
			$data->saham['dpjl_fee_penjualan'][$i] = $p->dpjl_fee_penjualan;
			$data->saham['dpjl_pajak_penjualan'][$i] = $p->dpjl_pajak_penjualan;
			$data->saham['dpjl_netto'][$i] = $p->dpjl_netto;
			$data->saham['dpjl_jumlah_lot_saat_ini'][$i] = $this->pembelian_model->carisaldosaham($akun_id,$p->dpjl_sham_id);
			$i++;
		}
		
		$this->_form('ubah', $data);
	}
	
	private function _data_form()
	{
		$validasi = array (
			array (
				'field' => 'pnjl_tgl_jual',
				'label' => 'Tanggal Penjualan',
				'rules' => 'required',
			),
			array (
				'field' => 'pnjl_akun_id',
				'label' => 'Akun',
				'rules' => 'required',
			),
			array (
				'field' => 'pnjl_grand_tot',
				'label' => 'Grand Total',
				'rules' => '',
			),
			array (
				'field' => 'pnjl_kode',
				'label' => 'Kode Penjualan',
				'rules' => '',
			),
		);
		
		$this->form_validation->set_rules($validasi);
		
		if ($this->form_validation->run()) {
			
			$kunci_data = array (
				'pnjl_akun_id',
				'pnjl_tgl_jual',
				'pnjl_grand_tot|number',
				'pnjl_kode'
			);
			
			return data_post($kunci_data);
		}
		else {
			
			$this->session->set_flashdata('status_simpan', 'tidak_lengkap');
			$this->session->set_flashdata('validation_errors', validation_errors());
			$this->session->set_flashdata('data_form', (object) $this->input->post());
			redirect($this->agent->referrer());
		}
	}
	private function _data_detail()
	{
		$saham = $this->input->post('saham');
		
		$detail = array();
		
		foreach ($saham['dpjl_sham_id'] as $i => $dpjl_sham_id) {
			$detail[] = array (
				'dpjl_sham_id' => $saham['dpjl_sham_id'][$i],
				'dpjl_jumlah_lot' => strip_num_separator($saham['dpjl_jumlah_lot'][$i]),
				'dpjl_jumlah_shares' => strip_num_separator($saham['dpjl_jumlah_shares'][$i]),
				'dpjl_harga_shares' => strip_num_separator($saham['dpjl_harga_shares'][$i]),
				'dpjl_bruto' => strip_num_separator($saham['dpjl_bruto'][$i]),
				'dpjl_fee_penjualan' => strip_num_separator($saham['dpjl_fee_penjualan'][$i]),
				'dpjl_pajak_penjualan' => strip_num_separator($saham['dpjl_pajak_penjualan'][$i]),
				'dpjl_netto' => strip_num_separator($saham['dpjl_netto'][$i]),
			);
		}
		
		return $detail;
	}
	private function _data_detail_keuangan_saham()
	{
		$saham = $this->input->post('saham');
		
		$detail = array();
		
		foreach ($saham['dpjl_sham_id'] as $i => $dpjl_sham_id) {
			if($saham['dpjl_jumlah_lot'][$i]!=0){
				$detail[] = array (
					'kshm_sham_id' => $saham['dpjl_sham_id'][$i],
					// 'kshm_tgl_transaksi'=>date('Y-m-d'),
					'kshm_jns_transaksi' => 'penjualan',
					'kshm_jumlah' => strip_num_separator($saham['dpjl_jumlah_lot'][$i])*-1,
					'kshm_harga_rata_rata' => strip_num_separator($saham['dpjl_harga_shares'][$i]),
					'kshm_harga_netto' => strip_num_separator($saham['dpjl_netto'][$i])*-1,
					
				);
			}
			
		}
		
		return $detail;
	}

	public function cekGl($tgl,$akun,$saham,$harga,$jumlah){
		$hasil=$this->db->query("
			SELECT akun,saham,coalesce(sum(lot),0) as lot,coalesce(sum(netto),0) as netto,sham_kode FROM (
				(SELECT '' as id,'1.saldo-awal' AS ket,shal_tgl_saldo_awal AS tgl,shal_akun_id AS akun,shal_sham_id AS saham,shal_jumlah_lot AS lot,shal_rata_rata_harga AS harga,shal_total AS netto
				FROM saldo_awal_saham
				JOIN akun ON shal_akun_id=akun_id
				WHERE shal_akun_id='$akun' and shal_sham_id='$saham' AND akun_is_deleted='1'
				)
				UNION
				(SELECT pmbl_id,'2.pembelian',pmbl_tgl_beli,pmbl_akun_id,dbli_sham_id,dbli_jumlah_lot,dbli_harga_shares,dbli_netto
				FROM det_pembelian
				JOIN pembelian ON dbli_pmbl_id=pmbl_id
				WHERE pmbl_is_deleted='1' AND pmbl_akun_id='$akun' and dbli_sham_id='$saham' and pmbl_tgl_beli<='$tgl'
				)
				) AS dt
				JOIN saham on saham=sham_id

				ORDER BY saham,tgl,ket ASC
		")->row();
		$jumlah=$jumlah*-1;
		$lot=0;$netto=0;$rata=0;$gl=0;
		if(!empty($hasil)){
			$lot=$hasil->lot;
			$netto=$hasil->netto;
			if($lot==0){
				$rata=0;
			}else{
				$rata=round($netto/($lot*100),2);

			}
			$gl=$harga-$rata;
		}
		return $gl * $jumlah*100;
	}

	public function tambah_data()
	{
        
		$data = $this->_data_form();
		$detail = $this->_data_detail();
		$detailsaham=$this->_data_detail_keuangan_saham();
		
		if ($data != null && $detail!=null) {
			$data['pnjl_created_by'] = $data['pnjl_updated_by'] = session_pengguna('peng_id');
            $data['pnjl_created_time']=$data["pnjl_updated_time"] =date('Y-m-d H:i:s');
			$this->db->trans_begin();
			$this->db->insert('penjualan', $data);

			$id_penjualan = $this->db->insert_id();

			foreach ($detail as $i => $p){
				$detail[$i]['dpjl_pnjl_id'] = $id_penjualan;
			}
			$this->db->insert_batch('det_penjualan', $detail);

			//insert ke saham
			foreach ($detailsaham as $i => $p){
				$detailsaham[$i]['kshm_transaksi_id'] = $id_penjualan;
				$detailsaham[$i]['kshm_akun_id'] = $data['pnjl_akun_id'];
				$detailsaham[$i]['kshm_tgl_transaksi'] = $data['pnjl_tgl_jual'];
				$detailsaham[$i]['kshm_gl'] = $this->cekGl($data['pnjl_tgl_jual'],$data['pnjl_akun_id'],$p['kshm_sham_id'],$p['kshm_harga_rata_rata'],$p['kshm_jumlah']) ;
				$detailsaham[$i]['kshm_created_by'] = session_pengguna('peng_id');
				$detailsaham[$i]['kshm_created_time'] = date('Y-m-d H:i:s');
			}
			// var_dump($detailsaham);
			// die();
			$this->db->insert_batch('keuangan_saham', $detailsaham);
			//insert ke keuangan
			$datakeuangan=array(
				'uang_akun_id'=>$data['pnjl_akun_id'],
				'uang_tgl'=>$data['pnjl_tgl_jual'],
				'uang_jns_transaksi'=>'penjualan',
				'uang_transaksi_id'=>$id_penjualan,
				'uang_nominal'=>$data['pnjl_grand_tot'],
				'uang_created_by'=>session_pengguna('peng_id'),
				'uang_created_time'=>date('Y-m-d H:i:s'),
			);
			$this->db->insert('keuangan', $datakeuangan);

			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				$this->session->set_flashdata('status_simpan', 'gagal');
			}
			else{
				$this->db->trans_commit();
				$this->session->set_flashdata('status_simpan', 'ok');
			}
		}
		
		redirect($this->agent->referrer());
	}
	
	public function ubah_data()
	{
		$data = $this->_data_form();
		$detail = $this->_data_detail();
		$detailsaham=$this->_data_detail_keuangan_saham();
		
		if ($data != null && $detail!=null) {
			$data['pnjl_updated_by'] = session_pengguna('peng_id');
            $data["pnjl_updated_time"] =date('Y-m-d H:i:s');
			$id_penjualan=$this->input->post('pnjl_id');
			
			$this->db->trans_begin();
			$this->db->update('penjualan', $data,array('pnjl_id'=>$id_penjualan));
			$this->db->delete('det_penjualan', array('dpjl_pnjl_id' => $id_penjualan));

			foreach ($detail as $i => $p){
				$detail[$i]['dpjl_pnjl_id'] = $id_penjualan;
			}
			
			$this->db->insert_batch('det_penjualan', $detail);
			//delete from saham

			//insert ke saham
			foreach ($detailsaham as $i => $p){
				$detailsaham[$i]['kshm_transaksi_id'] = $id_penjualan;
				$detailsaham[$i]['kshm_akun_id'] = $data['pnjl_akun_id'];
				$detailsaham[$i]['kshm_tgl_transaksi'] = $data['pnjl_tgl_jual'];
				// $detailsaham[$i]['kshm_gl'] = $this->cekGl($data['pnjl_tgl_jual'],$data['pnjl_akun_id'],$p['kshm_sham_id'],$p['kshm_harga_rata_rata']) * $p['kshm_jumlah'];
				$detailsaham[$i]['kshm_gl'] = $this->cekGl($data['pnjl_tgl_jual'],$data['pnjl_akun_id'],$p['kshm_sham_id'],$p['kshm_harga_rata_rata'],$p['kshm_jumlah']) ;
				$detailsaham[$i]['kshm_created_by'] = session_pengguna('peng_id');
				$detailsaham[$i]['kshm_created_time'] = date('Y-m-d H:i:s');
			}
			//saham penjualan
			$this->db->delete('keuangan_saham', array('kshm_transaksi_id' => $id_penjualan,'kshm_jns_transaksi'=>'penjualan'));
			$this->db->insert_batch('keuangan_saham', $detailsaham);
			//insert ke keuangan
			$datakeuangan=array(
				'uang_akun_id'=>$data['pnjl_akun_id'],
				'uang_tgl'=>$data['pnjl_tgl_jual'],
				'uang_jns_transaksi'=>'penjualan',
				'uang_transaksi_id'=>$id_penjualan,
				'uang_nominal'=>$data['pnjl_grand_tot'],
				'uang_created_by'=>session_pengguna('peng_id'),
				'uang_created_time'=>date('Y-m-d H:i:s'),
			);
			$this->db->delete('keuangan', array('uang_transaksi_id' => $id_penjualan,'uang_jns_transaksi'=>'penjualan'));
			$this->db->insert('keuangan', $datakeuangan);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				$this->session->set_flashdata('status_simpan', 'gagal');
			}
			else{
				$this->db->trans_commit();
				$this->session->set_flashdata('status_simpan', 'ok');
			}
		}
		redirect(site_url('/transaksi/penjualan'));
	}
	
    public function delete($pnjl_id){
        $data['pnjl_updated_time'] = date('Y-m-d H:i:s');
        $data['pnjl_updated_by'] = session_pengguna('peng_id');
        $data['pnjl_is_deleted'] = "0";
        $where = array('pnjl_id' => $pnjl_id);
        
        $this->db->update('penjualan', $data, $where);
        redirect(site_url('/transaksi/penjualan'));
    }
}
