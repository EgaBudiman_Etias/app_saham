<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembelian_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
	public function carisaldoakun($akun_id){
		$src = $this->db
			->select('coalesce(sum(uang_nominal),0) as nominal,uang_akun_id')
			->from('keuangan')
			->where('uang_akun_id', $akun_id)
			->get()->row();
		return $src;
	}
	public function carisaldosaham($akun_id,$sham_id){
		$ci =& get_instance();
		$saldosahamawal=0;
		$saldopembeliansaham=0;
		$saldopenjualansaham=0;
		$srcsahamawal=$this->db->select('shal_id,shal_akun_id,akun_seku_id,shal_sham_id,shal_tgl_saldo_awal,shal_jumlah_lot,shal_jumlah_shares,shal_rata_rata_harga,shal_total ,shal_is_deleted ')
								 ->from('saldo_awal_saham')
								 ->JOIN('akun','shal_akun_id=akun_id')
								 ->JOIN('sekuritas','seku_id=akun_seku_id')
								 ->where(array('shal_akun_id'=>$akun_id,'shal_sham_id'=>$sham_id))
								 ->get()->row();
			
		if(!empty($srcsahamawal)){
			$saldosahamawal=$srcsahamawal->shal_jumlah_lot;
		}	
		$srcpembelian=$this->db->select('coalesce(sum(kshm_jumlah),0) as jumlah_lot')
							->from('keuangan_saham')
							->JOIN ('pembelian','kshm_transaksi_id=pmbl_id')
							->JOIN('akun','pmbl_akun_id=akun_id')
							->JOIN('sekuritas','seku_id=akun_seku_id')
							->WHERE(array('kshm_jns_transaksi'=>"pembelian",'pmbl_akun_id'=>$akun_id,'kshm_sham_id'=>$sham_id,'pmbl_is_deleted'=>'1'))
							->get()->row();
						
		if(!empty($srcpembelian)){
			$saldopembeliansaham=$srcpembelian->jumlah_lot;
		}
	
		$srcpenjualan=$this->db->select('coalesce(sum(kshm_jumlah),0) as jumlah_lot')
							->from('keuangan_saham')
							->JOIN ('penjualan','kshm_transaksi_id=pnjl_id')
							->JOIN('akun','pnjl_akun_id=akun_id')
							->JOIN('sekuritas','seku_id=akun_seku_id')
							->WHERE(array('kshm_jns_transaksi'=>"penjualan",'pnjl_akun_id'=>$akun_id,'kshm_sham_id'=>$sham_id,'pnjl_is_deleted'=>'1'))
							->get()->row();
						
		if(!empty($srcpenjualan)){
			$saldopenjualansahamsaham=$srcpenjualan->jumlah_lot;
		}
		return $saldosahamawal+$saldopembeliansaham+$saldopenjualansaham;
	}
    
}