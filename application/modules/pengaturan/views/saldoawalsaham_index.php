<div class="col-md-10 offset-md-1 p-0">
	<div class="card">
		<div class="card-header">
			Pengaturan Saldo Awal Saham
			
		</div>
		<div class="card-body">
			<table class="cell-border stripe order-column hover" id="datatable">
				<thead>	
					<tr>
						<th width="50px">Aksi</th>
						<th width="10px">No.</th>
						<th>Nama Saham</th>
                        <th>Kode Saham</th>
						<th>Sektor</th>
                        <th>Kinerja Kategori</th>
                        
                        <th>Tanggal Set Saldo Awal</th>
                        <th>Jumlah Lot</th>
                        <th>Jumlah Shares</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
    function checkDelete(){
        return confirm('Yakin Untuk Menghapus?');
    }
function init_datatable()
{
	datatable = $('#datatable').DataTable ({
		'bInfo': true,
		'serverSide': true,
		'serverMethod': 'post',
		'ajax': '<?php echo site_url('/pengaturan/saldoawalsaham/datatable'); ?>',
		'order': [[ 2, 'asc' ]],
		'fixedHeader': true,
		'columns': [
			{
				data: function (row, type, val, meta) {
                    return '' +
                        '<a class="btn btn-action btn-primary" href="saldoawalsaham/ubah/'+row.sham_id+'">'+
                            '<i class="ti ti-pencil-alt"></i>'+
                        '</a>&nbsp;'
						;
                },
				orderable: false,
				className: 'dt-body-center'
			},
            { data: 'no', orderable: false },
			{ data: 'sham_nama', orderable: false },
			{ data: 'sham_kode' },
            { data: 'sektor' },
            { data: 'kategori' },
            
            { data: 'shal_tgl_saldo_awal' ,className: 'dt-body-right'},
			{ data: 'shal_jumlah_lot',className: 'dt-body-right' },
			{ data: 'shal_jumlah_shares',className: 'dt-body-right' },
		]
	});
}

$().ready(function() {
	
	init_datatable();
	
});
</script>