<script type="text/javascript" src="<?=base_url()?>assets/js/saldoawalsaham.js"></script>
<div class="col-md-10 offset-md-1 p-0 mb-8">
	<div class="card">
		<div class="card-header">
			Form Saldo Awal Saham
			<a href="<?php echo site_url('/pengaturan/saldoawalsaham'); ?>" class="btn btn-outline-primary btn-sm btn-header">
				<i class="ti ti-back-left"></i> Kembali
			</a>
		</div>
		<div class="card-body">
			<?php if ($this->session->flashdata('status_simpan') == 'ok'): ?>
			<div class="alert alert-success">Data berhasil disimpan.</div>
			<?php endif; ?>
			
			<?php if ($this->session->flashdata('status_simpan') == 'tidak_lengkap'): ?>
			<div class="alert alert-danger"><?php echo $this->session->flashdata('validation_errors'); ?></div>
			<?php endif; ?>
			
			<form method="post" action="<?php echo $url_aksi; ?>" class="row dk-form dk-form-confirm">
				
				<div class="col-sm-4 col-md-3">
					<br>
					<div class="form-group">
						<label>Tgl. Saldo Awal</label>
						<input type="text" class="form-control tanggalan" id="shal_tgl_saldo_awal" name="shal_tgl_saldo_awal" value="<?php if ($data != null) echo $data->shal_tgl_saldo_awal; else echo date('Y-m-d'); ?>" readonly>
					</div>
					<div class="form-group">
						<label>Akun</label>
						<select class="form-control select2" name="shal_akun_id" id="shal_akun_id" data-placeholder="Pilih Akun" style="width:100%" <?php if ($aksi == 'ubah') echo 'disabled'; ?>>
							<option value=""></option>
							<?=options_akun2($data!=null?$data->shal_akun_id:"")?>
						</select>
					</div>
					
				</div>
				<div class="col-sm-8 col-md-9">
					<div class="mb-3">
						<button class="btn btn-sm btn-primary float-right" type="submit">
							<i class="ti ti-save"></i> Simpan
						</button>
						<div class="clearfix"></div>
					</div>
					<table class="table-cell dk-shal-table" width="100%">
						<thead>
							<tr>
								<th>Saham</th>
								<th width="70px">Jumlah Lot</th>
                                <th width="80px">Jumlah Shares</th>
								<th width="120px">Harga Rata - Rata (Rp)</th>
								<th width="120px">Bruto (Rp)</th>
								<th width="30px">Hapus</th>
							</tr>
						</thead>
						<tbody>
							<?php $saham_null = array(); ?>
							<?php $saham_null['shal_sham_id'][] = ''; ?>
							<?php $saham_null['shal_jumlah_lot'][] = ''; ?>
							<?php $saham_null['shal_jumlah_shares'][] = ''; ?>
							<?php $saham_null['shal_rata_rata_harga'][] = ''; ?>
							<?php $saham_null['shal_total'][] = ''; ?>
					
							<?php $p = $data == null ? $saham_null : $data->saham; ?>
							
							<?php foreach ($p['shal_sham_id'] as $i => $sham_id): ?>
							<tr>
								<td>
									<select class="form-control select2 dk-shal-saham" name="saham[shal_sham_id][]" data-placeholder="Pilih Saham" style="width:100%">
										<option value=""></option>
                                        <?=options_saham($p['shal_sham_id'][$i])?>
									</select>
								</td>
								<td><input type="text" name="saham[shal_jumlah_lot][]" class="control-number-no dk-shal-jumlah-lot" style="width:70px" value="<?php echo $p['shal_jumlah_lot'][$i]; ?>" ></td>
								<td><input type="text" name="saham[shal_jumlah_shares][]" class="control-number-no dk-shal-jumlah-shares" style="width:80px" value="<?php echo $p['shal_jumlah_shares'][$i]; ?>" readonly></td>
								<td><input type="text" name="saham[shal_rata_rata_harga][]" class="control-number-digit dk-shal-rata-rata-harga" style="width:120px" value="<?php echo $p['shal_rata_rata_harga'][$i]; ?>"></td>
								<td><input type="text" name="saham[shal_total][]" class="control-number-digit dk-shal-bruto" style="width:120px" value="<?php echo $p['shal_total'][$i]; ?>" readonly></td>
								
								<td align="center">
									<button class="btn btn-action btn-danger dk-shal-btn-remove">
										<i class="ti ti-trash"></i>
									</button>
								</td>
							</tr>
							<?php endforeach; ?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="3">
									<button class="btn btn-info dk-shal-btn-add">
										<i class="ti ti-plus"></i>
										Tambah Saham
									</button>
									<button class="btn btn-danger" id="HapusSaham" onclick="Hapus()" style="display:none;">
										<i class="ti ti-trash"></i>
										Hapus Saham
									</button>
								</td>
								<td align="right"><strong>TOTAL</strong></td>
								
								<td align="right"><input type="text" value="" name="shal_grand_tot" class="control-number-digit dk-shal-grand-total" style="width:120px" readonly></td>
								<td>&nbsp;</td>
							</tr>
							
						</tfoot>
					</table>
				</div>
			</form>
		</div>
	</div>
</div>

