<div class="col-md-6 offset-md-3 p-0 mb-4">
	<div class="card">
		<div class="card-header">
			Form Saldo Awal Akun
			<a href="<?php echo site_url('/pengaturan/saldoawalrdn'); ?>" class="btn btn-outline-primary btn-sm btn-header">
				<i class="ti ti-back-left"></i> Kembali
			</a>
		</div>
		<div class="card-body">
			<?php if ($this->session->flashdata('status_simpan') == 'ok'): ?>
			<div class="alert alert-success">Data berhasil disimpan.</div>
			<?php endif; ?>
			
			<?php if ($this->session->flashdata('status_simpan') == 'tidak_lengkap'): ?>
			<div class="alert alert-danger"><?php echo $this->session->flashdata('validation_errors'); ?></div>
			<?php endif; ?>
			
			<form method="post" action="<?php echo $url_aksi; ?>">
				<input type="hidden" name="akun_id" value="<?php if ($data != null) echo $data->akun_id; ?>">
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Sekuritas
					</label>
					<div class="col-sm-6 pr-sm-0">
                    <input type="readonly" class="form-control" name="seku_nama" value="<?php if ($data != null) echo $data->seku_nama; ?>">
					</div>
				</div>
                
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Nama Bank RDN
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="readonly" class="form-control" name="akun_bank_rdn" value="<?php if ($data != null) echo $data->akun_bank_rdn; ?>">
					</div>
				</div>
                
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> No Rekening Bank RDN
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="readonly" class="form-control" name="akun_no_rekening_rdn" value="<?php if ($data != null) echo $data->akun_no_rekening_rdn; ?>">
					</div>
				</div>
                
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Tanggal Set Saldo Awal
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="readonly" class="form-control tanggalan" name="swal_tgl_saldo_awal" value="<?php if ($data->swal_tgl_saldo_awal!= null) {echo $data->swal_tgl_saldo_awal;} else{echo date('Y-m-d');} ?>">
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Saldo Awal
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control control-number" name="swal_jumlah_saldo" value="<?php if ($data->swal_jumlah_saldo != null) {echo $data->swal_jumlah_saldo;}else{echo 0;} ?>">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">&nbsp;</label>
					<div class="col-sm-6 pr-sm-0">
						<button type="submit" class="btn btn-primary">Simpan Data</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>