<div class="col-md-6 offset-md-3 p-0">
	<div class="card">
		<div class="card-header">Pengaturan Grup Pengguna</div>
		<div class="card-body">
			<table class="cell-border stripe order-column hover" id="datatable">
				<thead>	
					<tr>
						<th width="60px">Aksi</th>
						<th width="10px">No.</th>
						<th>Nama Grup Pengguna</th>
						<th>Urutan</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
function init_datatable()
{
	datatable = $('#datatable').DataTable ({
		'bInfo': true,
		'serverSide': true,
		'serverMethod': 'post',
		'ajax': '<?php echo site_url('/pengaturan/pengguna-grup/datatable'); ?>',
		'order': [[ 3, 'asc' ]],
		'fixedHeader': true,
		'columns': [
			{
				data: function (row, type, val, meta) {
                    return '' +
                        '<a class="btn btn-action btn-warning" href="<?php echo site_url('/pengaturan/pengguna-grup/akses'); ?>/'+row.id+'">'+
                            '<i class="ti ti-key"></i>'+
                        '</a>&nbsp;'+
						'<a class="btn btn-action btn-primary" href="#">'+
                            '<i class="ti ti-pencil-alt"></i>'+
                        '</a>&nbsp;'+
						'<a class="btn btn-action btn-danger btn-delete" href="#">'+
                            '<i class="ti ti-trash"></i>'+
                        '</a>';
                },
				orderable: false,
				className: 'dt-body-center'
			},
			{ data: 'nomor', orderable: false },
			{ data: 'nama' },
			{ data: 'urutan', className: 'dt-body-right' },
		]
	});
}

$().ready(function() {
	
	init_datatable();
	
});
</script>