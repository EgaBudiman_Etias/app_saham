<div class="col-md-6 offset-md-3 p-0 mb-4">
	<div class="card">
		<div class="card-header">
			Form Pengguna
			<a href="<?php echo site_url('/pengaturan/pengguna'); ?>" class="btn btn-outline-primary btn-sm btn-header">
				<i class="ti ti-back-left"></i> Kembali
			</a>
		</div>
		<div class="card-body">
			<?php if ($this->session->flashdata('status_simpan') == 'tidak_lengkap'): ?>
			<div class="alert alert-danger"><?php echo $this->session->flashdata('validation_errors'); ?></div>
			<?php endif; ?>
			
			<form method="post" action="<?php echo $url_aksi; ?>" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?php if ($data != null) echo $data->id; ?>">
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Nama Pengguna
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="nama" value="<?php if ($data != null) echo $data->nama; ?>">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Alamat Surel
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="surel" value="<?php if ($data != null) echo $data->surel; ?>">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<?php if (! isset($data->id)): ?>
						<span class="text-danger">*</span>
						<?php endif; ?>
						Kata Sandi
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="kata_sandi" placeholder="<?php if (isset($data->id)) echo 'Isi jika ingin mengubah kata sandi'; ?>">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">Foto KTP</label>
					<div class="col-sm-6 pr-sm-0">
						<div class="custom-file">
							<input type="file" class="custom-file-input" id="foto-ktp" name="foto_ktp" accept=".jpg,.jpeg,.png,.gif,.bmp">
							<label class="custom-file-label" for="foto-ktp">Pilih file</label>
						</div>
						<img src="<?php echo base_url(); ?>uploads/pengguna/<?php echo $data == null ? 'no-image.jpg' : $data->foto_ktp; ?>" width="100%" class="rounded mt-2" id="pratinjau-ktp">
						<input type="hidden" name="file_foto_ktp" value="<?php echo $data == null ? 'no-image.jpg' : $data->foto_ktp; ?>">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">No. KTP</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="no_ktp" value="<?php if ($data != null) echo $data->no_ktp; ?>">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">Alamat</label>
					<div class="col-sm-6 pr-sm-0">
						<textarea class="form-control" name="alamat"><?php if ($data != null) echo $data->alamat; ?></textarea>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">No. HP / WhatsApp</label>
					<div class="col-sm-3 pr-sm-0">
						<input type="text" class="form-control" name="no_hp" value="<?php if ($data != null) echo $data->no_hp; ?>">
					</div>
					<div class="col-sm-3 pr-sm-0">
						<input type="text" class="form-control" name="no_wa" value="<?php if ($data != null) echo $data->no_wa; ?>">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">Nama Bank</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="bank" value="<?php if ($data != null) echo $data->bank; ?>">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">No. Rekening</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="no_rekening" value="<?php if ($data != null) echo $data->no_rekening; ?>">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">Nama Pemilik Rekening</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="pemilik_rekening" value="<?php if ($data != null) echo $data->pemilik_rekening; ?>">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Grup Pengguna
					</label>
					<div class="col-sm-6 pr-sm-0">
						<select class="form-control" name="id_pengguna_grup">
							<option value="">- Pilih Grup Pengguna -</option>
							<?php echo modules::run('options/pengguna_grup', $data == null ? null : $data->id_pengguna_grup); ?>
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">&nbsp;</label>
					<div class="col-sm-6 pr-sm-0">
						<button type="submit" class="btn btn-primary">Simpan Data</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
$().ready(function() {
	
	$('#foto-ktp').change(function() {
		var file = $(this).get(0).files[0];
		var nama_file = file.name;
		
		if (file) {
			var reader = new FileReader();
			
			reader.onload = function() {
				$('#pratinjau-ktp').attr('src', reader.result);
			}
			
			reader.readAsDataURL(file);
			
			$('.custom-file-label[for=foto-ktp]').text(nama_file);
		}
	});
	
});
</script>