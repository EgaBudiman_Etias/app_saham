<div class="col-md-6 offset-md-3 p-0 mb-4">
	<div class="card">
		<div class="card-header">
			Pajak
			<!-- <a href="<?php echo site_url('/master/refpilihan'); ?>" class="btn btn-outline-primary btn-sm btn-header">
				<i class="ti ti-back-left"></i> Kembali
			</a> -->
		</div>
        <?php
            // $a='0.34';
            // echo str_replace(',', '.', $a);
        ?>
		<div class="card-body">
			<?php if ($this->session->flashdata('status_simpan') == 'ok'): ?>
			<div class="alert alert-success">Data berhasil disimpan.</div>
			<?php endif; ?>
			
			<?php if ($this->session->flashdata('status_simpan') == 'tidak_lengkap'): ?>
			<div class="alert alert-danger"><?php echo $this->session->flashdata('validation_errors'); ?></div>
			<?php endif; ?>
			
			<form method="post" action="<?php echo $url_aksi; ?>">
				
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Pajak Penjualan (%)
					</label>
					<div class="col-sm-6 pr-sm-0">
                        <input type="hidden" name="konf-id-pajak-penjualan" value="<?=$pajak_penjualan->konf_id?>">
                        <input type="text"  class="form-control" name="konf-nilai-pajak-penjualan" value="<?=number_format($pajak_penjualan->konf_nilai,2,'.','')?>">
                        
					</div>
				</div>
                
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Pajak Deviden (%)
					</label>
					<div class="col-sm-6 pr-sm-0">
                        <input type="hidden" name="konf-id-pajak-deviden" value="<?=$pajak_deviden->konf_id?>">
                        <input type="text" class="form-control"  name="konf-nilai-pajak-deviden" value="<?=number_format($pajak_deviden->konf_nilai,2,'.','')?>">
                        
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">&nbsp;</label>
					<div class="col-sm-6 pr-sm-0">
						<button type="submit" class="btn btn-primary">Ubah Data</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>