<div class="col-md-12 offset-md-0 p-0">
	<div class="card">
		<div class="card-header">
			Pengaturan Saldo Awal Akun
			
		</div>
		<div class="card-body">
			<table class="cell-border stripe order-column hover" id="datatable">
				<thead>	
					<tr>
						<th width="50px">Aksi</th>
						<th width="10px">No.</th>
						<th>Nama Sekuritas</th>
                        <th>Bank RDN</th>
                        <th>No Rekening RDN</th>
                        <th>Tanggal Set Saldo Awal</th>
                        <th>Jumlah Saldo Awal</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
    function checkDelete(){
        return confirm('Yakin Untuk Menghapus?');
    }
function init_datatable()
{
	datatable = $('#datatable').DataTable ({
		'bInfo': true,
		'serverSide': true,
		'serverMethod': 'post',
		'ajax': '<?php echo site_url('/pengaturan/saldoawalrdn/datatable'); ?>',
		'order': [[ 2, 'asc' ]],
		'fixedHeader': true,
		'columns': [
			{
				data: function (row, type, val, meta) {
                    return '' +
                        '<a class="btn btn-action btn-primary" href="saldoawalrdn/ubah/'+row.akun_id+'">'+
                            '<i class="ti ti-pencil-alt"></i>'+
                        '</a>&nbsp;';
                },
				orderable: false,
				className: 'dt-body-center'
			},
            { data: 'no', orderable: false },
			{ data: 'seku_nama', orderable: false },
            { data: 'akun_bank_rdn'},
            { data: 'akun_no_rekening_rdn'},
            { data: 'swal_tgl_saldo_awal',className: 'dt-body-right'},
            { data: 'swal_jumlah_saldo',className: 'dt-body-right'},
		]
	});
}

$().ready(function() {
	
	init_datatable();
	
});
</script>