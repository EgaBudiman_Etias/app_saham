<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pajak extends MX_Controller {

	public function index()
	{
        $pajak_penjualan=$this->db->from('konfigurasi')->where('konf_kode','pajak_penjualan')->get()->row();
        $pajak_deviden=$this->db->from('konfigurasi')->where('konf_kode','pajak_deviden')->get()->row();
		$this->load->view('templates/site_tpl', array (
			'content' => 'pajak_index',
            'url_aksi' => site_url("/pengaturan/pajak/ubah_data"),
            'pajak_penjualan'=>$pajak_penjualan,
            'pajak_deviden'=>$pajak_deviden,
		));
	}
	
    public function ubah_data(){
        $validasi = array (
			array (
				'field' => 'konf-nilai-pajak-penjualan',
				'label' => 'Pajak Penjualan',
				'rules' => 'required|decimal',
			),
			array (
				'field' => 'konf-nilai-pajak-deviden',
				'label' => 'Pajak Deviden',
				'rules' => 'required|decimal',
			),
			
		);
        $this->form_validation->set_rules($validasi);
		
		if ($this->form_validation->run()) {
			
			$konf_id_pajak_penjualan=$this->input->post('konf-id-pajak-penjualan',true);
            $where = array('konf_id' => $konf_id_pajak_penjualan);
            $data_pajak_penjualan=array(
                'konf_nilai'=>$this->input->post('konf-nilai-pajak-penjualan',true),
                'konf_updated_time'=>date('Y-m-d H:i:s'),
                'konf_updated_by'=>session_pengguna('peng_id'),
            );
            $this->db->update('konfigurasi', $data_pajak_penjualan, $where);
			$this->session->set_flashdata('status_simpan', 'ok');

            $konf_id_pajak_deviden=$this->input->post('konf-id-pajak-deviden',true);
            $where = array('konf_id' => $konf_id_pajak_deviden);
            $data_pajak_deviden=array(
                'konf_nilai'=>$this->input->post('konf-nilai-pajak-deviden',true),
                'konf_updated_time'=>date('Y-m-d H:i:s'),
                'konf_updated_by'=>session_pengguna('peng_id'),
            );
            $this->db->update('konfigurasi', $data_pajak_deviden, $where);
            $this->session->set_flashdata('status_simpan', 'ok');

		}
		else {
			$this->session->set_flashdata('status_simpan', 'tidak_lengkap');
			$this->session->set_flashdata('validation_errors', validation_errors());
		}
        redirect(site_url('/pengaturan/pajak'));
    }

    public function ajax_pajak_deviden(){
		$this->load->model('transaksi/pembelian_model');
		$akun_id = $this->input->post('akun_id');
		$sham_id=$this->input->post('sham_id');
		$jumlahsaham=$this->pembelian_model->carisaldosaham($akun_id,$sham_id);
		$shares=$jumlahsaham*100;
		$src = $this->db
			->select('*')
			->from('konfigurasi')
			->where('konf_kode', 'pajak_deviden')
			->get()->row();
		$hasil=array(
			'konf_nilai'=>$src->konf_nilai,
			'saldosaham'=>$jumlahsaham,
			'shares'=>$shares,
		);
		header('Content-Type: application/json');
		echo json_encode($hasil);
    }
}
