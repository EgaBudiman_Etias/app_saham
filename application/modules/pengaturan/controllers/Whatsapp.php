<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Whatsapp extends MX_Controller {

	public function index()
	{
		$this->load->view('templates/site_tpl', array (
			'content' => 'whatsapp_index',
		));
	}
	
	public function coba_wa()
	{
		$no_wa_tujuan = $this->input->post('no_wa_tujuan');
		wablas_kirim($no_wa_tujuan);
	}
	
}
