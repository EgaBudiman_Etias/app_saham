<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Saldoawalsaham extends MX_Controller {

	public function index()
	{
       $this->_form('tambah');
	}

	
	public function datatable()
	{
		$draw = $this->input->post('draw');
		$offset = $this->input->post('start');
		$num_rows = $this->input->post('length');
		$order_index = $_POST['order'][0]['column'];
		$order_by = $_POST['columns'][$order_index]['data'];
		$order_direction = $_POST['order'][0]['dir'];
		$keyword = $_POST['search']['value'];
		
		$bindings = array("%{$keyword}%","%{$keyword}%");
		
		$base_sql = "
			from saham
            JOIN sekuritas on(sham_seku_id=seku_id)
			LEFT JOIN saldo_awal_saham on (shal_sham_id=sham_id)
			JOIN ref_pilihan a on (sham_sektor=a.plhn_id)
			join ref_pilihan b on(sham_kinerja_kategori=b.plhn_id)
			
            where
				sham_is_deleted = '1'
				and (
					sham_nama like ?
                    or seku_nama like ?
				)
		";
		
		$data_sql = "
			select
				saham.*,seku_nama,shal_sham_id,date_format(shal_tgl_saldo_awal,'%d %M %Y') 
				as shal_tgl_saldo_awal,coalesce(shal_jumlah_lot,0) as shal_jumlah_lot,
				coalesce(shal_jumlah_shares,0) as shal_jumlah_shares,
				a.plhn_nama as sektor,b.plhn_nama as kategori
				, row_number() over (
					order by
						{$order_by} {$order_direction}
						, sham_nama {$order_direction}
				  ) as nomor
			{$base_sql}
			order by
				{$order_by} {$order_direction}
				, sham_id {$order_direction}
			limit {$offset}, {$num_rows}
		";
                    
		$src = $this->db->query($data_sql, $bindings);
		// echo $this->db->last_query();
        // die();
		$count_sql = "
			select count(*) AS total
			{$base_sql}
		";
		$total_records = $this->db->query($count_sql, $bindings)->row('total');
		
		$data = array();
		
		foreach ($src->result() as $row) {
			$data[] = array (
				'sham_nama' => $row->sham_nama,
				'sham_id' => $row->sham_id,
				'sham_kode' => $row->sham_kode,
				'sham_sektor' => $row->sham_sektor,
				'sektor' => $row->sektor,
				'kategori' => $row->kategori,
                'seku_nama'=>$row->seku_nama,
                'no'=>$row->nomor,
				'shal_sham_id'=>$row->shal_sham_id,
				'shal_tgl_saldo_awal'=>$row->shal_tgl_saldo_awal,
				'shal_jumlah_lot'=>angka($row->shal_jumlah_lot),
				'shal_jumlah_shares'=>angka($row->shal_jumlah_shares),
			);
		}
		
		$response = array (
			'draw' => intval($draw),
			'iTotalRecords' => $src->num_rows(),
			'iTotalDisplayRecords' => $total_records,
			'aaData' => $data,
		);
		
		echo json_encode($response);
	}

	private function _form($aksi = 'tambah', $data = null)
	{
		if ($this->session->flashdata('data_form')) {
			$data = $this->session->flashdata('data_form');
		}
		
		$this->load->view('templates/site_tpl', array (
			'content' => 'saldoawalsaham_form',
			'url_aksi' => site_url("/pengaturan/saldoawalsaham/{$aksi}-data"),
			'data' => $data,
		));
	}

	private function _data_form()
	{
		$validasi = array (
			array (
				'field' => 'shal_tgl_saldo_awal',
				'label' => 'Tanggal Saldo Awal',
				'rules' => 'required',
			),
			array (
				'field' => 'shal_akun_id',
				'label' => 'Akun',
				'rules' => 'required',
			),
			
		);
		
		$this->form_validation->set_rules($validasi);
		
		if ($this->form_validation->run()) {
			
			$kunci_data = array (
				'shal_tgl_saldo_awal',
				'shal_akun_id',
			);
			
			return data_post($kunci_data);
		}
		else {
			$this->session->set_flashdata('status_simpan', 'tidak_lengkap');
			$this->session->set_flashdata('validation_errors', validation_errors());
			$this->session->set_flashdata('data_form', (object) $this->input->post());
			return null;
		}
	}
	public function ajax_saham(){
		$akun_id = $this->input->post('akun_id');
		$sham_id = $this->input->post('sham_id');
		$src = $this->db
			->select('*')
			->from('saldo_awal_saham')
			->where(array('shal_akun_id'=>$akun_id,'shal_sham_id'=>$sham_id))
			->get();
		// var_dump($this->db->last_query());
		header('Content-Type: application/json');
		echo json_encode($src->row());
	}

	private function _data_detail()
	{
		$saham = $this->input->post('saham');
		
		$shal_akun_id=$this->input->post('shal_akun_id');
		$shal_tgl_saldo_awal=$this->input->post('shal_tgl_saldo_awal');
		$detail = array();
		
		foreach ($saham['shal_sham_id'] as $i => $shal_sham_id) {
			$detail[] = array (
				'shal_sham_id' => $saham['shal_sham_id'][$i],
				'shal_akun_id' => $shal_akun_id,
				'shal_tgl_saldo_awal' => $shal_tgl_saldo_awal,
				'shal_jumlah_lot' => strip_num_separator($saham['shal_jumlah_lot'][$i]),
				'shal_jumlah_shares' => strip_num_separator($saham['shal_jumlah_shares'][$i]),
				'shal_rata_rata_harga' => strip_num_separator($saham['shal_rata_rata_harga'][$i]),
				'shal_total' => strip_num_separator($saham['shal_total'][$i]),
				'shal_created_time' => date('Y-m-d H:i:s'),
				'shal_created_by' => session_pengguna('peng_id'),
			);
		}
		
		return $detail;
	}

	private function _data_detail_saham()
	{
		$saham = $this->input->post('saham');
		
		$shal_akun_id=$this->input->post('shal_akun_id');
		$shal_tgl_saldo_awal=$this->input->post('shal_tgl_saldo_awal');
		$detail = array();
		
		foreach ($saham['shal_sham_id'] as $i => $shal_sham_id) {
			$detail[] = array (
				'kshm_sham_id' => $saham['shal_sham_id'][$i],
				'kshm_akun_id' => $shal_akun_id,
				'kshm_tgl_transaksi' => $shal_tgl_saldo_awal,
				'kshm_jns_transaksi'=>'saldoawal',
				'kshm_jumlah' => strip_num_separator($saham['shal_jumlah_lot'][$i]),
				
				'kshm_harga_rata_rata' => strip_num_separator($saham['shal_rata_rata_harga'][$i]),
				'kshm_harga_rata_rata_2' => strip_num_separator($saham['shal_rata_rata_harga'][$i]),
				'kshm_harga_netto' => strip_num_separator($saham['shal_total'][$i]),
				'kshm_created_time' => date('Y-m-d H:i:s'),
				'kshm_created_by' => session_pengguna('peng_id'),
			);
		}
		
		return $detail;
	}

	public function delete(){
		$akun=$this->input->post('akun_id');
		$delete=$this->db->delete('saldo_awal_saham',array('shal_akun_id'=>$akun));
		header('Content-Type: application/json');
		echo json_encode(array('pesan'=>'sukses'));
	}
	public function tambah_data()
	{
		$data = $this->_data_detail();
		$datakeuangansaham=$this->_data_detail_saham();
		$shal_akun_id=$this->input->post('shal_akun_id');
		if ($data != null) {
			
			$cek_record=$this->db->from('saldo_awal_saham')
								->where(array('shal_akun_id'=>$shal_akun_id))
								->get()->row();
			if(empty($cek_record)){
				//masukkan ke saldo awal_saham
				$this->db->insert_batch('saldo_awal_saham', $data);
				$this->db->insert_batch('keuangan_saham', $datakeuangansaham);
				$this->session->set_flashdata('status_simpan', 'ok');
				
			}else{
				$this->db->delete('saldo_awal_saham', array('shal_akun_id' => $shal_akun_id));
				$this->db->delete('keuangan_saham', array('kshm_akun_id' => $shal_akun_id,'kshm_jns_transaksi'=>'saldoawal'));
				$this->db->insert_batch('saldo_awal_saham', $data);
				$this->db->insert_batch('keuangan_saham', $datakeuangansaham);
				$this->session->set_flashdata('status_simpan', 'ok');
			}
		}else{
			$cek_record=$this->db->from('saldo_awal_saham')
								->where(array('shal_akun_id'=>$shal_akun_id))
								->get()->row();
			if(empty($cek_record)){
				
				
			}else{
				$this->db->delete('saldo_awal_saham', array('shal_akun_id' => $shal_akun_id));
				$this->db->delete('keuangan_saham', array('kshm_akun_id' => $shal_akun_id,'kshm_jns_transaksi'=>'saldoawal'));
				$this->session->set_flashdata('status_simpan', 'ok');
			}
		}
		
		redirect(site_url('/pengaturan/saldoawalsaham'));
	}
}