<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sistem extends MX_Controller {

	public function index()
	{
		$src = $this->db
			->from('konfigurasi')
			->where("kode not like 'notifikasi_%'")
			->where('is_hapus', 0)
			->order_by('nama')
			->get();
		
		$konfigurasi = array();
		
		foreach ($src->result() as $row) {
			$konfigurasi[$row->kode] = array (
				'nama' => $row->nama,
				'tipe_data' => $row->tipe_data,
				'nilai' => $row->nilai,
			);
		}
		
		$src = $this->db
			->from('konfigurasi')
			->where("kode like 'notifikasi_%'")
			->where('is_hapus', 0)
			->order_by('nama')
			->get();
		
		$notifikasi = array();
		
		foreach ($src->result() as $row) {
			$notifikasi[$row->kode] = array (
				'nama' => $row->nama,
				'tipe_data' => $row->tipe_data,
				'nilai' => explode(',', $row->nilai),
			);
		}
		
		$this->load->view('templates/site_tpl', array (
			'content' => 'sistem_index',
			'konfigurasi' => $konfigurasi,
			'notifikasi' => $notifikasi,
		));
	}
	
	public function simpan()
	{
		$kode_arr = $this->input->post('kode');
		
		$data = array();
		
		foreach ($kode_arr as $kode_tipe_data => $nilai) {
			
			$kode_tipe_data_arr = explode('|', $kode_tipe_data);
			
			$kode = $kode_tipe_data_arr[0];
			$nilai = trim($nilai);
			
			if (isset($kode_tipe_data_arr[1])) {
				if ($kode_tipe_data_arr[1] == 'numerik') {
					$nilai = str_replace('.', '', $nilai);
				}
			}
			
			$data = array (
				'nilai' => $nilai,
				'waktu_ubah' => date('Y-m-d H:i:s'),
				'id_pengguna_ubah' => session_pengguna('id'),
			);
			
			$where = array('kode' => $kode);
			
			$this->db->update('konfigurasi', $data, $where);
		}
		
		#
		# NOTIFIKASI
		#
		
		$notifikasi_registrasi_baru = $this->input->post('notifikasi_registrasi_baru');
		$notifikasi_pembayaran_masuk = $this->input->post('notifikasi_pembayaran_masuk');
		$notifikasi_invoice_jatuh_tempo = $this->input->post('notifikasi_invoice_jatuh_tempo');
		
		if ($notifikasi_registrasi_baru != null) {
			$nilai = implode(',', $notifikasi_registrasi_baru);
			
			$data = array (
				'nilai' => $nilai,
				'waktu_ubah' => date('Y-m-d H:i:s'),
				'id_pengguna_ubah' => session_pengguna('id'),
			);
			
			$where = array('kode' => 'notifikasi_registrasi_baru');
			
			$this->db->update('konfigurasi', $data, $where);
		}
		
		if ($notifikasi_pembayaran_masuk != null) {
			$nilai = implode(',', $notifikasi_pembayaran_masuk);
			
			$data = array (
				'nilai' => $nilai,
				'waktu_ubah' => date('Y-m-d H:i:s'),
				'id_pengguna_ubah' => session_pengguna('id'),
			);
			
			$where = array('kode' => 'notifikasi_pembayaran_masuk');
			
			$this->db->update('konfigurasi', $data, $where);
		}
		
		if ($notifikasi_invoice_jatuh_tempo != null) {
			$nilai = implode(',', $notifikasi_invoice_jatuh_tempo);
			
			$data = array (
				'nilai' => $nilai,
				'waktu_ubah' => date('Y-m-d H:i:s'),
				'id_pengguna_ubah' => session_pengguna('id'),
			);
			
			$where = array('kode' => 'notifikasi_invoice_jatuh_tempo');
			
			$this->db->update('konfigurasi', $data, $where);
		}
		
		redirect($this->agent->referrer());
	}
	
}
