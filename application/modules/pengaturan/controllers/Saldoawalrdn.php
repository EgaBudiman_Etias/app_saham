<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Saldoawalrdn extends MX_Controller {

	public function index()
	{
       
		$this->load->view('templates/site_tpl', array (
			'content' => 'saldoawalrdn_index',
            
		));
	}

	public function datatable()
	{
		$draw = $this->input->post('draw');
		$offset = $this->input->post('start');
		$num_rows = $this->input->post('length');
		$order_index = $_POST['order'][0]['column'];
		$order_by = $_POST['columns'][$order_index]['data'];
		$order_direction = $_POST['order'][0]['dir'];
		$keyword = $_POST['search']['value'];
		
		$bindings = array("%{$keyword}%","%{$keyword}%","%{$keyword}%");
		
		$base_sql = "
			from akun
            JOIN sekuritas on(akun_seku_id=seku_id)
			LEFT JOIN saldo_awal on (swal_akun_id=akun_id)
            where
				akun_is_deleted = '1'
				and (
					seku_nama like ?
                    or akun_kode_nasabah like ?
                    or akun_no_sid like ?
				)
		";
		
		$data_sql = "
			select
				akun.*,seku_nama,swal_akun_id,date_format(swal_tgl_saldo_awal,'%d %M %Y') as swal_tgl_saldo_awal,coalesce(swal_jumlah_saldo,0) as swal_jumlah_saldo
				, row_number() over (
					order by
						{$order_by} {$order_direction}
						, akun_kode_nasabah {$order_direction}
				  ) as nomor
			{$base_sql}
			order by
				{$order_by} {$order_direction}
				, akun_id {$order_direction}
			limit {$offset}, {$num_rows}
		";
                    
		$src = $this->db->query($data_sql, $bindings);
		// echo $this->db->last_query();
        // die();
		$count_sql = "
			select count(*) AS total
			{$base_sql}
		";
		$total_records = $this->db->query($count_sql, $bindings)->row('total');
		
		$data = array();
		
		foreach ($src->result() as $row) {
			$data[] = array (
				'seku_nama' => $row->seku_nama,
				'akun_kode_nasabah' => $row->akun_kode_nasabah,
				'akun_no_sid' => $row->akun_no_sid,
				'akun_no_ksei_kpei' => $row->akun_no_ksei_kpei,
				'akun_bank_rdn' => $row->akun_bank_rdn,
				'akun_bank_cab_rdn' => $row->akun_bank_cab_rdn,
                'akun_no_rekening_rdn' => $row->akun_no_rekening_rdn,
                'akun_email' => $row->akun_email,
                'akun_id'=>$row->akun_id,
                'no'=>$row->nomor,
				'swal_akun_id'=>$row->swal_akun_id,
				'swal_tgl_saldo_awal'=>$row->swal_tgl_saldo_awal,
				'swal_jumlah_saldo'=>rupiah($row->swal_jumlah_saldo),
			);
		}
		
		$response = array (
			'draw' => intval($draw),
			'iTotalRecords' => $src->num_rows(),
			'iTotalDisplayRecords' => $total_records,
			'aaData' => $data,
		);
		
		echo json_encode($response);
	}

	public function ubah($id = '')
	{
		
		if ( ! $this->agent->referrer()) {
			show_404();
		}
		
		$src = $this->db
			->from('akun')
			->join('saldo_awal','swal_akun_id=akun_id','LEFT')
			->join('sekuritas','seku_id=akun_seku_id')
			->where('akun_is_deleted', '1')
			->where('akun_id', $id)
			->get();
			// echo $this->db->last_query();
			// die();
		if ($src->num_rows() == 0) {
			show_404();
		}
		
		$this->_form('ubah', $src->row());
	}

	private function _form($aksi = 'tambah', $data = null)
	{
		if ($this->session->flashdata('data_form')) {
			$data = $this->session->flashdata('data_form');
		}
		
		$this->load->view('templates/site_tpl', array (
			'content' => 'saldoawalrdn_form',
			'url_aksi' => site_url("/pengaturan/saldoawalrdn/{$aksi}-data"),
			'data' => $data,
		));
	}

	private function _data_form()
	{
		$validasi = array (
			array (
				'field' => 'swal_tgl_saldo_awal',
				'label' => 'Tanggal Saldo Awal',
				'rules' => 'required',
			),
			array (
				'field' => 'swal_jumlah_saldo',
				'label' => 'Saldo Awal',
				'rules' => 'required',
			),
			
		);
		
		$this->form_validation->set_rules($validasi);
		
		if ($this->form_validation->run()) {
			
			$kunci_data = array (
				'swal_tgl_saldo_awal',
				'swal_jumlah_saldo|number',
				
			);
			
			return data_post($kunci_data);
		}
		else {
			$this->session->set_flashdata('status_simpan', 'tidak_lengkap');
			$this->session->set_flashdata('validation_errors', validation_errors());
			$this->session->set_flashdata('data_form', (object) $this->input->post());
			return null;
		}
	}

	public function ubah_data()
	{
		$data = $this->_data_form();
		$akun_id=$this->input->post('akun_id');
		
		if ($data != null) {
			
			$cek_record=$this->db->from('saldo_awal')->where(array('swal_akun_id'=>$akun_id))->get()->row();
			if(empty($cek_record)){
				$data['swal_created_by'] = $data['swal_updated_by'] = session_pengguna('peng_id');
            	$data['swal_created_time']=$data["swal_updated_time"] =date('Y-m-d H:i:s');
				$data['swal_akun_id']=$akun_id;
				$this->db->insert('saldo_awal', $data);
				$this->session->set_flashdata('status_simpan', 'ok');
			}else{
				$data['swal_updated_time'] = date('Y-m-d H:i:s');
				$data['swal_updated_by'] = session_pengguna('peng_id');
				$where = array('swal_akun_id' => $akun_id);
				$this->db->update('saldo_awal', $data, $where);
				$this->session->set_flashdata('status_simpan', 'ok');
			}
		}
		
		redirect(site_url('/pengaturan/saldoawalrdn'));
	}
}