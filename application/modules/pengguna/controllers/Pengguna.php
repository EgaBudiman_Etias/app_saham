<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengguna extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->form_validation->CI =& $this;
	}
	
	public function masuk()
	{
		$this->load->model('pengguna_grup_model');
		
		$peng_nama = $this->input->post('peng_nama');
		$kata_sandi = $this->input->post('peng_kata_sandi');
		// echo $peng_nama;
		// echo $kata_sandi;
		// die();
		$hash_kata_sandi = hash_kata_sandi($kata_sandi);
		
		$src = $this->db
			->select('
				peng_id,peng_nama,peng_cookie,peng_nama_lengkap
			')
			->from('pengguna')
			
			->where("'{$peng_nama}' in (peng_nama)")
			->where('peng_kata_sandi', $hash_kata_sandi)
			->where('peng_is_deleted', 1)
			
			->get();
			// echo $this->db->last_query();die();
		if ($src->num_rows() == 0) {
			$this->session->set_flashdata('status_masuk', 'gagal');
			redirect(site_url());
		}
		else {
			$pengguna = $src->row();
			// var_dump($pengguna);
			// die();
			$menu = $this->pengguna_grup_model->menu($pengguna->peng_id);
			// var_dump($menu);
			// die();
			$this->session->set_userdata('pengguna', $pengguna);
			$this->session->set_userdata('menu', $menu);
			
			if ($this->input->post('ingat_saya')) {
				$isi_cookie = md5('saham-cookie-'.date('YmdHis').'-'.mt_rand(100, 300));
				$hari_kadaluarsa = 30;
				
				$cookie = array (
					'name' => 'saham_cookie',
					'value' => $isi_cookie,
					'expire' => $hari_kadaluarsa * 24 * 3600,
				);
				
				$this->input->set_cookie($cookie);
				$this->db->update('pengguna', array('peng_cookie' => $isi_cookie), array('peng_id' => $pengguna->peng_id));
				
			}
			
			redirect(site_url('/dasboard'));
		}
	}
	
	public function profil()
	{
		$id = session_pengguna('peng_id');
		
		if ($this->session->flashdata('data_form')) {
			$data = $this->session->flashdata('data_form');
		}
		else {
			$src = $this->db
				->from('pengguna')
				->where('peng_is_deleted', 1)
				->where('peng_id', $id)
				->get();
		
			if ($src->num_rows() == 0) {
				show_404();
			}
			else {
				$data = $src->row();
			}
		}
		
		$this->load->view('templates/site_tpl', array (
			'content' => 'pengguna_profil',
			'data' => $data,
		));
	}
	
	public function simpan_profil()
	{
		$validasi = array (
			array (
				'field' => 'peng_nama_lengkap',
				'label' => 'Nama Pengguna',
				'rules' => 'required',
			),
			
		);
		
		$this->form_validation->set_rules($validasi);
		
		if ($this->form_validation->run()) {
			
			$kunci_data = array (
				'peng_nama_lengkap',
				
			);
			
			$data = data_post($kunci_data);
			
			
			$data['peng_updated_time'] = date('Y-m-d H:i:s');
			$data['peng_updated_by'] = session_pengguna('peng_id');
			
			$where = array('peng_id' => session_pengguna('peng_id'));
			
			$this->db->update('pengguna', $data, $where);
			
			$this->session->userdata('pengguna')->peng_nama_lengkap = $data['peng_nama_lengkap'];
			
			
			$this->session->set_flashdata('status_simpan', 'ok');
		}
		else {
			$this->session->set_flashdata('status_simpan', 'tidak_lengkap');
			$this->session->set_flashdata('validation_errors', validation_errors());
			$this->session->set_flashdata('data_form', (object) $this->input->post());
		}
		
		redirect($this->agent->referrer());
	}
	
	
	
	public function ubah_kata_sandi()
	{
		$this->load->view('templates/site_tpl', array (
			'content' => 'pengguna_ubah_kata_sandi',
		));
	}
	
	public function cek_kata_sandi($kata_sandi)
	{
		$id = session_pengguna('peng_id');
		$hash_kata_sandi = hash_kata_sandi($kata_sandi);
		
		$src = $this->db
			->from('pengguna')
			->where('peng_is_deleted', 1)
			->where('peng_id', $id)
			->where('peng_kata_sandi', $hash_kata_sandi)
			->get();
		
		if ($src->num_rows() == 0) {
			$this->form_validation->set_message('cek_kata_sandi', '<b>%s</b> salah.');
			return false;
		}
		else {
			return true;
		}
	}
	
	public function simpan_kata_sandi()
	{
		$validasi = array (
			array (
				'field' => 'kata_sandi_sekarang',
				'label' => 'Kata Sandi Sekarang',
				'rules' => 'required|callback_cek_kata_sandi',
			),
			array (
				'field' => 'kata_sandi_baru',
				'label' => 'Kata Sandi Baru',
				'rules' => 'required',
			),
		);
		
		$this->form_validation->set_rules($validasi);
		
		if ($this->form_validation->run()) {
			
			$id_pengguna = session_pengguna('peng_id');
			$hash_kata_sandi_baru = hash_kata_sandi(trim($this->input->post('kata_sandi_baru')));
			
			$data = array (
				'peng_kata_sandi' => $hash_kata_sandi_baru,
				'peng_updated_time' => date('Y-m-d H:i:s'),
				'peng_updated_by' => $id_pengguna,
			);
			
			$where = array (
				'peng_id' => $id_pengguna,
			);
			
			$this->db->update('pengguna', $data, $where);
			$this->session->set_flashdata('status_simpan', 'ok');
		}
		else {
			$this->session->set_flashdata('status_simpan', 'tidak_lengkap');
			$this->session->set_flashdata('validation_errors', validation_errors());
		}
		
		redirect(site_url('/pengguna/ubah-kata-sandi'));
	}
	
	public function keluar()
	{
		$id_pengguna = session_pengguna('peng_id');
		$data = array (
			'peng_cookie' =>null,
		);
		
		$where = array (
			'peng_id' => $id_pengguna,
		);
			
		$this->db->update('pengguna', $data, $where);
		$this->session->unset_userdata('pengguna');
		$this->session->unset_userdata('menu');
		$this->session->sess_destroy();
		
		delete_cookie('saham_cookie');
		
		redirect(site_url());
	}
	
}
