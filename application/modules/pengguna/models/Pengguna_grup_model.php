<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengguna_grup_model extends CI_Model {

	public function menu($grup_peng_id)
	{
		$sql = "
			select b.*
			from (
				select grup_menu_id
				from pengguna_grup_menu
				where
					grup_peng_id = ?
					and grup_is_deleted = '1'
			) as a
			join menu as b on
				a.grup_menu_id = b.menu_id
				and menu_is_deleted = '1'
			order by menu_nomor
			
		";
		$src = $this->db->query($sql, array($grup_peng_id));
		
		$menu = array();
		
		foreach ($src->result() as $row) {
			$val = array (
				'kode' => $row->menu_kode,
				'ikon' => $row->menu_ikon,
				'teks' => $row->menu_teks,
				'uri' => $row->menu_uri,
			);
			
			if ($row->menu_id_induk == null) {
				$menu[$row->menu_id] = $val;
			}
			else {
				$menu[$row->menu_id_induk]['sub'][] = $val;
			}
		}
		
		return $menu;
	}
	
}
