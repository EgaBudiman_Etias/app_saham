<div class="col-md-6 offset-md-3 p-0 mb-4">
	<div class="card">
		<div class="card-header">Profil Pengguna</div>
		<div class="card-body">
			<?php if ($this->session->flashdata('status_simpan') == 'ok'): ?>
			<div class="alert alert-success">Profil berhasil diubah.</div>
			<?php endif; ?>
			
			<?php if ($this->session->flashdata('status_simpan') == 'tidak_lengkap'): ?>
			<div class="alert alert-danger"><?php echo $this->session->flashdata('validation_errors'); ?></div>
			<?php endif; ?>
			
			<form method="post" action="<?php echo site_url('/pengguna/simpan-profil'); ?>" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?php if ($data != null) echo $data->peng_id; ?>">
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Nama Pengguna
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="peng_nama_lengkap" value="<?php if ($data != null) echo $data->peng_nama_lengkap; ?>">
					</div>
				</div>
				
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">&nbsp;</label>
					<div class="col-sm-6 pr-sm-0">
						<button type="submit" class="btn btn-primary">Ubah Profil</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
$().ready(function() {
	
	$('#foto-ktp').change(function() {
		var file = $(this).get(0).files[0];
		var nama_file = file.name;
		
		if (file) {
			var reader = new FileReader();
			
			reader.onload = function() {
				$('#pratinjau-ktp').attr('src', reader.result);
			}
			
			reader.readAsDataURL(file);
			
			$('.custom-file-label[for=foto-ktp]').text(nama_file);
		}
	});
	
});
</script>