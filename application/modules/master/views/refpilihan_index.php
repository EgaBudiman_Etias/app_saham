<div class="col-md-6 offset-md-3 p-0">
	<div class="card">
		<div class="card-header">
			Master Ref-Pilihan
			<a href="refpilihan/tambah" class="btn btn-primary btn-sm btn-header">
				<i class="ti ti-write"></i> Tambah Data
			</a>
		</div>
		<div class="card-body">
			<table class="cell-border stripe order-column hover" id="datatable">
				<thead>	
					<tr>
						<th width="50px">Aksi</th>
						<th width="10px">No.</th>
						<th>Kategori</th>
						<th>Keterangan</th>
						<th>Urutan</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
    function checkDelete(){
        return confirm('Yakin Untuk Menghapus?');
    }
function init_datatable()
{
	datatable = $('#datatable').DataTable ({
		'bInfo': true,
		'serverSide': true,
		'serverMethod': 'post',
		'ajax': '<?php echo site_url('/master/refpilihan/datatable'); ?>',
		'order': [[ 2, 'asc' ]],
		'fixedHeader': true,
		'columns': [
			{
				data: function (row, type, val, meta) {
                    return '' +
                        '<a class="btn btn-action btn-primary" href="refpilihan/ubah/'+row.plhn_id+'">'+
                            '<i class="ti ti-pencil-alt"></i>'+
                        '</a>&nbsp;'+
						'<a class="btn btn-action btn-danger btn-delete" href="refpilihan/delete/'+row.plhn_id+'">'+
                            '<i class="ti ti-trash"></i>'+
                        '</a>';
                },
				orderable: false,
				className: 'dt-body-center'
			},
            { data: 'no', orderable: false },
			{ data: 'plhn_kategori' },
			{ data: 'plhn_nama' },
			{ data: 'plhn_urutan'},
			
		]
	});
}

$().ready(function() {
	
	init_datatable();
	
});
</script>