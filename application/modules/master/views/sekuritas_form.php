<div class="col-md-6 offset-md-3 p-0 mb-4">
	<div class="card">
		<div class="card-header">
			Form Sekuritas
			<a href="<?php echo site_url('/master/sekuritas'); ?>" class="btn btn-outline-primary btn-sm btn-header">
				<i class="ti ti-back-left"></i> Kembali
			</a>
		</div>
        <?php
            // $a='0.34';
            // echo str_replace(',', '.', $a);
        ?>
		<div class="card-body">
			<?php if ($this->session->flashdata('status_simpan') == 'ok'): ?>
			<div class="alert alert-success">Data berhasil disimpan.</div>
			<?php endif; ?>
			
			<?php if ($this->session->flashdata('status_simpan') == 'tidak_lengkap'): ?>
			<div class="alert alert-danger"><?php echo $this->session->flashdata('validation_errors'); ?></div>
			<?php endif; ?>
			<!-- <?=var_dump($url_aksi)?> -->
			<form method="post" action="<?php echo $url_aksi; ?>">
				<input type="hidden" name="seku_id" value="<?php if ($data != null) echo $data->seku_id; ?>">
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Nama Sekuritas
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="seku_nama" value="<?php if ($data != null) echo $data->seku_nama; ?>">
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger"></span> Nomor Telp
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="seku_no_telp" value="<?php if ($data != null) echo $data->seku_no_telp; ?>">
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger"></span> Nomor Fax
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="seku_fax" value="<?php if ($data != null) echo $data->seku_fax; ?>">
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger"></span> Email
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="seku_email" value="<?php if ($data != null) echo $data->seku_email; ?>">
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger"></span> Alamat
					</label>
					<div class="col-sm-6 pr-sm-0">
						<!-- <input type="text" class="form-control" name="bank_nama_pemilik" value="<?php if ($data != null) echo $data->bank_nama_pemilik; ?>"> -->
                        <textarea name="seku_alamat" id="" cols="30" rows="10"class="form-control"><?php if ($data != null) echo $data->seku_alamat; ?></textarea>
                    </div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Fee Pembelian
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="seku_broker_fee_pembelian" value="<?php if ($data != null) {echo $data->seku_broker_fee_pembelian;}else{echo "0";} ?>">
						<span class="text-danger"><small> Gunakan . (titik) sebagai penanda koma </small></span>
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Fee Penjualan
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="seku_broker_fee_penjualan" value="<?php if ($data != null) {echo $data->seku_broker_fee_penjualan;}else{echo "0";} ?>">
						<span class="text-danger"><small> Gunakan . (titik) sebagai penanda koma </small></span>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">&nbsp;</label>
					<div class="col-sm-6 pr-sm-0">
						<button type="submit" class="btn btn-primary">Simpan Data</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>