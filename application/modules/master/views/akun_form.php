<div class="col-md-6 offset-md-3 p-0 mb-4">
	<div class="card">
		<div class="card-header">
			Form Akun
			<a href="<?php echo site_url('/master/akun'); ?>" class="btn btn-outline-primary btn-sm btn-header">
				<i class="ti ti-back-left"></i> Kembali
			</a>
		</div>
		<div class="card-body">
			<?php if ($this->session->flashdata('status_simpan') == 'ok'): ?>
			<div class="alert alert-success">Data berhasil disimpan.</div>
			<?php endif; ?>
			
			<?php if ($this->session->flashdata('status_simpan') == 'tidak_lengkap'): ?>
			<div class="alert alert-danger"><?php echo $this->session->flashdata('validation_errors'); ?></div>
			<?php endif; ?>
			
			<form method="post" action="<?php echo $url_aksi; ?>">
				<input type="hidden" name="akun_id" value="<?php if ($data != null) echo $data->akun_id; ?>">
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Sekuritas
					</label>
					<div class="col-sm-6 pr-sm-0">
						<select name="akun_seku_id" id="" class="form-control select2">
                            <option value="">--Pilih Sekuritas--</option>
                            
                            <?=options_sekuritas($data->akun_seku_id)?>
                        </select>
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Kode Nasabah
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="akun_kode_nasabah" value="<?php if ($data != null) echo $data->akun_kode_nasabah; ?>">
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> No SID
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="akun_no_sid" value="<?php if ($data != null) echo $data->akun_no_sid; ?>">
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> No KSEI/KPEI
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="akun_no_ksei_kpei" value="<?php if ($data != null) echo $data->akun_no_ksei_kpei; ?>">
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Nama Bank RDN
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="akun_bank_rdn" value="<?php if ($data != null) echo $data->akun_bank_rdn; ?>">
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger"></span> Nama Cabang Bank RDN
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="akun_bank_cab_rdn" value="<?php if ($data != null) echo $data->akun_bank_cab_rdn; ?>">
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> No Rekening Bank RDN
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="akun_no_rekening_rdn" value="<?php if ($data != null) echo $data->akun_no_rekening_rdn; ?>">
					</div>
				</div>
				
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Tanggal Saldo Awal
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control tanggalan" name="swal_tgl_saldo_awal" id="swal_tgl_saldo_awal" value="<?php if ($data != null) {echo $data->swal_tgl_saldo_awal;}else{echo date('Y-m-d');} ?>" readonly>
					</div>
				</div>
			
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Saldo Awal
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control angka" name="swal_jumlah_saldo" value="<?php if ($data != null) {echo $data->swal_jumlah_saldo;}else{echo "0";} ?>">
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger"></span> Email
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="akun_email" value="<?php if ($data != null) echo $data->akun_email; ?>">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">&nbsp;</label>
					<div class="col-sm-6 pr-sm-0">
						<button type="submit" class="btn btn-primary">Simpan Data</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>