<div class="col-md-6 offset-md-3 p-0 mb-4">
	<div class="card">
		<div class="card-header">
			Form Ref-Pilihan
			<a href="<?php echo site_url('/master/refpilihan'); ?>" class="btn btn-outline-primary btn-sm btn-header">
				<i class="ti ti-back-left"></i> Kembali
			</a>
		</div>
        <?php
            // $a='0.34';
            // echo str_replace(',', '.', $a);
        ?>
		<div class="card-body">
			<?php if ($this->session->flashdata('status_simpan') == 'ok'): ?>
			<div class="alert alert-success">Data berhasil disimpan.</div>
			<?php endif; ?>
			
			<?php if ($this->session->flashdata('status_simpan') == 'tidak_lengkap'): ?>
			<div class="alert alert-danger"><?php echo $this->session->flashdata('validation_errors'); ?></div>
			<?php endif; ?>
			
			<form method="post" action="<?php echo $url_aksi; ?>">
				<input type="hidden" name="plhn_id" value="<?php if ($data != null) echo $data->plhn_id; ?>">
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Kategori
					</label>
					<div class="col-sm-6 pr-sm-0">
						<select name="plhn_kategori" id="" class="form-control">
                            <option value="">--Pilih Kategori--</option>
                            <?=options_refpilihan(!empty($data)? $data->plhn_kategori : "")?>
                        </select>
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Keterangan
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="plhn_nama" value="<?php if ($data != null) echo $data->plhn_nama; ?>">
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Urutan
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="plhn_urutan" value="<?php if ($data != null) echo $data->plhn_urutan; ?>">
					</div>
				</div>
                
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">&nbsp;</label>
					<div class="col-sm-6 pr-sm-0">
						<button type="submit" class="btn btn-primary">Simpan Data</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>