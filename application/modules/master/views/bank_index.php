<div class="col-md-10 offset-md-1 p-0">
	<div class="card">
		<div class="card-header">
			Master Bank
			<a href="bank/tambah" class="btn btn-primary btn-sm btn-header">
				<i class="ti ti-write"></i> Tambah Data
			</a>
		</div>
		<div class="card-body">
			<table class="cell-border stripe order-column hover" id="datatable">
				<thead>	
					<tr>
						<th width="50px">Aksi</th>
						<th width="10px">No.</th>
						<th>Judul</th>
						<th>Nama Bank</th>
						<th>Cabang</th>
                        <th>No Rekening</th>
                        <th>Nama Pemilik</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
    function checkDelete(){
        return confirm('Yakin Untuk Menghapus?');
    }
function init_datatable()
{
	datatable = $('#datatable').DataTable ({
		'bInfo': true,
		'serverSide': true,
		'serverMethod': 'post',
		'ajax': '<?php echo site_url('/master/bank/datatable'); ?>',
		'order': [[ 2, 'asc' ]],
		'fixedHeader': true,
		'columns': [
			{
				data: function (row, type, val, meta) {
                    return '' +
                        '<a class="btn btn-action btn-primary" href="bank/ubah/'+row.bank_id+'">'+
                            '<i class="ti ti-pencil-alt"></i>'+
                        '</a>&nbsp;'+
						'<a class="btn btn-action btn-danger btn-delete" href="bank/delete/'+row.bank_id+'">'+
                            '<i class="ti ti-trash"></i>'+
                        '</a>';
                },
				orderable: false,
				className: 'dt-body-center'
			},
            { data: 'no', orderable: false },
			{ data: 'bank_judul' },
			{ data: 'bank_nama' },
			{ data: 'bank_cabang' },
			{ data: 'bank_no_rekening'},
            { data: 'bank_nama_pemilik'}
		]
	});
}

$().ready(function() {
	
	init_datatable();
	
});
</script>