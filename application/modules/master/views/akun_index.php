<div class="col-md-12 offset-md-0 p-0">
	<div class="card">
		<div class="card-header">
			Master Akun
			<a href="akun/tambah" class="btn btn-primary btn-sm btn-header">
				<i class="ti ti-write"></i> Tambah Data
			</a>
		</div>
		<div class="card-body">
			<table class="cell-border stripe order-column hover" id="datatable">
				<thead>	
					<tr>
						<th width="50px">Aksi</th>
						<th width="10px">No.</th>
						<th>Kode Akun</th>
						<th>Nama Sekuritas</th>
						<th>Kode Nasabah</th>
						<th>No SID</th>
                        <th>No KSEI/KPEI</th>
                        <th>Bank RDN</th>
                        <th>Cabang Bank RDN</th>
                        <th>No Rekening RDN</th>
                        <th>Saldo Awal</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
    function checkDelete(){
        return confirm('Yakin Untuk Menghapus?');
    }
function init_datatable()
{
	datatable = $('#datatable').DataTable ({
		'bInfo': true,
		'serverSide': true,
		'serverMethod': 'post',
		'ajax': '<?php echo site_url('/master/akun/datatable'); ?>',
		'order': [[ 2, 'asc' ]],
		'fixedHeader': true,
		'columns': [
			{
				data: function (row, type, val, meta) {
                    return '' +
                        '<a class="btn btn-action btn-primary" href="akun/ubah/'+row.akun_id+'">'+
                            '<i class="ti ti-pencil-alt"></i>'+
                        '</a>&nbsp;'+
						'<a class="btn btn-action btn-danger btn-delete" href="akun/delete/'+row.akun_id+'">'+
                            '<i class="ti ti-trash"></i>'+
                        '</a>';
                },
				orderable: false,
				className: 'dt-body-center'
			},
            { data: 'no', orderable: false },
			{ data: 'akun_kode'},
			{ data: 'seku_nama'},
			{ data: 'akun_kode_nasabah' },
			{ data: 'akun_no_sid' },
			{ data: 'akun_no_ksei_kpei'},
            { data: 'akun_bank_rdn'},
            { data: 'akun_bank_cab_rdn'},
            { data: 'akun_no_rekening_rdn'},
            { data: 'swal_jumlah_saldo',className: 'dt-body-right'}
		]
	});
}

$().ready(function() {
	
	init_datatable();
	
});
</script>