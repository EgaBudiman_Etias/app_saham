<div class="col-md-6 offset-md-3 p-0 mb-4">
	<div class="card">
		<div class="card-header">
			Form Bank
			<a href="<?php echo site_url('/master/bank'); ?>" class="btn btn-outline-primary btn-sm btn-header">
				<i class="ti ti-back-left"></i> Kembali
			</a>
		</div>
		<div class="card-body">
			<?php if ($this->session->flashdata('status_simpan') == 'ok'): ?>
			<div class="alert alert-success">Data berhasil disimpan.</div>
			<?php endif; ?>
			
			<?php if ($this->session->flashdata('status_simpan') == 'tidak_lengkap'): ?>
			<div class="alert alert-danger"><?php echo $this->session->flashdata('validation_errors'); ?></div>
			<?php endif; ?>
			
			<form method="post" action="<?php echo $url_aksi; ?>">
				<input type="hidden" name="bank_id" value="<?php if ($data != null) echo $data->bank_id; ?>">
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Judul
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="bank_judul" value="<?php if ($data != null) echo $data->bank_judul; ?>">
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Nama Bank
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="bank_nama" value="<?php if ($data != null) echo $data->bank_nama; ?>">
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger"></span> Cabang
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="bank_cabang" value="<?php if ($data != null) echo $data->bank_cabang; ?>">
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> No Rekening
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="bank_no_rekening" value="<?php if ($data != null) echo $data->bank_no_rekening; ?>">
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Nama Pemilik
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="bank_nama_pemilik" value="<?php if ($data != null) echo $data->bank_nama_pemilik; ?>">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">&nbsp;</label>
					<div class="col-sm-6 pr-sm-0">
						<button type="submit" class="btn btn-primary">Simpan Data</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>