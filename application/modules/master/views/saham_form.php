<div class="col-md-6 offset-md-3 p-0 mb-4">
	<div class="card">
		<div class="card-header">
			Form Saham
			<a href="<?php echo site_url('/master/saham'); ?>" class="btn btn-outline-primary btn-sm btn-header">
				<i class="ti ti-back-left"></i> Kembali
			</a>
		</div>
		<div class="card-body">
			<?php if ($this->session->flashdata('status_simpan') == 'ok'): ?>
			<div class="alert alert-success">Data berhasil disimpan.</div>
			<?php endif; ?>
			
			<?php if ($this->session->flashdata('status_simpan') == 'tidak_lengkap'): ?>
			<div class="alert alert-danger"><?php echo $this->session->flashdata('validation_errors'); ?></div>
			<?php endif; ?>
			
			<form method="post" action="<?php echo $url_aksi; ?>">
				<input type="hidden" name="sham_id" value="<?php if ($data != null) echo $data->sham_id; ?>">
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Nama Saham
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="sham_nama" value="<?php if ($data != null) echo $data->sham_nama; ?>">
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Kode Saham
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="sham_kode" value="<?php if ($data != null) echo $data->sham_kode; ?>">
					</div>
				</div>
                
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Sektor
					</label>
					<div class="col-sm-6 pr-sm-0">
						<select name="sham_sektor" id="" class="form-control select2">
                            <option value="">--Pilih Sektor--</option>
                            
                            <?=options_pilihan(!empty($data->sham_sektor)?$data->sham_sektor:"",'sektor-saham')?>
                        </select>
					</div>
				</div>
               
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Kategori Kinerja
					</label>
					<div class="col-sm-6 pr-sm-0">
						<select name="sham_kinerja_kategori" id="" class="form-control select2">
                            <option value="">--Pilih Kategori--</option>
                            
                            <?=options_pilihan(!empty($data->sham_kinerja_kategori)? $data->sham_kinerja_kategori:"",'kategori-saham')?>
                        </select>
					</div>
				</div>
				<!-- <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Sekuritas
					</label>
					<div class="col-sm-6 pr-sm-0">
						<select name="sham_seku_id" id="" class="form-control">
                            <option value="">--Pilih sekuritas--</option>
                            
                            <?=options_sekuritas(!empty($data->sham_seku_id)?$data->sham_seku_id:"")?>
                        </select>
					</div>
				</div> -->
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">&nbsp;</label>
					<div class="col-sm-6 pr-sm-0">
						<button type="submit" class="btn btn-primary">Simpan Data</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>