<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Refpilihan extends MX_Controller {

	public function index()
	{
		$this->load->view('templates/site_tpl', array (
			'content' => 'refpilihan_index',
		));
	}
	
	public function datatable()
	{
       
		$draw = $this->input->post('draw');
		$offset = $this->input->post('start');
		$num_rows = $this->input->post('length');
		$order_index = $_POST['order'][0]['column'];
		$order_by = $_POST['columns'][$order_index]['data'];
		$order_direction = $_POST['order'][0]['dir'];
		$keyword = $_POST['search']['value'];
		
		$bindings = array("%{$keyword}%","%{$keyword}%");
		
		$base_sql = "
			from ref_pilihan
			where
				plhn_is_deleted = '1'
				and (
					plhn_kategori like ?
                    or plhn_nama like ?
				)
		";
		
		$data_sql = "
			select
				ref_pilihan.*
				, row_number() over (
					order by
						{$order_by} {$order_direction}
						, plhn_urutan {$order_direction}
				  ) as nomor
			{$base_sql}
			order by
				{$order_by} {$order_direction}
				, plhn_urutan {$order_direction}
                
			limit {$offset}, {$num_rows}
		";
                    
		$src = $this->db->query($data_sql, $bindings);
		// echo $this->db->last_query();
		// die();
		$count_sql = "
			select count(*) AS total
			{$base_sql}
		";
		$total_records = $this->db->query($count_sql, $bindings)->row('total');
		
		$data = array();
		
		foreach ($src->result() as $row) {
			$data[] = array (
				'plhn_id' => $row->plhn_id,
				'plhn_kategori' => $row->plhn_kategori,
				'plhn_nama' => $row->plhn_nama,
				'plhn_urutan' => $row->plhn_urutan,
                'no'=>$row->nomor,
			);
		}
		
		$response = array (
			'draw' => intval($draw),
			'iTotalRecords' => $src->num_rows(),
			'iTotalDisplayRecords' => $total_records,
			'aaData' => $data,
		);
		
		echo json_encode($response);
	}
	
	private function _form($aksi = 'tambah', $data = null)
	{
		if ($this->session->flashdata('data_form')) {
			$data = $this->session->flashdata('data_form');
		}
		
		$this->load->view('templates/site_tpl', array (
			'content' => 'refpilihan_form',
			'url_aksi' => site_url("/master/refpilihan/{$aksi}-data"),
			'data' => $data,
		));
	}
	
	public function tambah()
	{
		$this->_form();
	}
	
	public function ubah($id = '')
	{
		if ( ! $this->agent->referrer()) {
			show_404();
		}
		
		$src = $this->db
			->from('ref_pilihan')
			->where('plhn_is_deleted', '1')
			->where('plhn_id', $id)
			->get();
		
		if ($src->num_rows() == 0) {
			show_404();
		}
		
		$this->_form('ubah', $src->row());
	}
	
	private function _data_form()
	{
		$validasi = array (
			array (
				'field' => 'plhn_kategori',
				'label' => 'Nama Kategori',
				'rules' => 'required',
			),
			array (
				'field' => 'plhn_nama',
				'label' => 'Keterangan',
				'rules' => 'required',
			),
			array (
				'field' => 'plhn_urutan',
				'label' => 'Urutan',
				'rules' => 'required',
			),
		);
		
		$this->form_validation->set_rules($validasi);
		// var_dump($this->input->post());
		if ($this->form_validation->run()) {
			
			$kunci_data = array (
				'plhn_nama',
				'plhn_kategori',
				'plhn_urutan',
                
			);
			
			return data_post($kunci_data);
		}
		else {
			$this->session->set_flashdata('status_simpan', 'tidak_lengkap');
			$this->session->set_flashdata('validation_errors', validation_errors());
			$this->session->set_flashdata('data_form', (object) $this->input->post());
			return null;
		}
	}
	
	public function tambah_data()
	{
        // var_dump($this->input->post('plhn_kategori'));
        // die();
		$data = $this->_data_form();
		
		if ($data != null) {
			$data['plhn_created_by'] = $data['plhn_updated_by'] = session_pengguna('peng_id');
            $data['plhn_created_time']=$data["plhn_updated_time"] =date('Y-m-d H:i:s');
			$this->db->insert('ref_pilihan', $data);
			$this->session->set_flashdata('status_simpan', 'ok');
		}
		
		redirect(site_url('/master/refpilihan/tambah'));
	}
	
	public function ubah_data()
	{
		$data = $this->_data_form();
		// var_dump($data);
		// die();
		if ($data != null) {
			$data['plhn_updated_time'] = date('Y-m-d H:i:s');
			$data['plhn_updated_by'] = session_pengguna('peng_id');
			
			$where = array('plhn_id' => $this->input->post('plhn_id'));
			
			$this->db->update('ref_pilihan', $data, $where);
			$this->session->set_flashdata('status_simpan', 'ok');
		}
		
		redirect(site_url('/master/refpilihan'));
	}
	
    public function delete($plhn_id){
        $data['plhn_updated_time'] = date('Y-m-d H:i:s');
        $data['plhn_updated_by'] = session_pengguna('peng_id');
        $data['plhn_is_deleted'] = "0";
        $where = array('plhn_id' => $plhn_id);
        
        $this->db->update('ref_pilihan', $data, $where);
        redirect(site_url('/master/refpilihan'));
    }
}
