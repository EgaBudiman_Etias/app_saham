<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bank extends MX_Controller {

	public function index()
	{
		$this->load->view('templates/site_tpl', array (
			'content' => 'bank_index',
		));
	}
	
	public function datatable()
	{
       
		$draw = $this->input->post('draw');
		$offset = $this->input->post('start');
		$num_rows = $this->input->post('length');
		$order_index = $_POST['order'][0]['column'];
		$order_by = $_POST['columns'][$order_index]['data'];
		$order_direction = $_POST['order'][0]['dir'];
		$keyword = $_POST['search']['value'];
		
		$bindings = array("%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%");
		
		$base_sql = "
			from bank as a
			where
				a.bank_is_deleted = '1'
				and (
					a.bank_nama like ?
					or a.bank_judul like ?
					or a.bank_cabang like ?
                    or a.bank_no_rekening like ?
					or a.bank_nama_pemilik like ?
				)
		";
		
		$data_sql = "
			select
				a.*
				, row_number() over (
					order by
						{$order_by} {$order_direction}
						, a.bank_nama {$order_direction}
				  ) as nomor
			{$base_sql}
			order by
				{$order_by} {$order_direction}
				, a.bank_nama {$order_direction}
			limit {$offset}, {$num_rows}
		";
                    
		$src = $this->db->query($data_sql, $bindings);
		// echo $this->db->last_query();
		// die();
		$count_sql = "
			select count(*) AS total
			{$base_sql}
		";
		$total_records = $this->db->query($count_sql, $bindings)->row('total');
		
		$data = array();
		
		foreach ($src->result() as $row) {
			$data[] = array (
				'bank_id' => $row->bank_id,
				'bank_judul' => $row->bank_judul,
				'bank_nama' => $row->bank_nama,
				'bank_cabang' => $row->bank_cabang,
				'bank_no_rekening' => $row->bank_no_rekening,
				'bank_nama_pemilik' => $row->bank_nama_pemilik,
                'no'=>$row->nomor,
			);
		}
		
		$response = array (
			'draw' => intval($draw),
			'iTotalRecords' => $src->num_rows(),
			'iTotalDisplayRecords' => $total_records,
			'aaData' => $data,
		);
		
		echo json_encode($response);
	}
	
	private function _form($aksi = 'tambah', $data = null)
	{
		if ($this->session->flashdata('data_form')) {
			$data = $this->session->flashdata('data_form');
		}
		
		$this->load->view('templates/site_tpl', array (
			'content' => 'bank_form',
			'url_aksi' => site_url("/master/bank/{$aksi}-data"),
			'data' => $data,
		));
	}
	
	public function tambah()
	{
		$this->_form();
	}
	
	public function ubah($id = '')
	{
		if ( ! $this->agent->referrer()) {
			show_404();
		}
		
		$src = $this->db
			->from('bank')
			->where('bank_is_deleted', '1')
			->where('bank_id', $id)
			->get();
		
		if ($src->num_rows() == 0) {
			show_404();
		}
		
		$this->_form('ubah', $src->row());
	}
	
	private function _data_form()
	{
		$validasi = array (
			array (
				'field' => 'bank_judul',
				'label' => 'Judul Bank',
				'rules' => 'required',
			),
			array (
				'field' => 'bank_nama',
				'label' => 'Nama Bank',
				'rules' => 'required',
			),
			array (
				'field' => 'bank_cabang',
				'label' => 'Cabang Bank',
				'rules' => '',
			),
            array (
				'field' => 'bank_no_rekening',
				'label' => 'Nomor Rekening',
				'rules' => 'required',
			),
            array (
				'field' => 'bank_nama_pemilik',
				'label' => 'Nama Pemilik',
				'rules' => 'required',
			),
		);
		
		$this->form_validation->set_rules($validasi);
		
		if ($this->form_validation->run()) {
			
			$kunci_data = array (
				'bank_judul',
				'bank_nama',
				'bank_cabang',
                'bank_no_rekening',
                'bank_nama_pemilik'
			);
			
			return data_post($kunci_data);
		}
		else {
			$this->session->set_flashdata('status_simpan', 'tidak_lengkap');
			$this->session->set_flashdata('validation_errors', validation_errors());
			$this->session->set_flashdata('data_form', (object) $this->input->post());
			return null;
		}
	}
	
	public function tambah_data()
	{
		$data = $this->_data_form();
		
		if ($data != null) {
			$data['bank_created_by'] = $data['bank_updated_by'] = session_pengguna('peng_id');
            $data['bank_created_time']=$data["bank_updated_time"] =date('Y-m-d H:i:s');
			$this->db->insert('bank', $data);
			$this->session->set_flashdata('status_simpan', 'ok');
		}
		
		redirect(site_url('/master/bank/tambah'));
	}
	
	public function ubah_data()
	{
		$data = $this->_data_form();
		$bank_id=$this->input->post('bank_id');
		if ($data != null) {
			$data['bank_updated_time'] = date('Y-m-d H:i:s');
			$data['bank_updated_by'] = session_pengguna('peng_id');
			
			$where = array('bank_id' => $bank_id);
			
			$this->db->update('bank', $data, $where);
			$this->session->set_flashdata('status_simpan', 'ok');
		}
		
		redirect(site_url('/master/bank/ubah/'.$bank_id));
	}
	
    public function delete($bank_id){
        $penanda=cekTransaksiBeforeDelete("Bank",$bank_id);
		if($penanda==0){
			$data['bank_updated_time'] = date('Y-m-d H:i:s');
			$data['bank_updated_by'] = session_pengguna('peng_id');
			$data['bank_is_deleted'] = "0";
			$where = array('bank_id' => $bank_id);
			
			$this->db->update('bank', $data, $where);
			redirect(site_url('/master/bank'));
		}
		else{
			echo "<script>
					alert('Bank ini Digunakan Dalam Transaksi');
					window.location.href='".base_url()."master/bank';
					</script>";
		}
    }
}
