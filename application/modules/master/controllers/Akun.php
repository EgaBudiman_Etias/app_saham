<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Akun extends MX_Controller {

	public function index()
	{
		$this->load->view('templates/site_tpl', array (
			'content' => 'akun_index',
		));
	}
	
	public function datatable()
	{
		$draw = $this->input->post('draw');
		$offset = $this->input->post('start');
		$num_rows = $this->input->post('length');
		$order_index = $_POST['order'][0]['column'];
		$order_by = $_POST['columns'][$order_index]['data'];
		$order_direction = $_POST['order'][0]['dir'];
		$keyword = $_POST['search']['value'];
		
		$bindings = array("%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%");
		
		$base_sql = "
			from akun
            JOIN sekuritas on(akun_seku_id=seku_id)
			LEFT JOIN saldo_awal on(akun_id=swal_akun_id)
            where
				akun_is_deleted = '1'
				and (
					seku_nama like ?
                    or akun_kode_nasabah like ?
                    or akun_no_sid like ?
					or akun_kode like ?
					or akun_no_ksei_kpei like ?
					or akun_bank_rdn like ?	
					or akun_bank_cab_rdn like ?	
					or akun_no_rekening_rdn like ?	
					or swal_jumlah_saldo like ?	
				)
		";
		
		$data_sql = "
			select
				akun.*,seku_nama,coalesce(swal_jumlah_saldo,0) as swal_jumlah_saldo
				, row_number() over (
					order by
						{$order_by} {$order_direction}
						, akun_kode {$order_direction}
				  ) as nomor
			{$base_sql}
			order by
				{$order_by} {$order_direction}
				, akun_id {$order_direction}
			limit {$offset}, {$num_rows}
		";
                    
		$src = $this->db->query($data_sql, $bindings);
		// echo $this->db->last_query();
        // die();
		$count_sql = "
			select count(*) AS total
			{$base_sql}
		";
		$total_records = $this->db->query($count_sql, $bindings)->row('total');
		
		$data = array();
		
		foreach ($src->result() as $row) {
			$data[] = array (
				'seku_nama' => $row->seku_nama,
				'akun_kode_nasabah' => $row->akun_kode_nasabah,
				'akun_no_sid' => $row->akun_no_sid,
				'akun_no_ksei_kpei' => $row->akun_no_ksei_kpei,
				'akun_bank_rdn' => $row->akun_bank_rdn,
				'akun_bank_cab_rdn' => $row->akun_bank_cab_rdn,
                'akun_no_rekening_rdn' => $row->akun_no_rekening_rdn,
                'akun_email' => $row->akun_email,
                'akun_id'=>$row->akun_id,
				'akun_kode'=>$row->akun_kode,
				'swal_jumlah_saldo'=>rupiah2($row->swal_jumlah_saldo),
                'no'=>$row->nomor,
			);
		}
		
		$response = array (
			'draw' => intval($draw),
			'iTotalRecords' => $src->num_rows(),
			'iTotalDisplayRecords' => $total_records,
			'aaData' => $data,
		);
		
		echo json_encode($response);
	}
	
	private function _form($aksi = 'tambah', $data = null)
	{
		if ($this->session->flashdata('data_form')) {
			$data = $this->session->flashdata('data_form');
		}
		
		$this->load->view('templates/site_tpl', array (
			'content' => 'akun_form',
			'url_aksi' => site_url("/master/akun/{$aksi}-data"),
			'data' => $data,
		));
	}
	
	public function tambah()
	{
		$this->_form();
	}
	
	public function ubah($id = '')
	{
		if ( ! $this->agent->referrer()) {
			show_404();
		}
		
		$src = $this->db
			->select('akun.*,coalesce(swal_jumlah_saldo,0) as swal_jumlah_saldo,swal_tgl_saldo_awal')
			->from('akun')
			->join('saldo_awal','swal_akun_id=akun_id','LEFT')
			->where('akun_is_deleted', '1')
			->where('akun_id', $id)
			->get();
		
		if ($src->num_rows() == 0) {
			show_404();
		}
		
		$this->_form('ubah', $src->row());
	}
	
	private function _data_form()
	{
		$validasi = array (
			array (
				'field' => 'akun_seku_id',
				'label' => 'Sekuritas Akun',
				'rules' => 'required',
			),
			array (
				'field' => 'akun_kode_nasabah',
				'label' => 'Kode Nasabah',
				'rules' => 'required',
			),
			array (
				'field' => 'akun_no_sid',
				'label' => 'Nomor SID',
				'rules' => 'required',
			),
            array (
				'field' => 'akun_no_ksei_kpei',
				'label' => 'Nomor KSEI/KPEI',
				'rules' => 'required',
			),
            array (
				'field' => 'akun_bank_rdn',
				'label' => 'Nama Bank RDN',
				'rules' => 'required',
			),
            array (
				'field' => 'akun_bank_cab_rdn',
				'label' => 'Nama Cabang Bank RDN',
				'rules' => '',
			),
            array (
				'field' => 'akun_no_rekening_rdn',
				'label' => 'Nomor Rekening Bank RDN',
				'rules' => 'required',
			),
            array (
				'field' => 'akun_email',
				'label' => 'Email',
				'rules' => '',
			),

			array (
				'field' => 'swal_tgl_saldo_awal',
				'label' => 'Tanggal Saldo Awal',
				'rules' => 'required',
			),
			array (
				'field' => 'swal_jumlah_saldo',
				'label' => 'Jumlah Saldo Awal',
				'rules' => 'required',
			),
		);
		
		$this->form_validation->set_rules($validasi);
		
		if ($this->form_validation->run()) {
			
			$kunci_data = array (
				'akun_seku_id',
				'akun_kode_nasabah',
				'akun_no_sid',
                'akun_no_ksei_kpei',
                'akun_bank_rdn',
                'akun_bank_cab_rdn',
                'akun_no_rekening_rdn',
                'akun_email',
			);
			
			return data_post($kunci_data);
		}
		else {
			$this->session->set_flashdata('status_simpan', 'tidak_lengkap');
			$this->session->set_flashdata('validation_errors', validation_errors());
			$this->session->set_flashdata('data_form', (object) $this->input->post());
			return null;
		}
	}
	
	public function tambah_data()
	{
		$data = $this->_data_form();
		
		if ($data != null) {
			$data['akun_created_by'] = $data['akun_updated_by'] = session_pengguna('peng_id');
            $data['akun_created_time']=$data["akun_updated_time"] =date('Y-m-d H:i:s');
			$data['akun_kode']=kode_akun();
			$this->db->insert('akun', $data);
			$id_akun=$this->db->insert_id();

			$data_awal['swal_created_by'] = $data_awal['swal_updated_by'] = session_pengguna('peng_id');
			$data_awal['swal_created_time']=$data_awal["swal_updated_time"] =date('Y-m-d H:i:s');
			$data_awal['swal_akun_id']=$id_akun;
			$data_awal['swal_tgl_saldo_awal']=$this->input->post('swal_tgl_saldo_awal',true);
			$data_awal['swal_jumlah_saldo']=str_replace(',', '', $this->input->post('swal_jumlah_saldo',true));
			$this->db->insert('saldo_awal', $data_awal);

			$data_keuangan=array(
				'uang_akun_id'=>$id_akun,
				'uang_tgl'=>$this->input->post('swal_tgl_saldo_awal',true),
				'uang_jns_transaksi'=>'saldoawal',
				'uang_transaksi_id'=>0,
				'uang_nominal'=>str_replace(',', '', $this->input->post('swal_jumlah_saldo',true)),
				'uang_created_by'=>session_pengguna('peng_id'),
				'uang_created_time'=>date('Y-m-d H:i:s'),
			);
			$this->db->insert('keuangan', $data_keuangan);
			$this->session->set_flashdata('status_simpan', 'ok');
		}
		
		redirect(site_url('/master/akun/tambah'));
	}
	
	public function ubah_data()
	{
		$data = $this->_data_form();
		$id_akun=$this->input->post('akun_id');
		if ($data != null) {
			$data['akun_updated_time'] = date('Y-m-d H:i:s');
			$data['akun_updated_by'] = session_pengguna('peng_id');
			
			$where = array('akun_id' => $this->input->post('akun_id'));
			
			$this->db->update('akun', $data, $where);
			$ceksaldoawal=$this->db->from('saldo_awal')->where('swal_akun_id',$this->input->post('akun_id'))->get()->row();
			if(empty($ceksaldoawal)){
				$data_awal['swal_created_by'] = $data_awal['swal_updated_by'] = session_pengguna('peng_id');
				$data_awal['swal_created_time']=$data_awal["swal_updated_time"] =date('Y-m-d H:i:s');
				$data_awal['swal_akun_id']=$id_akun;
				$data_awal['swal_tgl_saldo_awal']=$this->input->post('swal_tgl_saldo_awal',true);
				$data_awal['swal_jumlah_saldo']=str_replace(',', '', $this->input->post('swal_jumlah_saldo',true));
				$this->db->insert('saldo_awal', $data_awal);
			}else{
				$data_awal['swal_jumlah_saldo']=str_replace(',', '', $this->input->post('swal_jumlah_saldo',true));
				$data_awal['swal_tgl_saldo_awal']=$this->input->post('swal_tgl_saldo_awal',true);
				$where = array('swal_akun_id' => $this->input->post('akun_id'));
				$this->db->update('saldo_awal', $data_awal, $where);
			}
			$this->db->delete('keuangan', array('uang_transaksi_id' => 0,'uang_jns_transaksi'=>'saldoawal'));
			$data_keuangan=array(
				'uang_akun_id'=>$id_akun,
				'uang_tgl'=>$this->input->post('swal_tgl_saldo_awal',true),
				'uang_jns_transaksi'=>'saldoawal',
				'uang_transaksi_id'=>0,
				'uang_nominal'=>str_replace(',', '', $this->input->post('swal_jumlah_saldo',true)),
				'uang_created_by'=>session_pengguna('peng_id'),
				'uang_created_time'=>date('Y-m-d H:i:s'),
			);
			$this->db->insert('keuangan', $data_keuangan);
			$this->session->set_flashdata('status_simpan', 'ok');
		}
		
		redirect(site_url('/master/akun/ubah/'.$id_akun));
	}
	
    public function delete($akun_id){
        $penanda=cekTransaksiBeforeDelete("Akun",$akun_id);
		if($penanda==0){
			$data['akun_updated_time'] = date('Y-m-d H:i:s');
			$data['akun_updated_by'] = session_pengguna('peng_id');
			$data['akun_is_deleted'] = "0";
			$where = array('akun_id' => $akun_id);
			
			$this->db->update('akun', $data, $where);
			redirect(site_url('/master/akun'));
		}
		else{
			echo "<script>
					alert('Bank ini Digunakan Dalam Transaksi');
					window.location.href='".base_url()."master/akun';
					</script>";
		}
		
    }

	public function ajax_akun(){
		$akun_id = $this->input->post('pmbl_akun_id');
		$sham_id=$this->input->post('sham_id');
		// var_dump($sham_id);
		$saldosahamawal=0;
		$saldopembeliansaham=0;
		$saldopenjualansaham=0;
		$src = $this->db
			->select('akun_id,akun_seku_id,seku_broker_fee_pembelian,seku_broker_fee_penjualan')
			->from('akun')
			->join('sekuritas','akun_seku_id=seku_id')
			->where('akun_id', $akun_id)
			->get()->row();
		$srcsahamawal=$this->db->select('shal_id,shal_akun_id,akun_seku_id,shal_sham_id,shal_tgl_saldo_awal,shal_jumlah_lot,shal_jumlah_shares,shal_rata_rata_harga,shal_total ,shal_is_deleted ')
							 ->from('saldo_awal_saham')
							 ->JOIN('akun','shal_akun_id=akun_id')
							 ->JOIN('sekuritas','seku_id=akun_seku_id')
							 ->where(array('shal_akun_id'=>$akun_id,'shal_sham_id'=>$sham_id))
							 ->get()->row();
		// var_dump($this->db->last_query());
		if(!empty($srcsahamawal)){
			$saldosahamawal=$srcsahamawal->shal_jumlah_lot;
		}	
		$srcpembelian=$this->db->select('coalesce(sum(kshm_jumlah),0) as jumlah_lot')
						   ->from('keuangan_saham')
						   ->JOIN ('pembelian','kshm_transaksi_id=pmbl_id')
						   ->JOIN('akun','pmbl_akun_id=akun_id')
						   ->JOIN('sekuritas','seku_id=akun_seku_id')
						   ->WHERE(array('kshm_jns_transaksi'=>"pembelian",'pmbl_akun_id'=>$akun_id,'kshm_sham_id'=>$sham_id,'pmbl_is_deleted'=>'1'))
						   ->get()->row();
						//    var_dump($this->db->last_query());
		if(!empty($srcpembelian)){
			$saldopembeliansaham=$srcpembelian->jumlah_lot;
		}

		$srcpenjualan=$this->db->select('coalesce(sum(kshm_jumlah),0) as jumlah_lot')
						   ->from('keuangan_saham')
						   ->JOIN ('penjualan','kshm_transaksi_id=pnjl_id')
						   ->JOIN('akun','pnjl_akun_id=akun_id')
						   ->JOIN('sekuritas','seku_id=akun_seku_id')
						   ->WHERE(array('kshm_jns_transaksi'=>"penjualan",'pnjl_akun_id'=>$akun_id,'kshm_sham_id'=>$sham_id,'pnjl_is_deleted'=>'1'))
						   ->get()->row();
						//    var_dump($this->db->last_query());
		if(!empty($srcpenjualan)){
			$saldopenjualansahamsaham=$srcpenjualan->jumlah_lot;
		}
		$hasil=array(
			'saldosaham'=>$saldosahamawal+$saldopembeliansaham+$saldopenjualansaham,
			'akun_id'=>$src->akun_id,
			'akun_seku_id'=>$src->akun_seku_id,
			'seku_broker_fee_pembelian'=>$src->seku_broker_fee_pembelian,
			'seku_broker_fee_penjualan'=>$src->seku_broker_fee_penjualan,
		);
		
		header('Content-Type: application/json');
		echo json_encode($hasil);
	}

	public function ajax_akun_nominal(){
		$akun_id = $this->input->post('akun_id');
		// $src = $this->db
		// 	->select('coalesce(sum(uang_nominal),0) as nominal,uang_akun_id')
		// 	->from('keuangan')
		// 	->where(array('uang_akun_id'=> $akun_id))
		// 	->get();

		$src=$this->db->query("SELECT COALESCE(SUM(nominal),0) AS nominal FROM(
			(SELECT COALESCE(SUM(swal_jumlah_saldo),0) as nominal,swal_akun_id,'saldo awal' as uang_jns_transaksi from saldo_awal
			JOIN akun on swal_akun_id=akun_id
			WHERE swal_akun_id='$akun_id' AND swal_is_deleted='1'
			)
			-- UNION
			-- (SELECT COALESCE(SUM(uang_nominal),0) AS nominal,uang_akun_id,uang_jns_transaksi FROM keuangan 
			-- JOIN akun ON uang_akun_id=akun_id
			-- WHERE uang_jns_transaksi='saldoawal' AND akun_is_deleted='1' AND uang_akun_id='$akun_id'  AND uang_transaksi_id='0')
			
			UNION
			
			(SELECT COALESCE(SUM(uang_nominal),0) AS nominal,uang_akun_id,uang_jns_transaksi FROM keuangan 
			JOIN setoran ON uang_transaksi_id=stor_id
			WHERE uang_jns_transaksi='setoran' AND  stor_is_deleted='1' AND uang_akun_id='$akun_id')
			
			UNION
			
			(SELECT COALESCE(SUM(uang_nominal),0) AS nominal,uang_akun_id,uang_jns_transaksi FROM keuangan 
			JOIN penarikan ON uang_transaksi_id=trik_id
			WHERE uang_jns_transaksi='penarikan' AND trik_is_deleted='1'AND uang_akun_id='$akun_id')
			
			UNION 
			
			(SELECT COALESCE(SUM(uang_nominal),0) AS nominal,uang_akun_id,uang_jns_transaksi FROM keuangan 
			JOIN pembelian ON uang_transaksi_id=pmbl_id
			WHERE uang_jns_transaksi='pembelian' AND pmbl_is_deleted='1'AND uang_akun_id='$akun_id')
			
			UNION
			
			(SELECT COALESCE(SUM(uang_nominal),0) AS nominal,uang_akun_id,uang_jns_transaksi FROM keuangan 
			JOIN penjualan ON uang_transaksi_id=pnjl_id
			WHERE uang_jns_transaksi='penjualan' AND pnjl_is_deleted='1'AND uang_akun_id='$akun_id')
			
			UNION 
			
			(SELECT COALESCE(SUM(uang_nominal),0) AS nominal,uang_akun_id,uang_jns_transaksi FROM keuangan 
			JOIN deviden ON uang_transaksi_id=devi_id
			WHERE uang_jns_transaksi='deviden' AND devi_is_deleted='1' AND uang_akun_id='$akun_id')) AS td ");
		
		header('Content-Type: application/json');
		echo json_encode($src->row());
	}

	public function ajax_akun_saham(){
		$akun_id = $this->input->post('akun_id');
		$src = $this->db
			// ->select('COALESCE(SUM(kshm_jumlah),0) AS jumlah,kshm_sham_id,sham_nama,sham_kode,seku_broker_fee_penjualan')
			// ->from('keuangan_saham')
			// ->JOIn('saham','sham_id=kshm_sham_id')
			// ->JOIn('akun','akun_id=kshm_akun_id')
			// ->JOIn('sekuritasa','akun_seku_id=seku_id')
			// ->where('kshm_akun_id', $akun_id)
			// ->GROUP_BY('kshm_akun_id,kshm_sham_id')
			// ->order_by('sham_kode','asc')
			// ->get();
			->query("
			SELECT COALESCE(SUM(jumlah),0) AS jumlah,kshm_sham_id,sham_nama,sham_kode,seku_broker_fee_penjualan FROM(
				(SELECT COALESCE(SUM(shal_jumlah_lot),0) AS jumlah,shal_sham_id as kshm_sham_id ,sham_nama,sham_kode,seku_broker_fee_penjualan
				FROM saldo_awal_saham
				JOIN saham ON sham_id=shal_sham_id 
				JOIN akun ON akun_id=shal_akun_id 
				JOIN sekuritas ON akun_seku_id=seku_id
				WHERE shal_akun_id = '$akun_id' GROUP BY shal_akun_id, shal_sham_id ORDER BY sham_kode ASC)
				
				UNION

				(SELECT COALESCE(SUM(kshm_jumlah), 0) AS jumlah, kshm_sham_id, sham_nama, sham_kode, seku_broker_fee_penjualan
				FROM keuangan_saham
				JOIN saham ON sham_id=kshm_sham_id 
				JOIN akun ON akun_id=kshm_akun_id 
				JOIN sekuritas ON akun_seku_id=seku_id
				JOIN pembelian on kshm_transaksi_id=pmbl_id
				WHERE kshm_akun_id = '$akun_id' and pmbl_is_deleted='1' and kshm_jns_transaksi='pembelian' GROUP BY kshm_akun_id, kshm_sham_id ORDER BY sham_kode ASC)
				
				UNION

				(SELECT COALESCE(SUM(kshm_jumlah), 0) AS jumlah, kshm_sham_id, sham_nama, sham_kode, seku_broker_fee_penjualan
				FROM keuangan_saham
				JOIN saham ON sham_id=kshm_sham_id 
				JOIN akun ON akun_id=kshm_akun_id 
				JOIN sekuritas ON akun_seku_id=seku_id
				JOIN penjualan on kshm_transaksi_id=pnjl_id
				WHERE kshm_akun_id = '$akun_id' and pnjl_is_deleted='1' and kshm_jns_transaksi='penjualan' GROUP BY kshm_akun_id, kshm_sham_id ORDER BY sham_kode ASC)
				) as dt
			GROUP BY  kshm_sham_id
			");
		
		header('Content-Type: application/json');
		echo json_encode($src->result());
	}

	public function ajax_akun_awal(){
		$akun_id = $this->input->post('akun_id');
		$src = $this->db
			->select('*')
			->from('saldo_awal_saham')
			->JOIn('saham','sham_id=shal_sham_id')
			->JOIn('akun','akun_id=shal_akun_id')
			->JOIn('sekuritas','akun_seku_id=seku_id')
			->where('shal_akun_id', $akun_id)
			->order_by('sham_kode','asc')
			->get();
		
		header('Content-Type: application/json');
		echo json_encode($src->result());
	}
}
