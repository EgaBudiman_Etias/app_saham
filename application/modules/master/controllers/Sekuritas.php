<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sekuritas extends MX_Controller {

	public function index()
	{
		$this->load->view('templates/site_tpl', array (
			'content' => 'sekuritas_index',
		));
	}
	
	public function datatable()
	{
       
		$draw = $this->input->post('draw');
		$offset = $this->input->post('start');
		$num_rows = $this->input->post('length');
		$order_index = $_POST['order'][0]['column'];
		$order_by = $_POST['columns'][$order_index]['data'];
		$order_direction = $_POST['order'][0]['dir'];
		$keyword = $_POST['search']['value'];
		
		$bindings = array("%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%");
		
		$base_sql = "
			from sekuritas
			where
				seku_is_deleted = '1'
				and (
					seku_nama like ?
					or seku_no_telp like ?
					or seku_fax like ?
					or seku_email like ?
					or seku_alamat like ?
					or seku_broker_fee_pembelian like ?
					or seku_broker_fee_penjualan like ?

				)
		";
		
		$data_sql = "
			select
				sekuritas.*
				, row_number() over (
					order by
						{$order_by} {$order_direction}
						, seku_nama {$order_direction}
				  ) as nomor
			{$base_sql}
			order by
				{$order_by} {$order_direction}
				, seku_nama {$order_direction}
			limit {$offset}, {$num_rows}
		";
                    
		$src = $this->db->query($data_sql, $bindings);
		// echo $this->db->last_query();
		// die();
		$count_sql = "
			select count(*) AS total
			{$base_sql}
		";
		$total_records = $this->db->query($count_sql, $bindings)->row('total');
		
		$data = array();
		
		foreach ($src->result() as $row) {
			$data[] = array (
				'seku_id' => $row->seku_id,
				'seku_nama' => $row->seku_nama,
				'seku_no_telp' => $row->seku_no_telp,
				'seku_fax' => $row->seku_fax,
				'seku_email' => $row->seku_email,
				'seku_alamat' => $row->seku_alamat,
                'seku_broker_fee_pembelian' => $row->seku_broker_fee_pembelian,
				'seku_broker_fee_penjualan' => $row->seku_broker_fee_penjualan,
                'no'=>$row->nomor,
			);
		}
		
		$response = array (
			'draw' => intval($draw),
			'iTotalRecords' => $src->num_rows(),
			'iTotalDisplayRecords' => $total_records,
			'aaData' => $data,
		);
		
		echo json_encode($response);
	}
	
	private function _form($aksi = 'tambah', $data = null)
	{
		if ($this->session->flashdata('data_form')) {
			$data = $this->session->flashdata('data_form');
		}
		
		$this->load->view('templates/site_tpl', array (
			'content' => 'sekuritas_form',
			'url_aksi' => site_url("/master/sekuritas/{$aksi}-data"),
			'data' => $data,
		));
	}
	
	public function tambah()
	{
		$this->_form();
	}
	
	public function ubah($id = '')
	{
		if ( ! $this->agent->referrer()) {
			show_404();
		}
		
		$src = $this->db
			->from('sekuritas')
			->where('seku_is_deleted', '1')
			->where('seku_id', $id)
			->get();
		
		if ($src->num_rows() == 0) {
			show_404();
		}
		
		$this->_form('ubah', $src->row());
	}
	
	
	
	private function _data_form()
	{
		
		$validasi = array (
			array (
				'field' => 'seku_nama',
				'label' => 'Nama Sekuritas',
				'rules' => 'required',
			),
			array (
				'field' => 'seku_no_telp',
				'label' => 'Nomor Telp',
				'rules' => '',
			),
			array (
				'field' => 'seku_fax',
				'label' => 'Nomor FAX',
				'rules' => '',
			),
            array (
				'field' => 'seku_email',
				'label' => 'Email',
				'rules' => '',
			),
            array (
				'field' => 'seku_alamat',
				'label' => 'Alamat',
				'rules' => '',
			),
            array (
				'field' => 'seku_broker_fee_pembelian',
				'label' => 'Fee Pembelian',
				'rules' => 'required|numeric',
				
			),
            array (
				'field' => 'seku_broker_fee_penjualan',
				'label' => 'Fee Penjualan',
				'rules' => 'required|numeric',
				
			),
		);
		
		$this->form_validation->set_rules($validasi);
		if ($this->form_validation->run()) {
			
			$kunci_data = array (
				'seku_nama',
				'seku_no_telp',
				'seku_fax',
                'seku_email',
                'seku_alamat',
                'seku_broker_fee_pembelian',
                'seku_broker_fee_penjualan'
			);
			
			return data_post($kunci_data);
		}
		else {
			$this->session->set_flashdata('status_simpan', 'tidak_lengkap');
			$this->session->set_flashdata('validation_errors', validation_errors());
			$this->session->set_flashdata('data_form', (object) $this->input->post());
			return null;
		}
	}
	
	public function tambah_data()
	{
		
		$data = $this->_data_form();
		
		if ($data != null) {
			$data['seku_created_by'] = $data['seku_updated_by'] = session_pengguna('peng_id');
            $data['seku_created_time']=$data["seku_updated_time"] =date('Y-m-d H:i:s');
			$this->db->insert('sekuritas', $data);
			$this->session->set_flashdata('status_simpan', 'ok');
		}
		
		redirect(site_url('/master/sekuritas/tambah'));
	}
	
	public function ubah_data()
	{
		$data = $this->_data_form();
		$seku_id= $this->input->post('seku_id');
		if ($data != null) {
			$data['seku_updated_time'] = date('Y-m-d H:i:s');
			$data['seku_updated_by'] = session_pengguna('peng_id');
			
			$where = array('seku_id' =>$seku_id);
			
			$this->db->update('sekuritas', $data, $where);
				
			$this->session->set_flashdata('status_simpan', 'ok');
		}
		
		redirect(site_url('/master/sekuritas/ubah/'.$seku_id));
		// redirect(site_url('/master/sekuritas/ubah'));
	}
	
    public function delete($seku_id){
		$penanda=cekTransaksiBeforeDelete("Sekuritas",$seku_id);
		if($penanda==0){
			$data['seku_updated_time'] = date('Y-m-d H:i:s');
			$data['seku_updated_by'] = session_pengguna('peng_id');
			$data['seku_is_deleted'] = "0";
			$where = array('seku_id' => $seku_id);
			
			$this->db->update('sekuritas', $data, $where);
			redirect(site_url('/master/sekuritas'));
		}
		else{
			echo "<script>
					alert('Bank ini Digunakan Dalam Transaksi');
					window.location.href='".base_url()."master/sekuritas';
					</script>";
		}
        
    }
}
