<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Saham extends MX_Controller {

	public function index()
	{
		$this->load->view('templates/site_tpl', array (
			'content' => 'saham_index',
		));
	}
	
	public function datatable()
	{
		$draw = $this->input->post('draw');
		$offset = $this->input->post('start');
		$num_rows = $this->input->post('length');
		$order_index = $_POST['order'][0]['column'];
		$order_by = $_POST['columns'][$order_index]['data'];
		$order_direction = $_POST['order'][0]['dir'];
		$keyword = $_POST['search']['value'];
		
		$bindings = array("%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%");
		
		$base_sql = "
			from saham
            join ref_pilihan a on sham_sektor=a.plhn_id
            join ref_pilihan b on sham_kinerja_kategori=b.plhn_id
			
            where
				sham_is_deleted = '1'
				and (
					sham_nama like ?
                    or sham_kode like ?
					or a.plhn_nama like ?
					or b.plhn_nama like ?
				)
                
		";
		
		$data_sql = "
			select
				saham.*,a.plhn_nama as sektor,b.plhn_nama as kategori
				, row_number() over (
					order by
						{$order_by} {$order_direction}
						, sham_nama {$order_direction}
				  ) as nomor
			{$base_sql}
			order by
				{$order_by} {$order_direction}
				, sham_nama {$order_direction}
			limit {$offset}, {$num_rows}
		";
                    
		$src = $this->db->query($data_sql, $bindings);
		// echo $this->db->last_query();
        // die();
		$count_sql = "
			select count(*) AS total
			{$base_sql}
		";
		$total_records = $this->db->query($count_sql, $bindings)->row('total');
		
		$data = array();
		
		foreach ($src->result() as $row) {
			$data[] = array (
				'sham_nama' => $row->sham_nama,
				'sham_id' => $row->sham_id,
				'sham_kode' => $row->sham_kode,
				'sham_sektor' => $row->sham_sektor,
				'sektor' => $row->sektor,
				'kategori' => $row->kategori,
                
                'no'=>$row->nomor,
			);
		}
		
		$response = array (
			'draw' => intval($draw),
			'iTotalRecords' => $src->num_rows(),
			'iTotalDisplayRecords' => $total_records,
			'aaData' => $data,
		);
		
		echo json_encode($response);
	}
	
	private function _form($aksi = 'tambah', $data = null)
	{
		if ($this->session->flashdata('data_form')) {
			$data = $this->session->flashdata('data_form');
		}
		
		$this->load->view('templates/site_tpl', array (
			'content' => 'saham_form',
			'url_aksi' => site_url("/master/saham/{$aksi}-data"),
			'data' => $data,
		));
	}
	
	public function tambah()
	{
		$this->_form();
	}
	
	public function ubah($id = '')
	{
		if ( ! $this->agent->referrer()) {
			show_404();
		}
		
		$src = $this->db
			->from('saham')
			->where('sham_is_deleted', '1')
			->where('sham_id', $id)
			->get();
		
		if ($src->num_rows() == 0) {
			show_404();
		}
		
		$this->_form('ubah', $src->row());
	}
	
	private function _data_form()
	{
		$validasi = array (
			array (
				'field' => 'sham_nama',
				'label' => 'Nama Saham',
				'rules' => 'required',
			),
			array (
				'field' => 'sham_kode',
				'label' => 'Kode Saham',
				'rules' => 'required',
			),
			array (
				'field' => 'sham_sektor',
				'label' => 'Sektor Saham',
				'rules' => 'required',
			),
            array (
				'field' => 'sham_kinerja_kategori',
				'label' => 'Kategori Kinerja',
				'rules' => 'required',
			),
			
		);
		
		$this->form_validation->set_rules($validasi);
		
		if ($this->form_validation->run()) {
			
			$kunci_data = array (
				'sham_nama',
				'sham_kode',
				'sham_sektor',
                'sham_kinerja_kategori',
				
			);
			
			return data_post($kunci_data);
		}
		else {
			$this->session->set_flashdata('status_simpan', 'tidak_lengkap');
			$this->session->set_flashdata('validation_errors', validation_errors());
			$this->session->set_flashdata('data_form', (object) $this->input->post());
			return null;
		}
	}
	
	public function tambah_data()
	{
		$data = $this->_data_form();
		
		if ($data != null) {
			$data['sham_created_by'] = $data['sham_updated_by'] = session_pengguna('peng_id');
            $data['sham_created_time']=$data["sham_updated_time"] =date('Y-m-d H:i:s');
			$this->db->insert('saham', $data);
			$this->session->set_flashdata('status_simpan', 'ok');
		}
		
		redirect(site_url('/master/saham/tambah'));
	}
	
	public function ubah_data()
	{
		$data = $this->_data_form();
		$sham_id=$this->input->post('sham_id');
		if ($data != null) {
			$data['sham_updated_time'] = date('Y-m-d H:i:s');
			$data['sham_updated_by'] = session_pengguna('peng_id');
			
			$where = array('sham_id' => $sham_id);
			
			$this->db->update('saham', $data, $where);
			$this->session->set_flashdata('status_simpan', 'ok');
		}
		
		redirect(site_url('/master/saham/ubah/'.$sham_id));
	}
	
    public function delete($sham_id){
        $penanda=cekTransaksiBeforeDelete("Saham",$sham_id);
		
		if($penanda==0){
			$data['sham_updated_time'] = date('Y-m-d H:i:s');
			$data['sham_updated_by'] = session_pengguna('peng_id');
			$data['sham_is_deleted'] = "0";
			$where = array('sham_id' => $sham_id);
			
			$this->db->update('saham', $data, $where);
			redirect(site_url('/master/saham'));
		}else{
			echo "<script>
					alert('Saham ini Digunakan Dalam Transaksi');
					window.location.href='".base_url()."master/saham';
					</script>";
		}
		
    }

	public function getSaham(){
		$akun_id=$this->input->post('akun_id');
		$src=$this->db->query("
			SELECT akun,sham_kode,sham_nama,sham_id FROM (
				(SELECT '' AS id,'1.saldo-awal' AS ket,shal_tgl_saldo_awal AS tgl,shal_akun_id AS akun,shal_sham_id AS saham,shal_jumlah_lot AS lot,shal_rata_rata_harga AS harga,shal_total AS netto
				FROM saldo_awal_saham
				JOIN akun ON shal_akun_id=akun_id
				WHERE shal_akun_id='$akun_id' AND akun_is_deleted='1'
				)
				UNION
				(SELECT pmbl_id,'2.pembelian',pmbl_tgl_beli,pmbl_akun_id,dbli_sham_id,dbli_jumlah_lot,dbli_harga_shares,dbli_netto
				FROM det_pembelian
				JOIN pembelian ON dbli_pmbl_id=pmbl_id
				WHERE pmbl_is_deleted='1' AND pmbl_akun_id='$akun_id'
				)
				UNION
				(SELECT pnjl_id,'3.penjualan',pnjl_tgl_jual,pnjl_akun_id,dpjl_sham_id,dpjl_jumlah_lot,dpjl_harga_shares,dpjl_netto
				FROM det_penjualan
				JOIN penjualan ON dpjl_pnjl_id=pnjl_id
				WHERE pnjl_is_deleted='1' AND pnjl_akun_id='$akun_id'
				)
			) AS dt
			JOIN saham ON saham=sham_id
			GROUP BY sham_kode
			ORDER BY saham,tgl,ket ASC
		");
		header('Content-Type: application/json');
		echo json_encode($src->result());
	}

	public function list_saham(){
		$src=$this->db->query("SELECT * FROM saham where sham_is_deleted='1'");
		header('Content-Type: application/json');
		echo json_encode($src->result());
	}
	public function ajax_saham(){
		$akun_id=$this->input->post('akun_id');
		$sham_id=$this->input->post('sham_id');
		$saldosahamawal=0;
		$saldopembeliansaham=0;
		$saldopenjualansaham=0;
		$srcsahamawal=$this->select('shal_id,shal_akun_id,akun_seku_id,shal_sham_id,shal_tgl_saldo_awal,shal_jumlah_lot,shal_jumlah_shares,shal_rata_rata_harga,shal_total ,shal_is_deleted ')
							 ->from('saldo_awal_saham')
							 ->JOIN('akun','shal_akun_id=akun_id')
							 ->JOIN('sekuritas','seku_id=akun_seku_id')
							 ->where(array('shal_akun_id'=>$akun_id,'shal_sham_id'=>$sham_id))
							 ->get()->row();
		if(!empty($srcsahamawal)){
			$saldosahamawal=$srcsahamawal->shal_jumlah_lot;
		}	
		$srcpembelian=$this->select('coalesce(sum(kshm_jumlah),0) as jumlah_lot')
						   ->from('keuangan_saham')
						   ->JOIN ('pembelian','kshm_transaksi_id=pmbl_id')
						   ->JOIN('akun','pmbl_akun_id=akun_id')
						   ->JOIN('sekuritas','seku_id=akun_seku_id')
						   ->WHERE(array('kshm_jns_transaksi'=>"pembelian",'pmbl_akun_id'=>$akun_id,'kshm_sham_id'=>$sham_id,'pmbl_is_deleted'=>'1'))
						   ->get()->row();
		if(!empty($srcpembelian)){
			$saldopembeliansaham=$srcpembelian->jumlah_lot;
		}

		$srcpenjualan=$this->select('coalesce(sum(kshm_jumlah),0) as jumlah_lot')
						   ->from('keuangan_saham')
						   ->JOIN ('penjualan','kshm_transaksi_id=pnjl_id')
						   ->JOIN('akun','pnjl_akun_id=akun_id')
						   ->JOIN('sekuritas','seku_id=akun_seku_id')
						   ->WHERE(array('kshm_jns_transaksi'=>"penjualan",'pnjl_akun_id'=>$akun_id,'kshm_sham_id'=>$sham_id,'pnjl_is_deleted'=>'1'))
						   ->get()->row();
		if(!empty($srcpenjualan)){
			$saldopenjualansahamsaham=$srcpenjualan->jumlah_lot;
		}
		$hasil=array(
			'saldosaham'=>$saldosahamawal+$saldopembeliansaham+$saldopenjualansaham,
		);
		header('Content-Type: application/json');
		echo json_encode($hasil);
	}
}
