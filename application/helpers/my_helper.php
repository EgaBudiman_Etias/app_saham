<?php

function konfigurasi_wablas()
{
	return array (
		'url_api' => 'https://console.wablas.com',
		'token' => 'scOkBZSnnCxMLIFhj26qUhiUdbLz04GyIgaQQpuIx5hMsSAKQDzTkj2rIUFmvmn2',
		'encode_url_api' => 'aHR0cHM6Ly9jb25zb2xlLndhYmxhcy5jb20=',
	);
}

function hash_kata_sandi($kata_sandi)
{
	return md5("SAHAM-{$kata_sandi}-BALAM");
}

function session_pengguna($kunci)
{
	$ci =& get_instance();
	
	$pengguna = $ci->session->userdata('pengguna');
	
	if ( ! $pengguna) {
		return null;
	}
	else {
		return $pengguna->$kunci;
	}
}


function session_menu()
{
	$ci =& get_instance();
	return $ci->session->userdata('menu');
}

function angka($integer)
{
	if ($integer == '') return null;
	else return number_format($integer, 0, '.', ',');
}

function rupiah($angka)
{
	if ($angka == '') return null;
	else if ($angka==0) return 'Rp0';
	else return 'Rp'.angka($angka);
}

function rupiah2($angka)
{
	if ($angka == 0) return 'Rp0';
	else return 'Rp'.number_format((float)$angka, 2, '.', ',');
}

function rupiah3($angka)
{
	if ($angka == 0) return 'Rp0';
	else return 'Rp'.number_format((float)$angka, 2, '.', ',');
}
function carisaldosaham($akun_id,$sham_id){
	$ci =& get_instance();
	$saldosahamawal=0;
	$saldopembeliansaham=0;
	$saldopenjualansaham=0;
	$srcsahamawal=$this->db->select('shal_id,shal_akun_id,akun_seku_id,shal_sham_id,shal_tgl_saldo_awal,shal_jumlah_lot,shal_jumlah_shares,shal_rata_rata_harga,shal_total ,shal_is_deleted ')
							 ->from('saldo_awal_saham')
							 ->JOIN('akun','shal_akun_id=akun_id')
							 ->JOIN('sekuritas','seku_id=akun_seku_id')
							 ->where(array('shal_akun_id'=>$akun_id,'shal_sham_id'=>$sham_id))
							 ->get()->row();
		
	if(!empty($srcsahamawal)){
		$saldosahamawal=$srcsahamawal->shal_jumlah_lot;
	}	
	$srcpembelian=$this->db->select('coalesce(sum(kshm_jumlah),0) as jumlah_lot')
						->from('keuangan_saham')
						->JOIN ('pembelian','kshm_transaksi_id=pmbl_id')
						->JOIN('akun','pmbl_akun_id=akun_id')
						->JOIN('sekuritas','seku_id=akun_seku_id')
						->WHERE(array('kshm_jns_transaksi'=>"pembelian",'pmbl_akun_id'=>$akun_id,'kshm_sham_id'=>$sham_id,'pmbl_is_deleted'=>'1'))
						->get()->row();
					
	if(!empty($srcpembelian)){
		$saldopembeliansaham=$srcpembelian->jumlah_lot;
	}

	$srcpenjualan=$this->db->select('coalesce(sum(kshm_jumlah),0) as jumlah_lot')
						->from('keuangan_saham')
						->JOIN ('penjualan','kshm_transaksi_id=pnjl_id')
						->JOIN('akun','pnjl_akun_id=akun_id')
						->JOIN('sekuritas','seku_id=akun_seku_id')
						->WHERE(array('kshm_jns_transaksi'=>"penjualan",'pnjl_akun_id'=>$akun_id,'kshm_sham_id'=>$sham_id,'pnjl_is_deleted'=>'1'))
						->get()->row();
					
	if(!empty($srcpenjualan)){
		$saldopenjualansahamsaham=$srcpenjualan->jumlah_lot;
	}
	return $saldosahamawal+$saldopembeliansaham+$saldopenjualansaham;
}
function kode_akun(){
	//AK0001
	$ci =& get_instance();
	$query = $ci->db->query("SELECT RIGHT(akun_kode,4) as kode FROM akun ORDER by akun_id desc limit 1 ");
	
	if($query->num_rows() <> 0){      
	$data = $query->row();
	$kode = intval($data->kode) + 1;
	}
	else {       
	$kode = 1;
	}
	$kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
	$kodejadi = 'AK'.$kodemax;
	return $kodejadi;
}

function pajak_penjualan(){
	//AK0001
	$ci =& get_instance();
	$query = $ci->db->query("SELECT * from konfigurasi WHERE konf_kode='pajak-penjualan' limit 1 ")->row();
	
	return $query->konf_nilai;
}

function kode_setoran(){
	//AK0001
	$ci =& get_instance();
	$tgl=date('Y-m');
	$query = $ci->db->query("SELECT RIGHT(stor_kode,4) as kode FROM setoran where stor_tgl like '%$tgl%' ORDER by stor_id desc limit 1 ");
	// echo $ci->db->last_query();
	if($query->num_rows() <> 0){      
		$data = $query->row();
		$kode = intval($data->kode) + 1;
	}
	else {       
		$kode = 1;
	}
	$kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
	$kodejadi = 'S'.date('Ym').$kodemax;
	
	return $kodejadi;
}

function kode_deviden(){
	//AK0001
	$ci =& get_instance();
	$tgl=date('Y-m');
	$query = $ci->db->query("SELECT RIGHT(devi_kode,4) as kode FROM deviden where devi_tgl_bagi like '%$tgl%' ORDER by devi_id desc limit 1 ");
	// echo $ci->db->last_query();
	if($query->num_rows() <> 0){      
		$data = $query->row();
		$kode = intval($data->kode) + 1;
	}
	else {       
		$kode = 1;
	}
	$kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
	$kodejadi = 'D'.date('Ym').$kodemax;
	
	return $kodejadi;
}
function kode_penarikan(){
	//AK0001
	$ci =& get_instance();
	$tgl=date('Y-m');
	$query = $ci->db->query("SELECT RIGHT(trik_kode,4) as kode FROM penarikan where trik_tgl like '%$tgl%' ORDER by trik_id desc limit 1 ");
	// echo $ci->db->last_query();
	if($query->num_rows() <> 0){      
		$data = $query->row();
		$kode = intval($data->kode) + 1;
	}
	else {       
		$kode = 1;
	}
	$kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
	$kodejadi = 'T'.date('Ym').$kodemax;
	
	return $kodejadi;
}
function kode_penjualan(){
	//AK0001
	$ci =& get_instance();
	$tgl=date('Y-m');
	$query = $ci->db->query("SELECT RIGHT(pnjl_kode,4) as kode FROM penjualan where pnjl_tgl_jual like '%$tgl%' ORDER by pnjl_id desc limit 1 ");
	// echo $ci->db->last_query();
	if($query->num_rows() <> 0){      
		$data = $query->row();
		$kode = intval($data->kode) + 1;
	}
	else {       
		$kode = 1;
	}
	$kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
	$kodejadi = 'J'.date('Ym').$kodemax;
	
	return $kodejadi;
}

function kode_pembelian(){
	//AK0001
	$ci =& get_instance();
	$tgl=date('Y-m');
	$query = $ci->db->query("SELECT RIGHT(pmbl_kode,4) as kode FROM pembelian where pmbl_tgl_beli like '%$tgl%' ORDER by pmbl_id desc limit 1 ");
	// echo $ci->db->last_query();
	if($query->num_rows() <> 0){      
		$data = $query->row();
		$kode = intval($data->kode) + 1;
	}
	else {       
		$kode = 1;
	}
	$kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
	$kodejadi = 'B'.date('Ym').$kodemax;
	
	return $kodejadi;
}

function options_sekuritas($selected=''){
	$ci =& get_instance();
	$query = $ci->db->query("SELECT * FROM sekuritas where seku_is_deleted='1' order by seku_nama asc")->result();

	$options = "";

	foreach ($query as $data) {
		$options .= "<option value='$data->seku_id' " .($selected == $data->seku_id ? 'selected' : ''). ">$data->seku_nama</option>";
	}

	return $options;
}


function options_akun($selected=''){
	$ci =& get_instance();
	$query = $ci->db->query("SELECT * FROM akun join sekuritas on seku_id=akun_seku_id  where akun_is_deleted='1' order by seku_nama asc ")->result();

	$options = "";

	foreach ($query as $data) {
		$options .= "<option value='$data->akun_id' " .($selected == $data->akun_id ? 'selected' : ''). ">$data->seku_nama-No SID : $data->akun_no_sid-Bank RDN : $data->akun_bank_rdn-NO KSEI/KPEI : $data->akun_no_ksei_kpei</option>";
	}

	return $options;
}

function options_akun2($selected=''){
	$ci =& get_instance();
	$query = $ci->db->query("SELECT * FROM akun join sekuritas on seku_id=akun_seku_id  where akun_is_deleted='1' order by seku_nama asc ")->result();

	$options = "";

	foreach ($query as $data) {
		$options .= "<option value='$data->akun_id' " .($selected == $data->akun_id ? 'selected' : ''). ">$data->seku_nama - No SID : $data->akun_no_sid</option>";
	}

	return $options;
}
function options_bank($selected=''){
	$ci =& get_instance();
	$query = $ci->db->query("SELECT * FROM bank where bank_is_deleted='1' order by bank_judul asc")->result();

	$options = "";

	foreach ($query as $data) {
		$options .= "<option value='$data->bank_id' " .($selected == $data->bank_id ? 'selected' : ''). ">$data->bank_judul</option>";
	}

	return $options;
}

function strip_num_separator($str_num)
{
	$num = trim($str_num);
	$num = str_replace(',', '', $num);
	return $num;
}

function strip_num_separator2($str_num)
{
	$num = trim($str_num);
	$num = str_replace('.', '', $num);
	return $num;
}


function options_refpilihan($selected=''){
	$ci =& get_instance();
	

	$options = "";

	$options .= "<option value='kategori-saham' " .($selected == "kategori-saham" ? 'selected' : ''). ">kategori-saham</option>";
	$options .= "<option value='sektor-saham' " .($selected == "sektor-saham" ? 'selected' : ''). ">sektor-saham</option>";
	return $options;
}
function options_saham_sekuritas($selected="",$sekuritas){
	$ci =& get_instance();
	$query = $ci->db->query("SELECT * FROM saham where sham_seku_id='$sekuritas' where sham_is_deleted='1'")->result();

	$options = "";

	foreach ($query as $data) {
		$options .= "<option value='$data->sham_id' " .($selected == $data->sham_id ? 'selected' : ''). ">$data->sham_nama($data->sham_kode)</option>";
	}

	return $options;
}

function options_akun_judul($selected="",$sekuritas){
	$ci =& get_instance();
	$query = $ci->db->query("SELECT * FROM saham where sham_seku_id='$sekuritas' where sham_is_deleted='1'")->result();

	$options = "";

	foreach ($query as $data) {
		$options .= "<option value='$data->sham_id' " .($selected == $data->sham_id ? 'selected' : ''). ">$data->sham_nama($data->sham_kode)</option>";
	}

	return $options;
}

function options_saham($selected=""){
	$ci =& get_instance();
	$query = $ci->db->query("SELECT * FROM saham where sham_is_deleted='1' order by sham_kode asc")->result();

	$options = "";

	foreach ($query as $data) {
		$options .= "<option value='$data->sham_id' " .($selected == $data->sham_id ? 'selected' : ''). ">$data->sham_kode - $data->sham_nama</option>";
	}

	return $options;
}

function options_saham_jual($selected=""){
	$ci =& get_instance();
	$query = $ci->db->query("SELECT * FROM saham where sham_is_deleted='1' and sham_id='$selected' order by sham_kode asc")->result();

	$options = "";

	foreach ($query as $data) {
		$options .= "<option value='$data->sham_id' " .($selected == $data->sham_id ? 'selected' : ''). ">$data->sham_kode - $data->sham_nama</option>";
	}

	return $options;
}
function options_pilihan($selected='',$kategori){
	$ci =& get_instance();
	$query = $ci->db->query("SELECT * FROM ref_pilihan where plhn_is_deleted='1' and plhn_kategori='$kategori' ")->result();

	$options = "";

	foreach ($query as $data) {
		$options .= "<option value='$data->plhn_id' " .($selected == $data->plhn_id ? 'selected' : ''). ">$data->plhn_nama</option>";
	}

	return $options;
}
function options($src, $id, $ref_val, $text_field, $data_attr = array())
{
	$options = '';
	
	foreach ($src->result() as $row) {
		
		$opt_value	= $row->$id;
		$text_value	= $row->$text_field;
		
		$data_attr_str = '';
		
		foreach ($data_attr as $class => $data_field) {
			$data_attr_str .= 'data-'.$class.'="'.$row->$data_field.'" ';
		}
		
		if ($row->$id == $ref_val) {
			$options .= '<option value="'.$opt_value.'" '.$data_attr_str.'selected>'.$text_value.'</option>';
		}
		else {
			$options .= '<option value="'.$opt_value.'" '.$data_attr_str.'>'.$text_value.'</option>';
		}
	}
	
	return $options;
}

function options_multiple($src, $id, $ref_val = array(), $text_field, $data_attr = array())
{
	$options = '';
	
	foreach ($src->result() as $row) {
		
		$opt_value	= $row->$id;
		$text_value	= $row->$text_field;
		
		$data_attr_str = '';
		
		foreach ($data_attr as $class => $data_field) {
			$data_attr_str .= 'data-'.$class.'="'.$row->$data_field.'" ';
		}
		
		if (in_array($row->$id, $ref_val)) {
			$options .= '<option value="'.$opt_value.'" '.$data_attr_str.'selected>'.$text_value.'</option>';
		}
		else {
			$options .= '<option value="'.$opt_value.'" '.$data_attr_str.'>'.$text_value.'</option>';
		}
	}
	
	return $options;
}

function data_post($kunci = array())
{
	$ci =& get_instance();
	$data = array();
	
	foreach ($kunci as $k_str) {
		
		$k_arr = explode('|', $k_str);
		
		if (isset($k_arr[1])) {
			switch ($k_arr[1]) {
				case 'number': {
					$nilai = trim($ci->input->post($k_arr[0]));
					$nilai = str_replace(',', '', $nilai);
					break;
				}
			}
		}
		else {
			$nilai = trim($ci->input->post($k_arr[0]));
		}
		
		if ($nilai != '') {
			$data[$k_arr[0]] = $nilai;
		}
	}
	
	return $data;
}

function nomor($kode)
{
	$ci =& get_instance();
	
	$nomor = $ci->db
		->from('nomor')
		->where('is_hapus', 0)
		->where('kode', $kode)
		->get()
		->row();
	
	if ($nomor->tahun_sekarang == date('Y')) {
		if ($nomor->bulan_sekarang == date('m')) {
			$serial = $nomor->serial_berikutnya;
			$update = array('serial_berikutnya' => $serial + 1);
		}
		else {
			$update = array (
				'bulan_sekarang' => date('m'),
			);
			
			if ($nomor->reset_serial == 'bulanan') {
				$serial = 1;
				$update['serial_berikutnya'] = 2;
			}
			else {
				$serial = $nomor->serial_berikutnya;
				$update['serial_berikutnya'] = $serial + 1;
			}
		}
	}
	else {
		$serial = 1;
		$update = array (
			'tahun_sekarang' => date('Y'),
			'bulan_sekarang' => date('m'),
			'serial_berikutnya' => 2,
		);
	}
	
	$update['waktu_ubah'] = date('Y-m-d H:i:s');
	$where = array('kode' => $kode);
	$ci->db->update('nomor', $update, $where);
	
	$serial_str = str_pad($serial, $nomor->digit_serial, '0', STR_PAD_LEFT);
	
	$wildcard = array('@y4', '@y2', '@m', '@serial');
	$replace = array(date('Y'), date('y'), date('m'), $serial_str);
	
	return str_replace($wildcard, $replace, $nomor->format_nomor);
}

function status($kode)
{
	return strtoupper(str_replace('_', ' ', $kode));
}

function kata_sandi_acak()
{
	$vokal = array('a', 'i', 'u', 'e', 'o');
	$konsonan = array();
	
	for ($i = 'a'; $i <= 'z'; $i++) {
		if (array_search($i, $vokal) === false) {
			$konsonan[] = $i;
		}
	}
	
	$kata_sandi = '';
	
	for ($i = 1; $i <= 6; $i++) {
		if ($i % 2 == 1) { # GANJIL = KONSONAN
			$idx = mt_rand(0, 20);
			$kata_sandi .= $konsonan[$idx];
		}
		
		if ($i % 2 == 0) { # GENAP = VOKAL
			$idx = mt_rand(0, 4);
			$kata_sandi .= $vokal[$idx];
		}
	}
	
	return $kata_sandi;
}

function cleansing_no_hp($no_hp)
{
	# HILANGKAN MINUS (-)
	$no_hp = str_replace('-', '', $no_hp);
	
	# HILANGKAN SPASI
	$no_hp = str_replace(' ', '', $no_hp);
	
	# HILANGKAN TITIK
	$no_hp = str_replace('.', '', $no_hp);
	
	# GANTI +62 DENGAN 0
	$no_hp = str_replace('+62', '0', $no_hp);
	
	# JIKA NOMOR DEPAN 62, GANTI DENGAN 0
	if (substr($no_hp, 0, 2) == '62') {
		$no_belakang = substr($no_hp, 2);
		$no_hp = '0'.$no_belakang;
	}
	
	return $no_hp;
}

function wablas_status()
{
	$wablas = konfigurasi_wablas();
	
	$response = json_decode(file_get_contents($wablas['url_api'].'/api/device/info?token='.$wablas['token']));
	
	$status = 'disconnected';
	
	if (isset($response->data->whatsapp->status)) {
		$status = $response->data->whatsapp->status;
	}
	
	return $status;
}

function wablas_url_qr()
{
	$wablas = konfigurasi_wablas();
	$url = $wablas['url_api'].'/generate/qr.php?token='.$wablas['token'].'&url='.$wablas['encode_url_api'];
	return $url;
}

function wablas_kirim($no_wa_tujuan, $pesan = '')
{
	$no_wa_tujuan = cleansing_no_hp($no_wa_tujuan);
	
	$wablas = konfigurasi_wablas();
	
	if ($pesan == '') $pesan = 'Uji coba pesan WA dari ASNET. Waktu kirim: '.date('d M Y H:i:s');
	
	$curl = curl_init();
	
	$data = array (
		'phone' => $no_wa_tujuan,
		'message' => $pesan,
	);

	curl_setopt ($curl, CURLOPT_HTTPHEADER,
		array (
			"Authorization: {$wablas['token']}",
		)
	);
	
	curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	curl_setopt($curl, CURLOPT_URL, $wablas['url_api'].'/api/send-message');
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
	
	$response = curl_exec($curl);
	curl_close($curl);
}

function kode_unik($nominal)
{
	$ci =& get_instance();
	
	$maks = 999;
	
	$sql = "
		select total - ? as kode_unik
		from invoice
		where
			status = 'belum_lunas'
			and floor(total / 1000) * 1000 = ?
			and is_hapus = 0
	";
	
	$src = $ci->db->query($sql, array($nominal, $nominal));
	
	if ($src->num_rows() == 0) {
		return mt_rand(1, $maks);
	}
	else {
		/*
		$kode_unik_arr = array();
		
		foreach ($src->result() as $row) {
			$kode_unik_arr[] = $row->kode_unik;
		}
		
		do {
			$kode_unik = mt_rand(1, $maks);
		}
		while (in_array($kode_unik, $kode_unik_arr));
		*/
		
		$kode_unik = mt_rand(1, $maks);
		return $kode_unik;
	}
}

function kode_unik_serial($tagihan)
{
	$ci =& get_instance();
	
	$sql = "
		select kode_unik
		from pelanggan_layanan
		where
			is_hapus = 0
			and status != 'off'
			and tagihan = ?
			and kode_unik != 0
		order by kode_unik
	";
	
	$src = $ci->db->query($sql, array($tagihan));
	
	$i = 1;
	
	foreach ($src->result() as $row) {
		if ($row->kode_unik != $i) {
			return $i;
		}
		
		$i++;
	}
	
	return $i;
}

function tgl_isolir_baru($tgl_numerik){

	$tgl_numerik = (int) $tgl_numerik;
	
	$tgl_array = array(
			'1' => 15,
			'2' => 16,
			'3' => 17,
			'4' => 18,
			'5' => 19,
			'6' => 20,
			'7' => 21,
			'8' => 22,
			'9' => 23,
			'10' => 24,
			'11' => 25,
			'12' => 26,
			'13' => 27,
			'14' => 28,
			'15' => 28,
			'16' => 1,
			'17' => 2,
			'18' => 3,
			'19' => 4,
			'20' => 5,
			'21' => 6,
			'22' => 7,
			'23' => 8,
			'24' => 9,
			'25' => 10,
			'26' => 11,
			'27' => 12,
			'28' => 13,
			'29' => 14,
			'30' => 15,
			'31' => 16,
		);

	return $tgl_array[$tgl_numerik];

}

function filterdata($data)
{
    $param = "";
    $totalparam = 0;
    foreach ($data as $index => $val) {
        $param .= $val != "" ? "/".$val : "/-";
        $totalparam += $val != "" ? 1 : 0;
    }

    $filer = $totalparam > 0 ? "/index".$param : "";

    return $filer;
}

function import_xls($data)
{
    $param = "";
    $totalparam = 0;
    foreach ($data as $index => $val) {
        $param .= $val != "" ? "/".$val : "/-";
        $totalparam += $val != "" ? 1 : 0;
    }

    $filer = $totalparam > 0 ? $param : "";

    return $filer;
}

function cekTransaksiBeforeDelete($master,$id){
	$ci =& get_instance();
	$hasil=0;
	if($master=="Bank"){
		$hasil=$ci->db->query("(SELECT * FROM setoran where stor_bank_id='$id' and stor_is_deleted='1') UNION (SELECT * FROM penarikan where trik_bank_id='$id' and trik_is_deleted='1')")->num_rows();
	}else if($master=="Sekuritas"){
		$hasil=$ci->db->query("
		(SELECT pmbl_id from pembelian JOIN akun on pmbl_akun_id =akun_id JOIN sekuritas on seku_id=akun_seku_id where seku_id='$id' and pmbl_is_deleted='1')
		UNION
		(SELECT pnjl_id from penjualan JOIN akun on pnjl_akun_id =akun_id JOIN sekuritas on seku_id=akun_seku_id where seku_id='$id' and pnjl_is_deleted='1')
		UNION
		(SELECT devi_id from deviden JOIN akun on devi_akun_id =akun_id JOIN sekuritas on seku_id=akun_seku_id where seku_id='$id' and devi_is_deleted='1')
		
		")->num_rows();
	}else if($master=="Saham"){
		$hasil=$ci->db->query("
			(SELECT dbli_id FROM det_pembelian JOIN pembelian on (pmbl_id=dbli_pmbl_id) where dbli_sham_id='$id' and pmbl_is_deleted='1')
			UNION
			(SELECT dpjl_id FROM det_penjualan JOIN penjualan on (pnjl_id=dpjl_pnjl_id) where dpjl_sham_id='$id' and pnjl_is_deleted='1')
			UNION
			(SELECT ddev_id FROM det_deviden JOIN deviden on (devi_id=ddev_devi_id) where ddev_sham_id='$id' and devi_is_deleted='1')
		")->num_rows();
	}else if($master=="Akun"){
		$hasil=$ci->db->query("
		(SELECT pmbl_id from pembelian JOIN akun on pmbl_akun_id =akun_id JOIN sekuritas on seku_id=akun_seku_id where akun_id='$id' and pmbl_is_deleted='1')
		UNION
		(SELECT pnjl_id from penjualan JOIN akun on pnjl_akun_id =akun_id JOIN sekuritas on seku_id=akun_seku_id where akun_id='$id' and pnjl_is_deleted='1')
		UNION
		(SELECT devi_id from deviden JOIN akun on devi_akun_id =akun_id JOIN sekuritas on seku_id=akun_seku_id where akun_id='$id' and devi_is_deleted='1')
		
		")->num_rows();
	}
	return $hasil;
}