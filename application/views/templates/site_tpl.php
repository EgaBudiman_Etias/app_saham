<!DOCTYPE HTML>
<html>
<head>
	<title>APP Saham System</title>
	
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta name="robots" content="noindex, nofollow">
	<meta name="googlebot" content="noindex, nofollow">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<link rel="icon" href="<?php echo base_url(); ?>assets/img/logo.jpg" type="image/png">
	
	<!-- GOOGLE FONTS -->
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap">
	
	<!-- THEMIFY ICONS -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
	
	<!-- BOOTSTRAP & SITE CSS -->
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/site.css">
	
	<!-- JQUERY & BOOTSTRAP JS -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.4.1.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js"></script>
	
	<!-- DATATABLES -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/DataTables/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/DataTables/datatables.custom.css">
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/DataTables/datatables.min.js"></script>
	<link href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css" rel="stylesheet">
	<script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
	
	<!-- JQUERY NUMBER -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery.number.min.js"></script>
	
	<!-- ZEBRA DATEPICKER -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/zebra_datepicker/css/bootstrap/zebra_datepicker.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/zebra_datepicker/css/zebra_datepicker.custom.css">
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/zebra_datepicker/zebra_datepicker.min.js"></script>
	
	<!-- SELECT2 -->
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/select2.custom.css">
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
	
	<!-- js custom -->
	<script type="text/javascript" src="<?=base_url()?>assets/js/app.js"></script>
	
	
	<script type="text/javascript">
	var site_url = '<?php echo site_url(); ?>';
	
	function warna_status(status)
	{
		switch (status) {
			case 'AKTIF':
			case 'CLOSED':
			case 'LUNAS':
			case 'DONE':
				return '<span class="status-green">'+status+'</span>';
			
			case 'BATAL':
			case 'PENDING':
				return '<span class="status-gray">'+status+'</span>';
			
			case 'BARU':
			case 'OPEN':
			case 'OFF':
			case 'NON AKTIF':
			case 'BELUM LUNAS':
				return '<span class="status-red">'+status+'</span>';
			
			case 'CAPEL':
				return '<span class="status-brown">'+status+'</span>';
			
			case 'ISOLIR':
			case 'LEWAT':
			case 'INSTALASI':
			case 'HOLD':
				return '<span class="status-purple">'+status+'</span>';
			
			case 'PEMBAYARAN':
				return '<span class="status-cyan">'+status+'</span>';
			
			case 'ACTION':
				return '<span class="status-blue">'+status+'</span>';
			
			default:
				return status;
		}
	}
	
	$().ready(function() {
		$('input').attr('autocomplete', 'off');
		$('input.control-number').number(true, 2, ',', '.');
		$('input.control-number-no').number(true, 0);
		$('input.control-number-digit').number(true, 2);
		$('.angka').number( true, 0 );
		$('.select2').select2({ allowClear: true });
		$('.akun').select2({
			placeholder: "--Pilih Akun--",
			templateResult: formatDesign,
			allowClear: true
			
		});
		$('.select2-no-clear').select2({ allowClear: false });
		$('.select2-tags').select2({ tags: true });
		$('.select2-modal').select2({ allowClear: true, dropdownParent: $('.modal') });
    	$('.select2-modal-noclear').select2({ allowClear: false, dropdownParent: $('.modal') });
		$('.tanggalan1').Zebra_DatePicker({
			format: 'd M Y'
		});
		$('.tanggalan').Zebra_DatePicker({
			default_position: 'below'
			
		});

		function formatDesign(item) {
			
			var selectionText = item.text.split("-");
			var $returnString = $('<span><b>'+selectionText[0] + '</b></br>' + selectionText[1] + '<br>'+selectionText[2]+'<br>'+selectionText[3]+'<br>'+'</span>');
			return $returnString;
		};
	});
	</script>
	<style>
		td.highlight {
			/* font-weight: bold; */
			color: red;
		}
		td.highlightijo {
			/* font-weight: bold; */
			color: green;
		}
		/* as */
		.warnanyamerah{
			
			color: red;
		}
		.warnanyabiru{
			
			color: blue;
		}
		.tebel{
			font-weight: bold;
			font-size:30px;
		}
	</style>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top">
		<a class="navbar-brand" href="<?php echo site_url('/dasboard'); ?>">SAHAM.</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<?php $module = $this->uri->segment(1); ?>
		<?php $menu_arr = session_menu(); ?>
		
		<div class="collapse navbar-collapse" id="navbarNavDropdown">
			<ul class="navbar-nav">
				<?php foreach ($menu_arr as $kode => $menu) { ?>
				<?php if (isset($menu['sub'])) { ?>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="<?php echo site_url($menu['uri']); ?>" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="ti ti-<?php echo $menu['ikon']; ?>"></i> <?php echo $menu['teks']; ?>
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						<?php foreach ($menu['sub'] as $kode_sub_menu => $sub_menu) { ?>
						<a class="dropdown-item" href="<?php echo site_url($sub_menu['uri']); ?>"><?php echo $sub_menu['teks']; ?></a>
						<?php } ?>
					</div>
				</li>
				<?php } else { ?>
				<li class="nav-item <?php if ($menu['kode'] == $module) echo 'active'; ?>">
					<a class="nav-link" href="<?php echo site_url($menu['uri']); ?>">
						<i class="ti ti-<?php echo $menu['ikon']; ?>"></i> <?php echo $menu['teks']; ?>
					</a>
				</li>
				<?php } ?>
				<?php } ?>
			</ul>
			<ul class="nav navbar-nav ml-auto">
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="accountDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="ti ti-user"></i> <?php echo session_pengguna('peng_nama_lengkap'); ?>
					</a>
					<div class="dropdown-menu dropdown-menu-right" aria-labelledby="accountDropdown">
						<a class="dropdown-item" href="<?php echo site_url('/pengguna/profil'); ?>"><i class="ti ti-id-badge"></i> Profil</a>
						<a class="dropdown-item" href="<?php echo site_url('/pengguna/ubah-kata-sandi'); ?>"><i class="ti ti-key"></i> Ubah Kata Sandi</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="<?php echo site_url('/pengguna/keluar'); ?>"><i class="ti ti-power-off"></i> Keluar Aplikasi</a>
					</div>
				</li>
			</ul>
		</div>
	</nav>
	<main class="container-fluid">
		<?php if (isset($content)) $this->load->view($content); ?>
	</main>
</body>
</html>