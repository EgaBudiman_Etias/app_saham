<!DOCTYPE HTML>
<html>
<head>
	<title>APP SAHAM</title>
	
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta name="robots" content="noindex, nofollow">
	<meta name="googlebot" content="noindex, nofollow">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon.png" type="image/png">
	
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/login.css">
	
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js"></script>
	
	<script>
	$().ready(function() {
		$('input').attr('autocomplete', 'off');
	});
	</script>
</head>
<body>
<div class="container-fluid">
	<div class="row no-gutter">
		<div class="d-none d-md-flex col-md-4 col-lg-6 bg-image"></div>
		<div class="col-md-8 col-lg-6">
			<div class="login d-flex align-items-center py-5">
				<div class="container">
					<div class="row">
						<div class="col-md-9 col-lg-8 mx-auto">
							<h2 class="login-heading mb-4 text-center">APP Saham</h2>
							
							<?php if ($this->session->flashdata('status_masuk') == 'gagal') { ?>
							<div class="alert alert-danger">Gagal masuk aplikasi! Mohon periksa Nama Pengguna atau Kata Sandi.</div>
							<?php } ?>
							
							<form method="post" action="<?php echo site_url('/pengguna/masuk'); ?>">
								<div class="form-label-group">
									<input type="text" name="peng_nama" id="peng_nama" class="form-control" placeholder="Nama Pengguna" required autofocus>
									<label for="peng_nama">Nama Pengguna</label>
								</div>
								<div class="form-label-group">
									<input type="password" name="peng_kata_sandi" id="peng_kata_sandi" class="form-control" placeholder="Kata Sandi" required>
									<label for="peng_kata_sandi">Kata Sandi</label>
								</div>
								<div class="custom-control custom-checkbox mb-3">
									<input type="checkbox" class="custom-control-input" name="ingat_saya" id="ingat-saya" value="1">
									<label class="custom-control-label" for="ingat-saya">Ingat Saya</label>
								</div>
								<button class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">
									Masuk Aplikasi
								</button>
								<!-- <div class="text-center">
									<a class="small" href="<?php echo site_url('/pengguna/lupa-password'); ?>">Lupa Kata Sandi</a>
								</div> -->
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>