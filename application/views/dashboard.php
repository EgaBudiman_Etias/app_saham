<div class="row">
    <div class="col-md-4">
        <div class="card text-white bg-primary">
            
            <div class="card-body text-center">
                <h1><strong><?=$portofoliosaham?></strong></h1>
                
                <span>Portofolio Saham</span>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="card text-white bg-success">
            
            <div class="card-body text-center">
                <h1><strong>
                    <?=$gainlos?></strong></h1>
                    
                <span>Gain/Loss</span>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card text-white bg-danger">
            
            <div class="card-body text-center">
                <h1><strong><?=!empty($saldokas)?$saldokas:'Rp0'?></strong></h1>
                
                <span>Saldo Kas</span>
            </div>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-4">
        <div class="card">
            <div class="card-body">
                <?php foreach($akunsaham as $ak){
                ?>
                <table class="table table-bordered" >
                    <tbody>
                        <tr>
                            <td colspan='2' class="bg-dark text-white">
                                <strong><?=$ak['seku_nama']."  ".$ak['akun_no_sid']?></strong>
                            </td>
                            
                        </tr>
                        <?php
                            foreach($ak['saham'] as $s){
                        ?>
                        <tr>
                            <td><?=$s->sham_kode?></td>
                            <td class="text-right <?=$s->total<0?'highlight':'highlightijo' ?>">
                                <!-- <?=rupiah($s->total)?> -->
                                <?=$s->total<0?rupiah2($s->total*-1):rupiah2($s->total)?>
                            </td>
                        </tr>
                        <?php
                            }
                        ?>
                    </tbody>
                    
                </table>
                <?php 
                }?>
            </div>
        </div>
        
       
    </div>
    <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                <table class="table table-bordered" >
                    <tbody>
                        <tr>
                            <th colspan='5' class="bg-dark text-white">
                                Penjualan 30 Hari terakhir
                            </th>
                            
                        </tr>
                        <tr>
                            <th>Tanggal</th>
                            <th>Saham</th>
                            <th>Lot</th>
                            <th>Harga Jual</th>
                            <th>Gain Loss</th>
                        </tr>
                        <?php 
                           
                            
                            if(!empty($penjualan)){
                                foreach($penjualan as $h){
                                    
                                        
                        ?>
                            <tr>
                                <td>
                                    <?=$h->kshm_tgl_transaksi?>
                                </td>
                                <td>
                                    <?=$h->sham_kode?>
                                </td>
                                <td class="text-right">
                                    <?=angka($h->kshm_jumlah*-1)?>
                                </td>
                                <td class="text-right">
                                    <?=rupiah2($h->kshm_harga_rata_rata)?>
                                </td>
                                <td class="text-right <?=$h->kshm_gl<0?'highlight':'highlightijo' ?>" >
                                    <?=$h->kshm_gl<0?"(".rupiah2($h->kshm_gl*-1).")":"+".rupiah2($h->kshm_gl)?>
                                </td>
                            </tr>
                        <?php
                                }
                            }
                        
                        ?>
                    </tbody>
                    
                </table>

                <table class="table table-bordered" >
                    <tbody>
                        <tr>
                            <th colspan='5' class="bg-dark text-white">
                                Pembelian 30 Hari terakhir
                            </th>
                            
                        </tr>
                        <tr>
                            <th>Tanggal</th>
                            <th>Saham</th>
                            <th>Lot</th>
                            <th>Harga Beli</th>
                            <th>New Average</th>
                        </tr>
                        <?php 
                            $tglskr=date('Y-m-d');
                            $tglblnlalu = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-1 month" ) );
                            if(!empty($pembelian)){
                                foreach($pembelian as $h){
                                    if($h['tgl']>$tglblnlalu && $h['tgl']<=$tglskr){
                        ?>
                            <tr>
                                <td>
                                    <?=$h['tgl']?>
                                </td>
                                <td>
                                    <?=$h['saham_kode']?>
                                </td>
                                <td class="text-right">
                                    <?=angka($h['lot'])?>
                                </td>
                                <td class="text-right">
                                    <?=rupiah2($h['harga'])?>
                                </td>
                                <td class="text-right">
                                    <?=rupiah2($h['harga_rata_rata'])?>
                                </td>
                            </tr>
                        <?php
                                };
                            };
                        };
                        ?>
                    </tbody>
                    
                </table>
                <table class="table table-bordered" >
                    <tbody>
                        <tr>
                            <th colspan='5' class="bg-dark text-white">
                                Saldo Kas
                            </th>
                            
                        </tr>
                        <tr>
                            <th>Nama Akun</th>
                            <th>Nominal</th>
                            
                        </tr>
                        <?php 
                            
                            foreach($saldokasakun as $h){
                                $nominal=$h->awal+$h->setoran+$h->penarikan+$h->pembelian+$h->penjualan+$h->deviden;
                                 
                        ?>
                            <tr>
                                <td>
                                    <?=$h->seku_nama.' '.$h->akun_no_sid?>
                                </td>
                               
                                <td class="text-right <?=$nominal<0?'highlight':'highlightijo' ?>">
                                   
                                    <?php 
                                    
                                    
                                    echo rupiah2($nominal);
                                    
                                    ?>
                                   
                                </td>
                               
                            </tr>
                        <?php
                                
                            }
                        
                        ?>
                    </tbody>
                    
                </table>
            </div>
        </div>
    </div>
</div>


