<?php

function cek_session()
{
	$ci =& get_instance();
	
	$ci->load->model('pengguna/pengguna_grup_model');
	
	if ( ! session_pengguna('peng_id')) {
		$cookie = $ci->input->cookie('saham_cookie');
		
		$src = $ci->db
			->select('
				peng_id,peng_nama,peng_cookie,peng_nama_lengkap
			')
			->from('pengguna')
			
			->where('peng_cookie', $cookie)
			->where('peng_cookie is not null')
			->where('peng_is_deleted', 1)
			
			->get();
		
		if ($src->num_rows() == 1) {
			$pengguna = $src->row();
			$menu = $ci->pengguna_grup_model->menu($pengguna->peng_id);
			
			$ci->session->set_userdata('pengguna', $pengguna);
			$ci->session->set_userdata('menu', $menu);
		}
	}
	
	$controller = strtolower($ci->uri->segment(1));
	$method = strtolower($ci->uri->segment(2));
	
	$uri = strtolower($ci->uri->uri_string());
	
	$uri_pengecualian = array (
		'pengguna/masuk',
		'pengguna/keluar',
	);
	
	$is_cek_session = false;
	
	if ($controller == '' or $controller == 'site') {
		if (session_pengguna('peng_id')) {
			redirect(site_url('/dasboard'));
		}
	}
	else if (array_search($uri, $uri_pengecualian) !== false) {
		$is_cek_session = false;
	}
	else {
		$is_cek_session = true;
	}
	
	if ($is_cek_session) {
		if ( ! session_pengguna('peng_id')) {
			redirect(site_url());
		}
	}
}
