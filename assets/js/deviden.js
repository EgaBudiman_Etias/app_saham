function dk_beli_clear()
{
	var $table = $('.dk-deviden-table');
	
	$table.find('.select2').val('').trigger('change');
	$table.find('input[type=text]').val('');
	
	$table.find('tbody tr').not(':first').remove();
}
function get_fee_pembelian(devi_akun_id){
    var $jumlah_lot = $('.dk-deviden-jumlah-lot');
    
    $.post (
        site_url+'/master/akun/ajax_akun'
        , { devi_akun_id: devi_akun_id }
        , function(response) {
           return response;
        }
    );
}   

function dk_hitung_bruto()
{   
    var $sahamx=$('.dk-deviden-saham');
	var $jumlah_lotx = $('.dk-deviden-jumlah-lot');
    var $jumlah_sharesx = $('.dk-deviden-jumlah-shares');
    var $harga_sharesx = $('.dk-deviden-deviden-shares');
    var $feex = $('.dk-deviden-pajak');
    var $brutox = $('.dk-deviden-bruto');
    var $feerupiahx = $('.dk-deviden-pajak-rp');
    var $nettox = $('.dk-deviden-netto');
    var netto=0 ;
    var bruto=0;
    var jumlah_shares=0;
    var feerupiah=0;
    var total=0;
    
    $.each ($sahamx, function(i, o) {
        var lot = parseInt($($jumlah_lotx[i]).val());
        var harga = parseInt($($harga_sharesx[i]).val());
        var feepersen = parseFloat($($feex[i]).val());
        
        if (isNaN(lot)) {
			lot = 0;
		}
		
		if (isNaN(harga)) {
			harga = 0;
		}

        if (isNaN(feepersen)) {
			feepersen = 0;
		}
        jumlah_shares=lot*100;
        $($jumlah_sharesx[i]).val(jumlah_shares);

        bruto=lot*100*harga;
        $($brutox[i]).val(bruto);

        feerupiah=bruto*feepersen/100;
        $($feerupiahx[i]).val(feerupiah);

        netto=bruto-feerupiah;
        $($nettox[i]).val(netto);

        total = total + netto;

    });
	$('.dk-deviden-total-harga').val(total);
	
}
$().ready(function() {
    $('.dk-deviden-table').on('keyup', '.dk-deviden-jumlah-lot', dk_hitung_bruto);
    $('.dk-deviden-table').on('keyup', '.dk-deviden-deviden-shares', dk_hitung_bruto);
    $('.dk-deviden-table').on('change', '.dk-deviden-saham', function() {
        var devi_akun_id=$('#devi_akun_id').val();
        var sham_id=$(this).val();
        var $row = $(this).closest('tr');
        var is_sama = false;
        var $this = $(this);
        var $jumlah_lot = $('.dk-deviden-jumlah-lot');
        if(sham_id != ''){
            if(devi_akun_id==""){
                alert("Pilih Akun Terlebih Dahulu");
                return;  
            };
            // $('.dk-deviden-saham').not(this).each(function() {
			// 	if (sham_id == $(this).val()) {
			// 		is_sama = true;
			// 	}
			// });
            
            // if (is_sama) {
			// 	alert('Saham sudah dipilih sebelumnya!');
				
			// 	$this.val('').trigger('change');
			// 	$row.find('input[type=text]').val('');
			// 	return;
			// }
            $.post (
                site_url+'/pengaturan/pajak/ajax_pajak_deviden'
                , { pmbl_akun_id: devi_akun_id,akun_id: devi_akun_id ,sham_id:sham_id }
                , function(response) {
                    $row.find('.dk-deviden-pajak').val(response.konf_nilai);
                    $row.find('.dk-deviden-jumlah-lot').val(response.saldosaham);
                    $row.find('.dk-deviden-deviden-shares').val(1);
                    dk_hitung_bruto();
                }
            );
        }
        
    });
    $('#devi_akun_id').on('change',function(){
        dk_beli_clear();
        var devi_akun_id=$('#devi_akun_id').val();
        var pajak_deviden=$('#pajak_deviden').val();
        var $table = $('.dk-deviden-table');
        $('#devi_akun_id2').val(devi_akun_id);
        var $row = $('.dk-deviden-table').closest('tr');
        $.post (
            site_url+'/master/akun/ajax_akun_saham'
            , { akun_id: devi_akun_id}
            , function(response) {
                $table.find('tbody tr').remove();
                // parse = $.parseJSON(response);
                
                $.each(response, function(name, value) {
                    console.log(response);
                    var sham_kode=value.sham_kode;
                    var sham_nama=value.sham_nama;
                    var saham=sham_kode+"-"+sham_nama;
                    var kshm_sham_id=value.kshm_sham_id;
                    var jumlah_lot=value.jumlah;
                    var markup="";
                    if(jumlah_lot!=0){
                        markup = '<tr>'+
                            '<td>'+
                                '<select class="form-control select2 dk-deviden-saham" name="saham[ddev_sham_id][]" data-placeholder="Pilih Saham" style="width:100%">'+
                                    '<option value="'+kshm_sham_id+'">'+saham+'</option>'+
                                    
                                '</select> '+
                            '</td>'+
                            '<td><input type="text" name="saham[ddev_jumlah_lot][]" class="control-number-no dk-deviden-jumlah-lot" style="width:100%" value="'+jumlah_lot+'" ></td>'+
                            '<td><input type="text" name="saham[ddev_jumlah_shares][]" class="control-number-no dk-deviden-jumlah-shares" style="width:100%" value="100" readonly></td>'+
                            '<td><input type="text" name="saham[ddev_deviden_shares][]" class="control-number-digit dk-deviden-deviden-shares" style="width:100%" value="0"></td>'+
                            '<td><input type="text" name="saham[ddev_bruto][]" class="control-number-digit dk-deviden-bruto" style="width:100%" value="0" readonly></td>'+
                            '<td>'+
                                '<input type="hidden" name="saham[ddev_pajak_deviden_persen][]"  class="control-number-digit dk-deviden-pajak" style="width:120px" value="'+pajak_deviden+'">  '+
                                '<input type="text" name="saham[ddev_pajak_deviden][]"  class="control-number-digit dk-deviden-pajak-rp" readonly style="width:120px" value="0">'+
                            '</td>'+
                            '<td><input type="text" class="control-number-digit dk-deviden-netto" name="saham[ddev_netto][]" style="width:100%" value="0" readonly></td>'+
                            
                            '<td align="center">'+
                                '<button class="btn btn-action btn-danger dk-deviden-btn-remove">'+
                                    '<i class="ti ti-trash"></i>'+
                                '</button>'+
                            '</td>'+
                        '</tr>';
                    };
                    // console.log(markup);
                    tableBody = $(".dk-deviden-table tbody");
                    tableBody.append(markup);
                    dk_hitung_bruto();
                    // var $row = $('.dk-jual-table > tbody').find('tr').first();
		
                    // $row.find('select.select2').select2('destroy');
                    
                    // var $new_row = $row.clone().appendTo('.dk-jual-table > tbody');
                    
                    // $new_row.find('.select2').val('');
                    // $new_row.find('input[type=text]').val('');
                    
                    $('.select2').select2({ allowClear: true });
                    $('input.control-number-no').number(true, 0);
                    $('input.control-number-digit').number(true, 2);
                });
            }
        );
    });

    $('.dk-deviden-btn-add').click(function(e) {
		e.preventDefault();
		
		var $row = $('.dk-deviden-table > tbody').find('tr').first();
		
		$row.find('select.select2').select2('destroy');
		
		var $new_row = $row.clone().appendTo('.dk-deviden-table > tbody');
		
		$new_row.find('.select2').val('');
		$new_row.find('input[type=text]').val('');
		
		$('.select2').select2({ allowClear: true });
		$('input.control-number-no').number(true, 0);
        $('input.control-number-digit').number(true, 2);
	});

    $('.dk-deviden-table').on('click', '.dk-deviden-btn-remove', function(e) {
		e.preventDefault();
		
		var jumlah_row = $('.dk-deviden-table > tbody').find('tr').length;
		
		var $row = $(this).closest('tr');
		
		if (jumlah_row == 1) {
			$row.find('.dk-deviden-saham').val('').trigger('change');
			$row.find('input[type=text]').val('');
		}
		else {
			$row.remove();
		}
		
		dk_hitung_bruto();
	});
});