/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 10.4.14-MariaDB : Database - app_saham
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `akun` */

DROP TABLE IF EXISTS `akun`;

CREATE TABLE `akun` (
  `akun_id` int(11) NOT NULL AUTO_INCREMENT,
  `akun_kode` varchar(10) DEFAULT NULL,
  `akun_seku_id` int(11) NOT NULL,
  `akun_kode_nasabah` varchar(100) NOT NULL,
  `akun_no_sid` varchar(100) NOT NULL,
  `akun_no_ksei_kpei` varchar(100) NOT NULL,
  `akun_bank_rdn` varchar(100) NOT NULL,
  `akun_bank_cab_rdn` varchar(100) DEFAULT NULL,
  `akun_no_rekening_rdn` varchar(100) NOT NULL,
  `akun_email` varchar(100) NOT NULL,
  `akun_created_by` int(11) NOT NULL,
  `akun_created_time` datetime DEFAULT NULL,
  `akun_updated_by` int(11) DEFAULT NULL,
  `akun_updated_time` datetime DEFAULT NULL,
  `akun_is_deleted` enum('0','1') DEFAULT '1' COMMENT '0= non aktif, 1= aktif',
  `akun_balance` double DEFAULT 0,
  `akun_setoran` double DEFAULT 0,
  `akun_penarikan` double DEFAULT 0,
  PRIMARY KEY (`akun_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

/*Data for the table `akun` */

insert  into `akun`(`akun_id`,`akun_kode`,`akun_seku_id`,`akun_kode_nasabah`,`akun_no_sid`,`akun_no_ksei_kpei`,`akun_bank_rdn`,`akun_bank_cab_rdn`,`akun_no_rekening_rdn`,`akun_email`,`akun_created_by`,`akun_created_time`,`akun_updated_by`,`akun_updated_time`,`akun_is_deleted`,`akun_balance`,`akun_setoran`,`akun_penarikan`) values 
(1,'AK0001',1,'121','3131','131','131','3131','1313','',131,'2022-01-03 20:55:19',131,'2022-01-04 05:58:12','1',0,0,0),
(2,'AK0002',5,'122233','808080','233221','PT. BANK MEGA','Bekasi','099999888','adiba@gmail.com',131,'2022-01-04 05:48:36',131,'2022-01-04 05:51:50','1',0,0,0),
(3,'AK0003',2,'232','1212','3232','PT Bank Nationalnobu Tbk','Tangerang','222332','aisyah@gmail.com',131,'2022-01-04 05:56:26',131,'2022-01-04 06:00:20','0',0,0,0),
(4,'AK0004',2,'22111','222111','23232','Bank Sekuritas','Jakarta','823782','',131,'2022-01-04 12:24:05',131,'2022-01-04 12:24:05','1',0,0,0),
(5,'AK0005',9,'234343434','121324','121331','Bank Bca','Jakarta','432313434','',131,'2022-01-04 12:38:07',131,'2022-01-04 12:38:07','1',0,0,0),
(6,'AK0006',16,'3239093','123232','343094','7672','AKUNCOBA','211223','',131,'2022-01-07 20:45:04',131,'2022-01-07 20:45:04','1',0,0,0),
(7,'AK0007',18,'123','123','123','Bank Sad',NULL,'122','',131,'2022-01-09 11:56:38',131,'2022-01-09 11:56:38','1',0,0,0);

/*Table structure for table `bank` */

DROP TABLE IF EXISTS `bank`;

CREATE TABLE `bank` (
  `bank_id` int(11) NOT NULL AUTO_INCREMENT,
  `bank_judul` varchar(100) NOT NULL,
  `bank_nama` varchar(100) NOT NULL,
  `bank_cabang` varchar(100) DEFAULT NULL,
  `bank_no_rekening` varchar(20) NOT NULL,
  `bank_nama_pemilik` varchar(100) NOT NULL,
  `bank_created_by` int(11) DEFAULT NULL,
  `bank_created_time` datetime DEFAULT NULL,
  `bank_updated_by` int(11) DEFAULT NULL,
  `bank_updated_time` datetime DEFAULT NULL,
  `bank_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`bank_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;

/*Data for the table `bank` */

insert  into `bank`(`bank_id`,`bank_judul`,`bank_nama`,`bank_cabang`,`bank_no_rekening`,`bank_nama_pemilik`,`bank_created_by`,`bank_created_time`,`bank_updated_by`,`bank_updated_time`,`bank_is_deleted`) values 
(1,'Terima Duit','BNI',NULL,'123','Budiman',131,'2022-01-03 20:54:37',131,'2022-01-03 20:54:37','1'),
(2,'Mandiri','Mandiri','Madiun','099988788','Budiman',131,'2022-01-04 04:09:30',131,'2022-01-04 04:09:57','1'),
(3,'Bank Mandiri','Mandiri','Jakarta','0998899','Uswatun Khasanah',131,'2022-01-04 04:13:52',131,'2022-01-04 04:13:52','1'),
(4,'Bank BNI','BNI','Solo','009900','Budiman',131,'2022-01-04 04:15:00',131,'2022-01-04 04:20:51','0'),
(5,'BSI','Bank Syariah Indonesia','Madiun','0900880','Uswatun Khasanah',131,'2022-01-04 04:17:21',131,'2022-01-04 04:17:21','1'),
(6,'Bank CIMB Niaga','CIMB Niaga','Jakarta','00998899','Uswatun Khasanah',131,'2022-01-04 04:18:37',131,'2022-01-04 04:18:37','1'),
(7,'BCA','BCA','Madiun','223454','Uswatun Khasanah',131,'2022-01-04 12:26:13',131,'2022-01-04 12:26:13','1'),
(8,'BNI','BNI','Madiun','12224343','Uswatun Khasanah',131,'2022-01-04 12:29:33',131,'2022-01-04 12:29:33','1'),
(9,'Bank Syariah','Bank Syariah Indonesia','Bandung','99888788','Uswatun Khasanah',131,'2022-01-05 09:17:39',131,'2022-01-05 09:17:39','1'),
(10,'Bank Muamalat','Bank Muamalat','Bandung','999889089','Uswatun Khasanah',131,'2022-01-05 09:19:17',131,'2022-01-05 09:19:17','1'),
(11,'Bank Muamalat','Bank Muamalat','Bandung','999889089','Uswatun Khasanah',131,'2022-01-05 09:19:17',131,'2022-01-05 09:19:17','1'),
(12,'Bank Muamalat','Bank Muamalat','Bandung','999889089','Uswatun Khasanah',131,'2022-01-05 09:19:17',131,'2022-01-05 09:19:17','1'),
(13,'Bank Tabungan Negara (BTN)','Bank Tabungan Negara (BTN)','Madiun','99887788','Uswatun Khasanah',131,'2022-01-05 09:20:31',131,'2022-01-05 09:20:31','1'),
(14,'Bank Tabungan Negara (BTN)','Bank Tabungan Negara (BTN)','Madiun','99887788','Uswatun Khasanah',131,'2022-01-05 09:20:31',131,'2022-01-05 09:20:31','1'),
(15,'Permata Bank','Permata Bank','Jakarta','9988890','Uswatun Khasanah',131,'2022-01-05 09:21:31',131,'2022-01-05 09:21:31','1'),
(16,'Bank Danamon','Bank Danamon','Malang','998909','Uswatun Khasanah',131,'2022-01-05 09:22:35',131,'2022-01-05 09:22:35','1'),
(17,'Bank Danamon','Bank Danamon','Malang','998909','Uswatun Khasanah',131,'2022-01-05 09:22:36',131,'2022-01-05 09:22:36','1');

/*Table structure for table `det_deviden` */

DROP TABLE IF EXISTS `det_deviden`;

CREATE TABLE `det_deviden` (
  `ddev_id` int(11) NOT NULL AUTO_INCREMENT,
  `ddev_devi_id` int(11) DEFAULT NULL,
  `ddev_no_ref` varchar(30) DEFAULT NULL,
  `ddev_sham_id` int(11) DEFAULT 0,
  `ddev_jumlah_lot` int(11) DEFAULT 0,
  `ddev_jumlah_shares` int(11) DEFAULT NULL,
  `ddev_deviden_shares` double DEFAULT 0,
  `ddev_netto` double DEFAULT 0,
  `ddev_pajak_deviden` double DEFAULT 0,
  `ddev_bruto` double DEFAULT 0,
  PRIMARY KEY (`ddev_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `det_deviden` */

/*Table structure for table `det_pembelian` */

DROP TABLE IF EXISTS `det_pembelian`;

CREATE TABLE `det_pembelian` (
  `dbli_id` int(11) NOT NULL AUTO_INCREMENT,
  `dbli_pmbl_id` int(11) DEFAULT NULL,
  `dbli_no_ref` varchar(30) DEFAULT NULL,
  `dbli_sham_id` int(11) DEFAULT NULL,
  `dbli_jumlah_lot` int(11) DEFAULT 0,
  `dbli_jumlah_shares` double DEFAULT 0,
  `dbli_harga_shares` double DEFAULT 0,
  `dbli_bruto` double DEFAULT 0,
  `dbli_fee_pembelian` double DEFAULT 0,
  `dbli_netto` double DEFAULT 0,
  PRIMARY KEY (`dbli_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `det_pembelian` */

insert  into `det_pembelian`(`dbli_id`,`dbli_pmbl_id`,`dbli_no_ref`,`dbli_sham_id`,`dbli_jumlah_lot`,`dbli_jumlah_shares`,`dbli_harga_shares`,`dbli_bruto`,`dbli_fee_pembelian`,`dbli_netto`) values 
(1,1,NULL,2,100,10000,100,1000000,1000,1001000),
(2,1,NULL,7,100,10000,100,1000000,1000,1001000),
(3,2,NULL,2,150,15000,150,2250000,2250,2252250),
(4,2,NULL,7,150,15000,120,1800000,1800,1801800);

/*Table structure for table `det_penjualan` */

DROP TABLE IF EXISTS `det_penjualan`;

CREATE TABLE `det_penjualan` (
  `dpjl_id` int(11) NOT NULL AUTO_INCREMENT,
  `dpjl_pnjl_id` int(11) DEFAULT NULL,
  `dpjl_no_ref` varchar(30) DEFAULT NULL,
  `dpjl_sham_id` int(11) DEFAULT NULL,
  `dpjl_jumlah_lot` int(11) DEFAULT 0,
  `dpjl_jumlah_shares` int(11) DEFAULT 0,
  `dpjl_harga_shares` double DEFAULT 0,
  `dpjl_bruto` double DEFAULT 0,
  `dpjl_fee_penjualan` double DEFAULT 0,
  `dpjl_pajak_penjualan` double DEFAULT 0,
  `dpjl_netto` double DEFAULT 0,
  `dpjl_harga_rata_rata` double DEFAULT 0,
  PRIMARY KEY (`dpjl_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `det_penjualan` */

insert  into `det_penjualan`(`dpjl_id`,`dpjl_pnjl_id`,`dpjl_no_ref`,`dpjl_sham_id`,`dpjl_jumlah_lot`,`dpjl_jumlah_shares`,`dpjl_harga_shares`,`dpjl_bruto`,`dpjl_fee_penjualan`,`dpjl_pajak_penjualan`,`dpjl_netto`,`dpjl_harga_rata_rata`) values 
(1,1,NULL,2,250,25000,180,4500000,4500,31500,4464000,0),
(2,1,NULL,7,250,25000,150,3750000,3750,26250,3720000,0);

/*Table structure for table `deviden` */

DROP TABLE IF EXISTS `deviden`;

CREATE TABLE `deviden` (
  `devi_id` int(11) NOT NULL AUTO_INCREMENT,
  `devi_kode` varchar(20) DEFAULT NULL,
  `devi_akun_id` int(11) DEFAULT NULL,
  `devi_tgl_bagi` date DEFAULT NULL,
  `devi_grand_tot` double DEFAULT 0,
  `devi_created_by` int(11) DEFAULT NULL,
  `devi_created_time` datetime DEFAULT NULL,
  `devi_updated_by` int(11) DEFAULT NULL,
  `devi_updated_time` datetime DEFAULT NULL,
  `devi_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`devi_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `deviden` */

/*Table structure for table `keuangan` */

DROP TABLE IF EXISTS `keuangan`;

CREATE TABLE `keuangan` (
  `uang_id` int(11) NOT NULL AUTO_INCREMENT,
  `uang_akun_id` int(11) NOT NULL,
  `uang_tgl` date NOT NULL,
  `uang_jns_transaksi` enum('setoran','penarikan','deviden','pembelian','penjualan','saldoawal') DEFAULT NULL,
  `uang_transaksi_id` int(11) DEFAULT NULL,
  `uang_nominal` double NOT NULL DEFAULT 0,
  `uang_created_by` int(11) NOT NULL,
  `uang_created_time` datetime NOT NULL,
  PRIMARY KEY (`uang_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `keuangan` */

insert  into `keuangan`(`uang_id`,`uang_akun_id`,`uang_tgl`,`uang_jns_transaksi`,`uang_transaksi_id`,`uang_nominal`,`uang_created_by`,`uang_created_time`) values 
(1,7,'2022-01-01','saldoawal',0,100000000000,131,'2022-01-09 11:56:38'),
(2,7,'2022-01-09','pembelian',1,-2002000,131,'2022-01-09 12:19:38'),
(3,7,'2022-01-09','pembelian',2,-4054050,131,'2022-01-09 12:28:56'),
(4,7,'2022-01-09','penjualan',1,8184000,131,'2022-01-09 13:14:53');

/*Table structure for table `keuangan_saham` */

DROP TABLE IF EXISTS `keuangan_saham`;

CREATE TABLE `keuangan_saham` (
  `kshm_id` int(11) NOT NULL AUTO_INCREMENT,
  `kshm_akun_id` int(11) DEFAULT NULL,
  `kshm_sham_id` int(11) DEFAULT NULL,
  `kshm_tgl_transaksi` date DEFAULT NULL,
  `kshm_jns_transaksi` enum('penjualan','pembelian','saldoawal') DEFAULT NULL,
  `kshm_transaksi_id` int(11) DEFAULT NULL,
  `kshm_jumlah` int(11) DEFAULT 0,
  `kshm_harga_rata_rata` double DEFAULT 0,
  `kshm_harga_rata_rata_2` double DEFAULT 0,
  `kshm_harga_netto` double DEFAULT 0,
  `kshm_created_by` int(11) DEFAULT NULL,
  `kshm_created_time` datetime DEFAULT NULL,
  PRIMARY KEY (`kshm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

/*Data for the table `keuangan_saham` */

insert  into `keuangan_saham`(`kshm_id`,`kshm_akun_id`,`kshm_sham_id`,`kshm_tgl_transaksi`,`kshm_jns_transaksi`,`kshm_transaksi_id`,`kshm_jumlah`,`kshm_harga_rata_rata`,`kshm_harga_rata_rata_2`,`kshm_harga_netto`,`kshm_created_by`,`kshm_created_time`) values 
(3,7,7,'2021-12-31','saldoawal',NULL,1000,100,100,10000000,131,'2022-01-09 12:15:02'),
(4,7,2,'2021-12-31','saldoawal',NULL,1000,100,100,10000000,131,'2022-01-09 12:15:02'),
(5,7,2,'2022-01-09','pembelian',1,100,100,0,1001000,131,'2022-01-09 12:19:38'),
(6,7,7,'2022-01-09','pembelian',1,100,100,0,1001000,131,'2022-01-09 12:19:38'),
(7,7,2,'2022-01-09','pembelian',2,150,150,0,2252250,131,'2022-01-09 12:28:56'),
(8,7,7,'2022-01-09','pembelian',2,150,120,0,1801800,131,'2022-01-09 12:28:56'),
(9,7,2,'2022-01-09','penjualan',1,-250,180,0,4464000,131,'2022-01-09 13:14:53'),
(10,7,7,'2022-01-09','penjualan',1,-250,150,0,3720000,131,'2022-01-09 13:14:53');

/*Table structure for table `konfigurasi` */

DROP TABLE IF EXISTS `konfigurasi`;

CREATE TABLE `konfigurasi` (
  `konf_id` int(11) NOT NULL AUTO_INCREMENT,
  `konf_kode` varchar(50) NOT NULL,
  `konf_nama` varchar(100) NOT NULL,
  `konf_tipe_data` varchar(100) NOT NULL,
  `konf_nilai` double NOT NULL,
  `konf_is_deleted` enum('0','1') DEFAULT '1',
  `konf_created_time` datetime DEFAULT NULL,
  `konf_created_by` int(11) DEFAULT NULL,
  `konf_updated_time` datetime DEFAULT NULL,
  `konf_updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`konf_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `konfigurasi` */

insert  into `konfigurasi`(`konf_id`,`konf_kode`,`konf_nama`,`konf_tipe_data`,`konf_nilai`,`konf_is_deleted`,`konf_created_time`,`konf_created_by`,`konf_updated_time`,`konf_updated_by`) values 
(15,'pajak_penjualan','Pajak Penjualan','string',0.7,'1','2021-12-06 10:20:01',NULL,'2022-01-07 15:04:20',131),
(16,'pajak_deviden','Pajak Deviden','string',1.3,'1','2021-12-06 10:20:01',NULL,'2022-01-07 15:04:20',131);

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id_induk` int(11) DEFAULT NULL,
  `menu_kode` varchar(100) NOT NULL,
  `menu_nomor` varchar(20) NOT NULL DEFAULT '0',
  `menu_ikon` varchar(50) DEFAULT NULL,
  `menu_teks` varchar(200) NOT NULL,
  `menu_uri` varchar(200) NOT NULL,
  `menu_is_deleted` enum('0','1') NOT NULL DEFAULT '1',
  `menu_created_time` datetime DEFAULT NULL,
  `menu_created_id` int(11) DEFAULT NULL,
  `menu_updated_time` datetime DEFAULT NULL,
  `menu_updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `menu` */

insert  into `menu`(`menu_id`,`menu_id_induk`,`menu_kode`,`menu_nomor`,`menu_ikon`,`menu_teks`,`menu_uri`,`menu_is_deleted`,`menu_created_time`,`menu_created_id`,`menu_updated_time`,`menu_updated_by`) values 
(42,NULL,'dasbor','010.000','dashboard','Dashboard','/dasboard','1','2021-12-06 08:54:55',131,'0000-00-00 00:00:00',NULL),
(43,NULL,'transaksi','030.000','face-smile','Transaksi','#','1','2021-12-06 08:54:52',131,'0000-00-00 00:00:00',NULL),
(44,43,'setoran','030.010',NULL,'Setoran','/transaksi/setoran','1','2021-12-06 08:54:52',131,'0000-00-00 00:00:00',NULL),
(46,NULL,'master','050.000','harddrive','Data Master','#','1','2021-12-06 08:54:52',131,NULL,NULL),
(47,NULL,'laporan','040.000','money','Laporan','#','1','2021-12-08 17:32:25',131,NULL,NULL),
(49,47,'aruskas','040.010',NULL,'Arus Kas','/laporan/aruskas','1','2021-12-08 17:32:21',131,NULL,NULL),
(50,47,'arussaham','040.020',NULL,'Arus Saham','/laporan/arussaham','1','2021-12-08 17:32:19',131,NULL,NULL),
(52,43,'penarikan','030.050',NULL,'Penarikan','/transaksi/penarikan','1','2021-12-08 17:32:17',131,NULL,NULL),
(53,43,'pembelian','030.020',NULL,'Pembelian Saham','/transaksi/pembelian','1','2021-12-08 17:32:15',131,NULL,NULL),
(54,43,'penjualan','030.030',NULL,'Penjualan Saham','/transaksi/penjualan','1','2021-12-08 17:32:13',131,NULL,NULL),
(55,43,'deviden','030.040',NULL,'Deviden','/transaksi/deviden','1','2021-12-08 17:32:11',131,NULL,NULL),
(56,46,'bank','050.010',NULL,'Bank','/master/bank','1','2021-12-08 17:32:10',131,NULL,NULL),
(57,46,'akun','050.040',NULL,'Akun','/master/akun','1','2021-12-08 17:32:08',131,NULL,NULL),
(58,46,'saham','050.030',NULL,'Saham','/master/saham','1','2021-12-08 17:32:07',131,NULL,NULL),
(59,46,'sekuritas','050.020',NULL,'Sekuritas','/master/sekuritas','1','2021-12-08 17:32:04',131,NULL,NULL),
(60,NULL,'pengaturan','060.000','settings','Pengaturan','#','1','2021-12-08 17:32:02',131,NULL,NULL),
(61,60,'saldoawalrdn','060.030',NULL,'Saldo Awal RDN','/pengaturan/saldoawalrdn','0',NULL,NULL,NULL,NULL),
(62,60,'pajak','060.020',NULL,'Pajak','/pengaturan/pajak','1',NULL,NULL,NULL,NULL),
(63,46,'refpilihan','050.050',NULL,'Ref Pilihan','/master/refpilihan','0',NULL,NULL,NULL,NULL),
(64,60,'saldoawalsaham','060.010',NULL,'Saldo Awal Saham','/pengaturan/saldoawalsaham','1',NULL,NULL,NULL,NULL),
(65,NULL,'','0',NULL,'','','1',NULL,NULL,NULL,NULL);

/*Table structure for table `pembelian` */

DROP TABLE IF EXISTS `pembelian`;

CREATE TABLE `pembelian` (
  `pmbl_id` int(11) NOT NULL AUTO_INCREMENT,
  `pmbl_kode` varchar(20) DEFAULT NULL,
  `pmbl_akun_id` int(11) DEFAULT NULL,
  `pmbl_tgl_beli` date DEFAULT NULL,
  `pmbl_grand_tot` double DEFAULT 0,
  `pmbl_created_by` int(11) DEFAULT NULL,
  `pmbl_created_time` datetime DEFAULT NULL,
  `pmbl_updated_by` int(11) DEFAULT NULL,
  `pmbl_updated_time` datetime DEFAULT NULL,
  `pmbl_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`pmbl_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `pembelian` */

insert  into `pembelian`(`pmbl_id`,`pmbl_kode`,`pmbl_akun_id`,`pmbl_tgl_beli`,`pmbl_grand_tot`,`pmbl_created_by`,`pmbl_created_time`,`pmbl_updated_by`,`pmbl_updated_time`,`pmbl_is_deleted`) values 
(1,'B2022010001',7,'2022-01-01',2002000,131,'2022-01-09 12:19:38',131,'2022-01-09 12:19:38','1'),
(2,'B2022010002',7,'2022-01-02',4054050,131,'2022-01-09 12:28:56',131,'2022-01-09 12:28:56','1');

/*Table structure for table `penarikan` */

DROP TABLE IF EXISTS `penarikan`;

CREATE TABLE `penarikan` (
  `trik_id` int(11) NOT NULL AUTO_INCREMENT,
  `trik_kode` varchar(20) DEFAULT NULL,
  `trik_tgl` date DEFAULT NULL,
  `trik_akun_id` int(11) DEFAULT NULL,
  `trik_bank_id` int(11) DEFAULT NULL,
  `trik_no_ref` varchar(20) DEFAULT NULL,
  `trik_nominal` double DEFAULT 0,
  `trik_created_by` int(11) DEFAULT NULL,
  `trik_created_time` datetime DEFAULT NULL,
  `trik_updated_by` int(11) DEFAULT NULL,
  `trik_updated_time` datetime DEFAULT NULL,
  `trik_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`trik_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `penarikan` */

/*Table structure for table `pengguna` */

DROP TABLE IF EXISTS `pengguna`;

CREATE TABLE `pengguna` (
  `peng_id` int(11) NOT NULL AUTO_INCREMENT,
  `peng_nama` varchar(100) NOT NULL,
  `peng_nama_lengkap` varchar(100) DEFAULT NULL,
  `peng_kata_sandi` varchar(50) NOT NULL,
  `peng_cookie` varchar(50) DEFAULT NULL,
  `peng_is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `peng_created_time` datetime NOT NULL,
  `peng_created_by` int(11) NOT NULL,
  `peng_updated_time` datetime NOT NULL,
  `peng_updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`peng_id`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `pengguna` */

insert  into `pengguna`(`peng_id`,`peng_nama`,`peng_nama_lengkap`,`peng_kata_sandi`,`peng_cookie`,`peng_is_deleted`,`peng_created_time`,`peng_created_by`,`peng_updated_time`,`peng_updated_by`) values 
(131,'dinata','Ega Budiman','2986032a8c843640542c6dad2e30b8cf','a8f9d6f8010b269d4e46feb96ccbe88d',1,'0000-00-00 00:00:00',0,'2022-01-06 15:48:40',131);

/*Table structure for table `pengguna_grup_menu` */

DROP TABLE IF EXISTS `pengguna_grup_menu`;

CREATE TABLE `pengguna_grup_menu` (
  `grup_id` int(11) NOT NULL AUTO_INCREMENT,
  `grup_peng_id` int(11) NOT NULL,
  `grup_menu_id` int(11) NOT NULL,
  `grup_is_deleted` int(4) NOT NULL DEFAULT 1,
  `grup_created_time` datetime DEFAULT NULL,
  `grup_created_by` int(11) DEFAULT NULL,
  `grup_updated_time` datetime DEFAULT NULL,
  `grup_updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`grup_id`)
) ENGINE=InnoDB AUTO_INCREMENT=290 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `pengguna_grup_menu` */

insert  into `pengguna_grup_menu`(`grup_id`,`grup_peng_id`,`grup_menu_id`,`grup_is_deleted`,`grup_created_time`,`grup_created_by`,`grup_updated_time`,`grup_updated_by`) values 
(268,131,42,1,'2021-12-08 17:44:57',131,NULL,NULL),
(269,131,43,1,'2021-12-08 17:45:01',131,NULL,NULL),
(270,131,44,1,'2021-12-08 17:45:05',131,NULL,NULL),
(271,131,46,1,'2021-12-08 17:45:07',131,NULL,NULL),
(272,131,47,1,'2021-12-08 17:45:08',131,NULL,NULL),
(273,131,48,1,'2021-12-08 17:45:10',131,NULL,NULL),
(274,131,49,1,'2021-12-08 17:45:13',131,NULL,NULL),
(275,131,50,1,'2021-12-08 17:45:14',131,NULL,NULL),
(276,131,51,1,'2021-12-08 17:45:16',131,NULL,NULL),
(277,131,52,1,'2021-12-08 17:45:18',131,NULL,NULL),
(278,131,53,1,'2021-12-08 17:45:20',131,NULL,NULL),
(279,131,54,1,'2021-12-08 17:45:23',131,NULL,NULL),
(280,131,55,1,'2021-12-08 17:45:25',131,NULL,NULL),
(281,131,56,1,'2021-12-08 17:45:27',131,NULL,NULL),
(282,131,57,1,'2021-12-08 17:45:29',131,NULL,NULL),
(283,131,58,1,'2021-12-08 17:45:30',131,NULL,NULL),
(284,131,59,1,'2021-12-08 17:45:33',131,NULL,NULL),
(285,131,60,1,'2021-12-08 17:45:35',131,NULL,NULL),
(286,131,61,1,'2021-12-08 17:45:36',131,NULL,NULL),
(287,131,62,1,'2021-12-08 17:45:38',131,NULL,NULL),
(288,131,63,1,'2021-12-09 04:58:07',131,NULL,NULL),
(289,131,64,1,'2021-12-09 06:15:18',131,NULL,NULL);

/*Table structure for table `penjualan` */

DROP TABLE IF EXISTS `penjualan`;

CREATE TABLE `penjualan` (
  `pnjl_id` int(11) NOT NULL AUTO_INCREMENT,
  `pnjl_kode` varchar(20) DEFAULT NULL,
  `pnjl_akun_id` int(11) DEFAULT NULL,
  `pnjl_tgl_jual` date DEFAULT NULL,
  `pnjl_grand_tot` double DEFAULT 0,
  `pnjl_created_by` int(11) DEFAULT NULL,
  `pnjl_created_time` datetime DEFAULT NULL,
  `pnjl_updated_by` int(11) DEFAULT NULL,
  `pnjl_updated_time` datetime DEFAULT NULL,
  `pnjl_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`pnjl_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `penjualan` */

insert  into `penjualan`(`pnjl_id`,`pnjl_kode`,`pnjl_akun_id`,`pnjl_tgl_jual`,`pnjl_grand_tot`,`pnjl_created_by`,`pnjl_created_time`,`pnjl_updated_by`,`pnjl_updated_time`,`pnjl_is_deleted`) values 
(1,'J2022010001',7,'2022-01-02',8184000,131,'2022-01-09 13:14:53',131,'2022-01-09 13:14:53','1');

/*Table structure for table `ref_pilihan` */

DROP TABLE IF EXISTS `ref_pilihan`;

CREATE TABLE `ref_pilihan` (
  `plhn_id` int(11) NOT NULL AUTO_INCREMENT,
  `plhn_kategori` varchar(100) NOT NULL,
  `plhn_nama` varchar(100) NOT NULL,
  `plhn_urutan` int(11) NOT NULL,
  `plhn_is_deleted` enum('0','1') NOT NULL DEFAULT '1',
  `plhn_created_time` datetime NOT NULL,
  `plhn_created_by` int(11) NOT NULL,
  `plhn_updated_time` datetime NOT NULL,
  `plhn_updated_by` int(11) NOT NULL,
  PRIMARY KEY (`plhn_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `ref_pilihan` */

insert  into `ref_pilihan`(`plhn_id`,`plhn_kategori`,`plhn_nama`,`plhn_urutan`,`plhn_is_deleted`,`plhn_created_time`,`plhn_created_by`,`plhn_updated_time`,`plhn_updated_by`) values 
(1,'kategori-saham','Sangat Baik',1,'1','2021-12-08 19:11:23',131,'0000-00-00 00:00:00',0),
(2,'kategori_saham','Baik',2,'1','2021-12-08 19:11:27',131,'0000-00-00 00:00:00',0),
(3,'kategori-saham','Cukup',3,'1','2021-12-08 19:11:29',131,'0000-00-00 00:00:00',0),
(4,'kategori-saham','Buruk',4,'1','2021-12-08 19:11:31',131,'0000-00-00 00:00:00',0),
(5,'sektor-saham','Telekomunikasi',1,'1','2021-12-08 19:11:33',131,'0000-00-00 00:00:00',0),
(6,'sektor-saham','Minerba',2,'1','2021-12-08 19:11:56',131,'0000-00-00 00:00:00',0),
(7,'sektor-saham','Bank',3,'0','2021-12-08 23:30:34',131,'2021-12-08 23:31:05',131);

/*Table structure for table `saham` */

DROP TABLE IF EXISTS `saham`;

CREATE TABLE `saham` (
  `sham_id` int(11) NOT NULL AUTO_INCREMENT,
  `sham_nama` varchar(100) NOT NULL,
  `sham_kode` varchar(10) NOT NULL,
  `sham_sektor` varchar(100) NOT NULL,
  `sham_seku_id` int(11) DEFAULT NULL,
  `sham_kinerja_kategori` varchar(100) NOT NULL,
  `sham_created_by` int(11) DEFAULT NULL,
  `sham_created_time` datetime DEFAULT NULL,
  `sham_updated_by` int(11) DEFAULT NULL,
  `sham_updated_time` datetime DEFAULT NULL,
  `sham_is_deleted` enum('0','1') DEFAULT '1',
  `sham_total_lot` double DEFAULT 0,
  `sham_harga_rata_rata` double DEFAULT 0,
  PRIMARY KEY (`sham_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

/*Data for the table `saham` */

insert  into `saham`(`sham_id`,`sham_nama`,`sham_kode`,`sham_sektor`,`sham_seku_id`,`sham_kinerja_kategori`,`sham_created_by`,`sham_created_time`,`sham_updated_by`,`sham_updated_time`,`sham_is_deleted`,`sham_total_lot`,`sham_harga_rata_rata`) values 
(1,'Telkom','TLKM','5',1,'1',1,'2021-12-06 08:35:55',131,'2021-12-08 13:47:13','1',0,0),
(2,'Metro Care','CARE','6',1,'1',1,'2021-12-06 08:47:31',131,'2021-12-08 13:47:04','1',0,0),
(3,'Sidomuncul','SIDO','6',2,'1',131,'2021-12-08 13:40:23',131,'2021-12-08 13:40:23','1',0,0),
(4,'Mahaka Media Center','121212','5',NULL,'4',131,'2022-01-04 04:52:12',131,'2022-01-04 04:54:53','0',0,0),
(5,'Astra Agro Lestari Tbk.','65656','6',NULL,'3',131,'2022-01-05 10:01:38',131,'2022-01-05 10:01:38','1',0,0),
(6,'Tri Banyan Tirta Tbk','4433442','6',NULL,'3',131,'2022-01-05 10:05:57',131,'2022-01-05 10:05:57','1',0,0),
(7,'Asuransi Multi Artha Guna Tbk','ASUM','5',NULL,'4',131,'2022-01-05 10:06:36',131,'2022-01-05 10:06:36','1',0,0),
(8,'Bank Amar Indonesia Tbk','432423','5',NULL,'1',131,'2022-01-05 10:07:35',131,'2022-01-05 10:07:35','1',0,0),
(9,'Bank Amar Indonesia Tbk','AMAR','5',NULL,'4',131,'2022-01-05 10:10:31',131,'2022-01-05 10:10:31','1',0,0),
(10,'Tri Banyan Tirta Tbk','BYN','5',NULL,'1',131,'2022-01-05 10:33:32',131,'2022-01-05 10:33:32','1',0,0),
(11,'Tri Buyana Tbk','BYN','5',NULL,'3',131,'2022-01-05 10:35:56',131,'2022-01-05 10:35:56','1',0,0),
(12,'Tri Banyan Tirta Tbk','BYN','5',NULL,'1',131,'2022-01-05 10:48:16',131,'2022-01-05 10:48:16','1',0,0),
(13,'Tri Banyan Tirta Tbk','BYN','5',NULL,'1',131,'2022-01-05 10:48:16',131,'2022-01-05 10:48:16','1',0,0);

/*Table structure for table `saldo_awal` */

DROP TABLE IF EXISTS `saldo_awal`;

CREATE TABLE `saldo_awal` (
  `swal_id` int(11) NOT NULL AUTO_INCREMENT,
  `swal_akun_id` int(11) DEFAULT NULL,
  `swal_tgl_saldo_awal` date DEFAULT NULL,
  `swal_jumlah_saldo` double DEFAULT 0,
  `swal_created_time` datetime DEFAULT NULL,
  `swal_created_by` int(11) DEFAULT NULL,
  `swal_updated_by` int(11) DEFAULT NULL,
  `swal_updated_time` datetime DEFAULT NULL,
  `swal_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`swal_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `saldo_awal` */

insert  into `saldo_awal`(`swal_id`,`swal_akun_id`,`swal_tgl_saldo_awal`,`swal_jumlah_saldo`,`swal_created_time`,`swal_created_by`,`swal_updated_by`,`swal_updated_time`,`swal_is_deleted`) values 
(1,7,'2022-01-01',100000000000,'2022-01-09 11:56:38',131,131,'2022-01-09 11:56:38','1');

/*Table structure for table `saldo_awal_saham` */

DROP TABLE IF EXISTS `saldo_awal_saham`;

CREATE TABLE `saldo_awal_saham` (
  `shal_id` int(11) NOT NULL AUTO_INCREMENT,
  `shal_akun_id` int(11) DEFAULT NULL,
  `shal_sham_id` int(11) DEFAULT NULL,
  `shal_tgl_saldo_awal` date DEFAULT NULL,
  `shal_jumlah_lot` double DEFAULT 0,
  `shal_jumlah_shares` double DEFAULT 0,
  `shal_rata_rata_harga` double DEFAULT 0,
  `shal_total` double DEFAULT 0,
  `shal_created_time` datetime DEFAULT NULL,
  `shal_created_by` int(11) DEFAULT NULL,
  `shal_updated_by` int(11) DEFAULT NULL,
  `shal_updated_time` datetime DEFAULT NULL,
  `shal_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`shal_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

/*Data for the table `saldo_awal_saham` */

insert  into `saldo_awal_saham`(`shal_id`,`shal_akun_id`,`shal_sham_id`,`shal_tgl_saldo_awal`,`shal_jumlah_lot`,`shal_jumlah_shares`,`shal_rata_rata_harga`,`shal_total`,`shal_created_time`,`shal_created_by`,`shal_updated_by`,`shal_updated_time`,`shal_is_deleted`) values 
(7,7,7,'2021-12-31',1000,100000,100,10000000,'2022-01-09 12:15:02',131,NULL,NULL,'1'),
(8,7,2,'2021-12-31',1000,100000,100,10000000,'2022-01-09 12:15:02',131,NULL,NULL,'1');

/*Table structure for table `sekuritas` */

DROP TABLE IF EXISTS `sekuritas`;

CREATE TABLE `sekuritas` (
  `seku_id` int(11) NOT NULL AUTO_INCREMENT,
  `seku_nama` varchar(100) NOT NULL,
  `seku_no_telp` varchar(20) NOT NULL,
  `seku_fax` varchar(20) NOT NULL,
  `seku_email` varchar(30) NOT NULL,
  `seku_alamat` text NOT NULL,
  `seku_broker_fee_pembelian` double NOT NULL DEFAULT 0,
  `seku_broker_fee_penjualan` double NOT NULL DEFAULT 0,
  `seku_created_by` int(11) DEFAULT NULL,
  `seku_created_time` datetime DEFAULT NULL,
  `seku_updated_by` int(11) DEFAULT NULL,
  `seku_updated_time` datetime DEFAULT NULL,
  `seku_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`seku_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4;

/*Data for the table `sekuritas` */

insert  into `sekuritas`(`seku_id`,`seku_nama`,`seku_no_telp`,`seku_fax`,`seku_email`,`seku_alamat`,`seku_broker_fee_pembelian`,`seku_broker_fee_penjualan`,`seku_created_by`,`seku_created_time`,`seku_updated_by`,`seku_updated_time`,`seku_is_deleted`) values 
(1,'Ajaib','-','-','-','-',0.8,0.3,1,'2021-12-06 07:56:08',131,'2021-12-23 19:00:04','1'),
(2,'BNI Sekuritas','-','-','-','-',0.1,0.7,1,'2021-12-06 07:56:41',131,'2021-12-23 19:07:27','1'),
(3,'Mandiri Sekuritas','-','-','-','-',0,0.9,1,'2021-12-06 08:49:18',131,'2021-12-23 20:19:36','1'),
(5,'Binomo','123456789123','123456789123','asd','asd',3,2,131,'2021-12-08 22:50:20',131,'2022-01-04 16:15:41','0'),
(8,'PT Bahano Sekuritas','087789877','89911','bahana@gmail.co','Jl pisang no 58 riau',1.2,1,131,'2022-01-04 04:25:32',131,'2022-01-04 04:33:21','0'),
(9,'Ipot','0876676788','332','uswa@gmail.com','jl. salah no 34 jakarta',2,2,131,'2022-01-04 12:31:53',131,'2022-01-05 10:55:11','0'),
(10,'PT Ajaib Sekuritas Asia','0889979','223342','ajaib@gmail.com','Jl. Mangga no 12 Kota Madiun',1.2,1,131,'2022-01-05 09:28:04',131,'2022-01-05 09:28:04','1'),
(11,'PT Bahana Sekuritas','0898889898','3221','bahana@gmail.com','jl. kelapa no 12  Kota solo',1,0.9,131,'2022-01-05 09:31:13',131,'2022-01-05 09:31:13','1'),
(12,'PT CGS-CIMB Sekuritas Indonesia.','085667889','5565','cimb@gmail.com','jl.melon no 13 kota Madiun',0.9,0.5,131,'2022-01-05 09:36:02',131,'2022-01-05 09:36:02','1'),
(13,'PT CGS-CIMB Sekuritas Indonesia.','085667889','5565','cimb@gmail.com','jl.melon no 13 kota Madiun',0.9,0.5,131,'2022-01-05 09:36:02',131,'2022-01-05 09:36:02','1'),
(14,'PT Danareksa Sekuritas','0987787878','09089','danareksa@gmail.com','jl pisang no 11 kota madiun',1.2,0.9,131,'2022-01-05 09:38:11',131,'2022-01-06 10:14:47','0'),
(15,'PT Dhanawibawa Sekuritas Indonesia.','087788989788','9989','Dhanawibawa@gmail.com','Jl mangga no 12  kota pekanbaru',1,0.9,131,'2022-01-05 09:40:51',131,'2022-01-05 09:40:51','1'),
(16,'PT Erdikha Elit Sekuritas.','0856787889','09989','Erdikha@gmail.com','jl. peangeran timur no 12  kota jakarta',0.8,0.6,131,'2022-01-05 09:45:18',131,'2022-01-05 09:45:18','1'),
(17,'PT Mandiri Sekuritas.','0998099098','989998','mandiri@gmail.com','Jl. Pangeran timur  no 12 Kota Jakarta',2,1.9,131,'2022-01-05 09:57:20',131,'2022-01-05 09:57:20','1'),
(18,'Tes Budiman','','','','',0.1,0.1,131,'2022-01-09 11:55:20',131,'2022-01-09 11:55:20','1');

/*Table structure for table `setoran` */

DROP TABLE IF EXISTS `setoran`;

CREATE TABLE `setoran` (
  `stor_id` int(11) NOT NULL AUTO_INCREMENT,
  `stor_kode` varchar(20) DEFAULT NULL,
  `stor_tgl` date DEFAULT NULL,
  `stor_akun_id` int(11) DEFAULT NULL,
  `stor_bank_id` int(11) DEFAULT NULL,
  `stor_no_ref` varchar(20) DEFAULT NULL,
  `stor_nominal` double DEFAULT 0,
  `stor_created_by` int(11) DEFAULT NULL,
  `stor_created_time` datetime DEFAULT NULL,
  `stor_updated_by` int(11) DEFAULT NULL,
  `stor_updated_time` datetime DEFAULT NULL,
  `stor_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`stor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `setoran` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
