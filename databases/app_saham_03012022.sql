/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 10.4.14-MariaDB : Database - app_saham
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `akun` */

DROP TABLE IF EXISTS `akun`;

CREATE TABLE `akun` (
  `akun_id` int(11) NOT NULL AUTO_INCREMENT,
  `akun_kode` varchar(10) DEFAULT NULL,
  `akun_seku_id` int(11) NOT NULL,
  `akun_kode_nasabah` varchar(100) NOT NULL,
  `akun_no_sid` varchar(100) NOT NULL,
  `akun_no_ksei_kpei` varchar(100) NOT NULL,
  `akun_bank_rdn` varchar(100) NOT NULL,
  `akun_bank_cab_rdn` varchar(100) DEFAULT NULL,
  `akun_no_rekening_rdn` varchar(100) NOT NULL,
  `akun_email` varchar(100) NOT NULL,
  `akun_created_by` int(11) NOT NULL,
  `akun_created_time` datetime DEFAULT NULL,
  `akun_updated_by` int(11) DEFAULT NULL,
  `akun_updated_time` datetime DEFAULT NULL,
  `akun_is_deleted` enum('0','1') DEFAULT '1' COMMENT '0= non aktif, 1= aktif',
  `akun_balance` double DEFAULT 0,
  `akun_setoran` double DEFAULT 0,
  `akun_penarikan` double DEFAULT 0,
  PRIMARY KEY (`akun_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `akun` */

/*Table structure for table `bank` */

DROP TABLE IF EXISTS `bank`;

CREATE TABLE `bank` (
  `bank_id` int(11) NOT NULL AUTO_INCREMENT,
  `bank_judul` varchar(100) NOT NULL,
  `bank_nama` varchar(100) NOT NULL,
  `bank_cabang` varchar(100) DEFAULT NULL,
  `bank_no_rekening` varchar(20) NOT NULL,
  `bank_nama_pemilik` varchar(100) NOT NULL,
  `bank_created_by` int(11) DEFAULT NULL,
  `bank_created_time` datetime DEFAULT NULL,
  `bank_updated_by` int(11) DEFAULT NULL,
  `bank_updated_time` datetime DEFAULT NULL,
  `bank_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`bank_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `bank` */

/*Table structure for table `det_deviden` */

DROP TABLE IF EXISTS `det_deviden`;

CREATE TABLE `det_deviden` (
  `ddev_id` int(11) NOT NULL AUTO_INCREMENT,
  `ddev_devi_id` int(11) DEFAULT NULL,
  `ddev_no_ref` varchar(30) DEFAULT NULL,
  `ddev_sham_id` int(11) DEFAULT 0,
  `ddev_jumlah_lot` int(11) DEFAULT 0,
  `ddev_jumlah_shares` int(11) DEFAULT NULL,
  `ddev_deviden_shares` double DEFAULT 0,
  `ddev_netto` double DEFAULT 0,
  `ddev_pajak_deviden` double DEFAULT 0,
  `ddev_bruto` double DEFAULT 0,
  PRIMARY KEY (`ddev_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `det_deviden` */

/*Table structure for table `det_pembelian` */

DROP TABLE IF EXISTS `det_pembelian`;

CREATE TABLE `det_pembelian` (
  `dbli_id` int(11) NOT NULL AUTO_INCREMENT,
  `dbli_pmbl_id` int(11) DEFAULT NULL,
  `dbli_no_ref` varchar(30) DEFAULT NULL,
  `dbli_sham_id` int(11) DEFAULT NULL,
  `dbli_jumlah_lot` int(11) DEFAULT 0,
  `dbli_jumlah_shares` double DEFAULT 0,
  `dbli_harga_shares` double DEFAULT 0,
  `dbli_bruto` double DEFAULT 0,
  `dbli_fee_pembelian` double DEFAULT 0,
  `dbli_netto` double DEFAULT 0,
  PRIMARY KEY (`dbli_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `det_pembelian` */

/*Table structure for table `det_penjualan` */

DROP TABLE IF EXISTS `det_penjualan`;

CREATE TABLE `det_penjualan` (
  `dpjl_id` int(11) NOT NULL AUTO_INCREMENT,
  `dpjl_pnjl_id` int(11) DEFAULT NULL,
  `dpjl_no_ref` varchar(30) DEFAULT NULL,
  `dpjl_sham_id` int(11) DEFAULT NULL,
  `dpjl_jumlah_lot` int(11) DEFAULT 0,
  `dpjl_jumlah_shares` int(11) DEFAULT 0,
  `dpjl_harga_shares` double DEFAULT 0,
  `dpjl_bruto` double DEFAULT 0,
  `dpjl_fee_penjualan` double DEFAULT 0,
  `dpjl_netto` double DEFAULT 0,
  `dpjl_harga_rata_rata` double DEFAULT 0,
  PRIMARY KEY (`dpjl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `det_penjualan` */

/*Table structure for table `deviden` */

DROP TABLE IF EXISTS `deviden`;

CREATE TABLE `deviden` (
  `devi_id` int(11) NOT NULL AUTO_INCREMENT,
  `devi_kode` varchar(20) DEFAULT NULL,
  `devi_akun_id` int(11) DEFAULT NULL,
  `devi_tgl_bagi` date DEFAULT NULL,
  `devi_grand_tot` double DEFAULT 0,
  `devi_created_by` int(11) DEFAULT NULL,
  `devi_created_time` datetime DEFAULT NULL,
  `devi_updated_by` int(11) DEFAULT NULL,
  `devi_updated_time` datetime DEFAULT NULL,
  `devi_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`devi_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `deviden` */

/*Table structure for table `keuangan` */

DROP TABLE IF EXISTS `keuangan`;

CREATE TABLE `keuangan` (
  `uang_id` int(11) NOT NULL AUTO_INCREMENT,
  `uang_akun_id` int(11) NOT NULL,
  `uang_tgl` date NOT NULL,
  `uang_jns_transaksi` enum('setoran','penarikan','esetoran','epenarikan','deviden','pembelian','penjualan') DEFAULT NULL,
  `uang_transaksi_id` int(11) DEFAULT NULL,
  `uang_nominal` double NOT NULL DEFAULT 0,
  `uang_created_by` int(11) NOT NULL,
  `uang_created_time` datetime NOT NULL,
  PRIMARY KEY (`uang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `keuangan` */

/*Table structure for table `keuangan_saham` */

DROP TABLE IF EXISTS `keuangan_saham`;

CREATE TABLE `keuangan_saham` (
  `kshm_id` int(11) NOT NULL AUTO_INCREMENT,
  `kshm_akun_id` int(11) DEFAULT NULL,
  `kshm_sham_id` int(11) DEFAULT NULL,
  `kshm_tgl_transaksi` date DEFAULT NULL,
  `kshm_jns_transaksi` enum('penjualan','pembelian','epenjualan','epembelian') DEFAULT NULL,
  `kshm_transaksi_id` int(11) DEFAULT NULL,
  `kshm_jumlah` int(11) DEFAULT 0,
  `kshm_harga_rata_rata` double DEFAULT 0,
  `kshm_harga_rata_rata_2` double DEFAULT 0,
  `kshm_harga_netto` double DEFAULT 0,
  `kshm_created_by` int(11) DEFAULT NULL,
  `kshm_created_time` datetime DEFAULT NULL,
  PRIMARY KEY (`kshm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `keuangan_saham` */

/*Table structure for table `konfigurasi` */

DROP TABLE IF EXISTS `konfigurasi`;

CREATE TABLE `konfigurasi` (
  `konf_id` int(11) NOT NULL AUTO_INCREMENT,
  `konf_kode` varchar(50) NOT NULL,
  `konf_nama` varchar(100) NOT NULL,
  `konf_tipe_data` varchar(100) NOT NULL,
  `konf_nilai` double NOT NULL,
  `konf_is_deleted` enum('0','1') DEFAULT '1',
  `konf_created_time` datetime DEFAULT NULL,
  `konf_created_by` int(11) DEFAULT NULL,
  `konf_updated_time` datetime DEFAULT NULL,
  `konf_updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`konf_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `konfigurasi` */

insert  into `konfigurasi`(`konf_id`,`konf_kode`,`konf_nama`,`konf_tipe_data`,`konf_nilai`,`konf_is_deleted`,`konf_created_time`,`konf_created_by`,`konf_updated_time`,`konf_updated_by`) values 
(15,'pajak_penjualan','Pajak Penjualan','string',0.7,'1','2021-12-06 10:20:01',NULL,'2021-12-25 11:40:36',131),
(16,'pajak_deviden','Pajak Deviden','string',1.3,'1','2021-12-06 10:20:01',NULL,'2021-12-25 11:40:36',131);

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id_induk` int(11) DEFAULT NULL,
  `menu_kode` varchar(100) NOT NULL,
  `menu_nomor` varchar(20) NOT NULL DEFAULT '0',
  `menu_ikon` varchar(50) DEFAULT NULL,
  `menu_teks` varchar(200) NOT NULL,
  `menu_uri` varchar(200) NOT NULL,
  `menu_is_deleted` enum('0','1') NOT NULL DEFAULT '1',
  `menu_created_time` datetime DEFAULT NULL,
  `menu_created_id` int(11) DEFAULT NULL,
  `menu_updated_time` datetime DEFAULT NULL,
  `menu_updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `menu` */

insert  into `menu`(`menu_id`,`menu_id_induk`,`menu_kode`,`menu_nomor`,`menu_ikon`,`menu_teks`,`menu_uri`,`menu_is_deleted`,`menu_created_time`,`menu_created_id`,`menu_updated_time`,`menu_updated_by`) values 
(42,NULL,'dasbor','010.000','dashboard','Dashboard','/dasboard','1','2021-12-06 08:54:55',131,'0000-00-00 00:00:00',NULL),
(43,NULL,'transaksi','030.000','face-smile','Transaksi','#','1','2021-12-06 08:54:52',131,'0000-00-00 00:00:00',NULL),
(44,43,'setoran','030.010',NULL,'Setoran','/transaksi/setoran','1','2021-12-06 08:54:52',131,'0000-00-00 00:00:00',NULL),
(46,NULL,'master','050.000','harddrive','Data Master','#','1','2021-12-06 08:54:52',131,NULL,NULL),
(47,NULL,'laporan','040.000','money','Laporan','#','1','2021-12-08 17:32:25',131,NULL,NULL),
(49,47,'aruskas','040.010',NULL,'Arus Kas','/laporan/aruskas','1','2021-12-08 17:32:21',131,NULL,NULL),
(50,47,'arussaham','040.020',NULL,'Arus Saham','/laporan/arussaham','1','2021-12-08 17:32:19',131,NULL,NULL),
(52,43,'penarikan','030.050',NULL,'Penarikan','/transaksi/penarikan','1','2021-12-08 17:32:17',131,NULL,NULL),
(53,43,'pembelian','030.020',NULL,'Pembelian Saham','/transaksi/pembelian','1','2021-12-08 17:32:15',131,NULL,NULL),
(54,43,'penjualan','030.030',NULL,'Penjualan Saham','/transaksi/penjualan','1','2021-12-08 17:32:13',131,NULL,NULL),
(55,43,'deviden','030.040',NULL,'Deviden','/transaksi/deviden','1','2021-12-08 17:32:11',131,NULL,NULL),
(56,46,'bank','050.010',NULL,'Bank','/master/bank','1','2021-12-08 17:32:10',131,NULL,NULL),
(57,46,'akun','050.040',NULL,'Akun','/master/akun','1','2021-12-08 17:32:08',131,NULL,NULL),
(58,46,'saham','050.030',NULL,'Saham','/master/saham','1','2021-12-08 17:32:07',131,NULL,NULL),
(59,46,'sekuritas','050.020',NULL,'Sekuritas','/master/sekuritas','1','2021-12-08 17:32:04',131,NULL,NULL),
(60,NULL,'pengaturan','060.000','settings','Pengaturan','#','1','2021-12-08 17:32:02',131,NULL,NULL),
(61,60,'saldoawalrdn','060.030',NULL,'Saldo Awal RDN','/pengaturan/saldoawalrdn','0',NULL,NULL,NULL,NULL),
(62,60,'pajak','060.020',NULL,'Pajak','/pengaturan/pajak','1',NULL,NULL,NULL,NULL),
(63,46,'refpilihan','050.050',NULL,'Ref Pilihan','/master/refpilihan','0',NULL,NULL,NULL,NULL),
(64,60,'saldoawalsaham','060.010',NULL,'Saldo Awal Saham','/pengaturan/saldoawalsaham','1',NULL,NULL,NULL,NULL),
(65,NULL,'','0',NULL,'','','1',NULL,NULL,NULL,NULL);

/*Table structure for table `pembelian` */

DROP TABLE IF EXISTS `pembelian`;

CREATE TABLE `pembelian` (
  `pmbl_id` int(11) NOT NULL AUTO_INCREMENT,
  `pmbl_kode` varchar(20) DEFAULT NULL,
  `pmbl_akun_id` int(11) DEFAULT NULL,
  `pmbl_tgl_beli` date DEFAULT NULL,
  `pmbl_grand_tot` double DEFAULT 0,
  `pmbl_created_by` int(11) DEFAULT NULL,
  `pmbl_created_time` datetime DEFAULT NULL,
  `pmbl_updated_by` int(11) DEFAULT NULL,
  `pmbl_updated_time` datetime DEFAULT NULL,
  `pmbl_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`pmbl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `pembelian` */

/*Table structure for table `penarikan` */

DROP TABLE IF EXISTS `penarikan`;

CREATE TABLE `penarikan` (
  `trik_id` int(11) NOT NULL AUTO_INCREMENT,
  `trik_kode` varchar(20) DEFAULT NULL,
  `trik_tgl` date DEFAULT NULL,
  `trik_akun_id` int(11) DEFAULT NULL,
  `trik_bank_id` int(11) DEFAULT NULL,
  `trik_no_ref` varchar(20) DEFAULT NULL,
  `trik_nominal` double DEFAULT 0,
  `trik_created_by` int(11) DEFAULT NULL,
  `trik_created_time` datetime DEFAULT NULL,
  `trik_updated_by` int(11) DEFAULT NULL,
  `trik_updated_time` datetime DEFAULT NULL,
  `trik_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`trik_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `penarikan` */

/*Table structure for table `pengguna` */

DROP TABLE IF EXISTS `pengguna`;

CREATE TABLE `pengguna` (
  `peng_id` int(11) NOT NULL AUTO_INCREMENT,
  `peng_nama` varchar(100) NOT NULL,
  `peng_nama_lengkap` varchar(100) DEFAULT NULL,
  `peng_kata_sandi` varchar(50) NOT NULL,
  `peng_cookie` varchar(50) DEFAULT NULL,
  `peng_is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `peng_created_time` datetime NOT NULL,
  `peng_created_by` int(11) NOT NULL,
  `peng_updated_time` datetime NOT NULL,
  `peng_updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`peng_id`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `pengguna` */

insert  into `pengguna`(`peng_id`,`peng_nama`,`peng_nama_lengkap`,`peng_kata_sandi`,`peng_cookie`,`peng_is_deleted`,`peng_created_time`,`peng_created_by`,`peng_updated_time`,`peng_updated_by`) values 
(131,'dinata','Ega Budiman','2986032a8c843640542c6dad2e30b8cf','6dc2e8af07e57d795f1d7b62bfb88bd6',1,'0000-00-00 00:00:00',0,'2022-01-03 19:37:42',131);

/*Table structure for table `pengguna_grup_menu` */

DROP TABLE IF EXISTS `pengguna_grup_menu`;

CREATE TABLE `pengguna_grup_menu` (
  `grup_id` int(11) NOT NULL AUTO_INCREMENT,
  `grup_peng_id` int(11) NOT NULL,
  `grup_menu_id` int(11) NOT NULL,
  `grup_is_deleted` int(4) NOT NULL DEFAULT 1,
  `grup_created_time` datetime DEFAULT NULL,
  `grup_created_by` int(11) DEFAULT NULL,
  `grup_updated_time` datetime DEFAULT NULL,
  `grup_updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`grup_id`)
) ENGINE=InnoDB AUTO_INCREMENT=290 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `pengguna_grup_menu` */

insert  into `pengguna_grup_menu`(`grup_id`,`grup_peng_id`,`grup_menu_id`,`grup_is_deleted`,`grup_created_time`,`grup_created_by`,`grup_updated_time`,`grup_updated_by`) values 
(268,131,42,1,'2021-12-08 17:44:57',131,NULL,NULL),
(269,131,43,1,'2021-12-08 17:45:01',131,NULL,NULL),
(270,131,44,1,'2021-12-08 17:45:05',131,NULL,NULL),
(271,131,46,1,'2021-12-08 17:45:07',131,NULL,NULL),
(272,131,47,1,'2021-12-08 17:45:08',131,NULL,NULL),
(273,131,48,1,'2021-12-08 17:45:10',131,NULL,NULL),
(274,131,49,1,'2021-12-08 17:45:13',131,NULL,NULL),
(275,131,50,1,'2021-12-08 17:45:14',131,NULL,NULL),
(276,131,51,1,'2021-12-08 17:45:16',131,NULL,NULL),
(277,131,52,1,'2021-12-08 17:45:18',131,NULL,NULL),
(278,131,53,1,'2021-12-08 17:45:20',131,NULL,NULL),
(279,131,54,1,'2021-12-08 17:45:23',131,NULL,NULL),
(280,131,55,1,'2021-12-08 17:45:25',131,NULL,NULL),
(281,131,56,1,'2021-12-08 17:45:27',131,NULL,NULL),
(282,131,57,1,'2021-12-08 17:45:29',131,NULL,NULL),
(283,131,58,1,'2021-12-08 17:45:30',131,NULL,NULL),
(284,131,59,1,'2021-12-08 17:45:33',131,NULL,NULL),
(285,131,60,1,'2021-12-08 17:45:35',131,NULL,NULL),
(286,131,61,1,'2021-12-08 17:45:36',131,NULL,NULL),
(287,131,62,1,'2021-12-08 17:45:38',131,NULL,NULL),
(288,131,63,1,'2021-12-09 04:58:07',131,NULL,NULL),
(289,131,64,1,'2021-12-09 06:15:18',131,NULL,NULL);

/*Table structure for table `penjualan` */

DROP TABLE IF EXISTS `penjualan`;

CREATE TABLE `penjualan` (
  `pnjl_id` int(11) NOT NULL AUTO_INCREMENT,
  `pnjl_kode` varchar(20) DEFAULT NULL,
  `pnjl_akun_id` int(11) DEFAULT NULL,
  `pnjl_tgl_jual` date DEFAULT NULL,
  `pnjl_grand_tot` double DEFAULT 0,
  `pnjl_created_by` int(11) DEFAULT NULL,
  `pnjl_created_time` datetime DEFAULT NULL,
  `pnjl_updated_by` int(11) DEFAULT NULL,
  `pnjl_updated_time` datetime DEFAULT NULL,
  `pnjl_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`pnjl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `penjualan` */

/*Table structure for table `ref_pilihan` */

DROP TABLE IF EXISTS `ref_pilihan`;

CREATE TABLE `ref_pilihan` (
  `plhn_id` int(11) NOT NULL AUTO_INCREMENT,
  `plhn_kategori` varchar(100) NOT NULL,
  `plhn_nama` varchar(100) NOT NULL,
  `plhn_urutan` int(11) NOT NULL,
  `plhn_is_deleted` enum('0','1') NOT NULL DEFAULT '1',
  `plhn_created_time` datetime NOT NULL,
  `plhn_created_by` int(11) NOT NULL,
  `plhn_updated_time` datetime NOT NULL,
  `plhn_updated_by` int(11) NOT NULL,
  PRIMARY KEY (`plhn_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `ref_pilihan` */

insert  into `ref_pilihan`(`plhn_id`,`plhn_kategori`,`plhn_nama`,`plhn_urutan`,`plhn_is_deleted`,`plhn_created_time`,`plhn_created_by`,`plhn_updated_time`,`plhn_updated_by`) values 
(1,'kategori-saham','Sangat Baik',1,'1','2021-12-08 19:11:23',131,'0000-00-00 00:00:00',0),
(2,'kategori_saham','Baik',2,'1','2021-12-08 19:11:27',131,'0000-00-00 00:00:00',0),
(3,'kategori-saham','Cukup',3,'1','2021-12-08 19:11:29',131,'0000-00-00 00:00:00',0),
(4,'kategori-saham','Buruk',4,'1','2021-12-08 19:11:31',131,'0000-00-00 00:00:00',0),
(5,'sektor-saham','Telekomunikasi',1,'1','2021-12-08 19:11:33',131,'0000-00-00 00:00:00',0),
(6,'sektor-saham','Minerba',2,'1','2021-12-08 19:11:56',131,'0000-00-00 00:00:00',0),
(7,'sektor-saham','Bank',3,'0','2021-12-08 23:30:34',131,'2021-12-08 23:31:05',131);

/*Table structure for table `saham` */

DROP TABLE IF EXISTS `saham`;

CREATE TABLE `saham` (
  `sham_id` int(11) NOT NULL AUTO_INCREMENT,
  `sham_nama` varchar(100) NOT NULL,
  `sham_kode` varchar(10) NOT NULL,
  `sham_sektor` varchar(100) NOT NULL,
  `sham_seku_id` int(11) DEFAULT NULL,
  `sham_kinerja_kategori` varchar(100) NOT NULL,
  `sham_created_by` int(11) DEFAULT NULL,
  `sham_created_time` datetime DEFAULT NULL,
  `sham_updated_by` int(11) DEFAULT NULL,
  `sham_updated_time` datetime DEFAULT NULL,
  `sham_is_deleted` enum('0','1') DEFAULT '1',
  `sham_total_lot` double DEFAULT 0,
  `sham_harga_rata_rata` double DEFAULT 0,
  PRIMARY KEY (`sham_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `saham` */

insert  into `saham`(`sham_id`,`sham_nama`,`sham_kode`,`sham_sektor`,`sham_seku_id`,`sham_kinerja_kategori`,`sham_created_by`,`sham_created_time`,`sham_updated_by`,`sham_updated_time`,`sham_is_deleted`,`sham_total_lot`,`sham_harga_rata_rata`) values 
(1,'Telkom','TLKM','5',1,'1',1,'2021-12-06 08:35:55',131,'2021-12-08 13:47:13','1',0,0),
(2,'Metro Care','CARE','6',1,'1',1,'2021-12-06 08:47:31',131,'2021-12-08 13:47:04','1',0,0),
(3,'Sidomuncul','SIDO','6',2,'1',131,'2021-12-08 13:40:23',131,'2021-12-08 13:40:23','1',0,0);

/*Table structure for table `saldo_awal` */

DROP TABLE IF EXISTS `saldo_awal`;

CREATE TABLE `saldo_awal` (
  `swal_id` int(11) NOT NULL AUTO_INCREMENT,
  `swal_akun_id` int(11) DEFAULT NULL,
  `swal_tgl_saldo_awal` date DEFAULT NULL,
  `swal_jumlah_saldo` double DEFAULT 0,
  `swal_created_time` datetime DEFAULT NULL,
  `swal_created_by` int(11) DEFAULT NULL,
  `swal_updated_by` int(11) DEFAULT NULL,
  `swal_updated_time` datetime DEFAULT NULL,
  `swal_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`swal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `saldo_awal` */

/*Table structure for table `saldo_awal_saham` */

DROP TABLE IF EXISTS `saldo_awal_saham`;

CREATE TABLE `saldo_awal_saham` (
  `shal_id` int(11) NOT NULL AUTO_INCREMENT,
  `shal_akun_id` int(11) DEFAULT NULL,
  `shal_sham_id` int(11) DEFAULT NULL,
  `shal_tgl_saldo_awal` date DEFAULT NULL,
  `shal_jumlah_lot` double DEFAULT 0,
  `shal_jumlah_shares` double DEFAULT 0,
  `shal_rata_rata_harga` double DEFAULT 0,
  `shal_total` double DEFAULT 0,
  `shal_created_time` datetime DEFAULT NULL,
  `shal_created_by` int(11) DEFAULT NULL,
  `shal_updated_by` int(11) DEFAULT NULL,
  `shal_updated_time` datetime DEFAULT NULL,
  `shal_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`shal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `saldo_awal_saham` */

/*Table structure for table `sekuritas` */

DROP TABLE IF EXISTS `sekuritas`;

CREATE TABLE `sekuritas` (
  `seku_id` int(11) NOT NULL AUTO_INCREMENT,
  `seku_nama` varchar(100) NOT NULL,
  `seku_no_telp` varchar(20) NOT NULL,
  `seku_fax` varchar(20) NOT NULL,
  `seku_email` varchar(30) NOT NULL,
  `seku_alamat` text NOT NULL,
  `seku_broker_fee_pembelian` double NOT NULL DEFAULT 0,
  `seku_broker_fee_penjualan` double NOT NULL DEFAULT 0,
  `seku_created_by` int(11) DEFAULT NULL,
  `seku_created_time` datetime DEFAULT NULL,
  `seku_updated_by` int(11) DEFAULT NULL,
  `seku_updated_time` datetime DEFAULT NULL,
  `seku_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`seku_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

/*Data for the table `sekuritas` */

insert  into `sekuritas`(`seku_id`,`seku_nama`,`seku_no_telp`,`seku_fax`,`seku_email`,`seku_alamat`,`seku_broker_fee_pembelian`,`seku_broker_fee_penjualan`,`seku_created_by`,`seku_created_time`,`seku_updated_by`,`seku_updated_time`,`seku_is_deleted`) values 
(1,'Ajaib','-','-','-','-',0.8,0.3,1,'2021-12-06 07:56:08',131,'2021-12-23 19:00:04','1'),
(2,'BNI Sekuritas','-','-','-','-',0.1,0.7,1,'2021-12-06 07:56:41',131,'2021-12-23 19:07:27','1'),
(3,'Mandiri Sekuritas','-','-','-','-',0,0.9,1,'2021-12-06 08:49:18',131,'2021-12-23 20:19:36','1'),
(5,'Binomo','123456789123','123456789123','asd','asd',3,2,131,'2021-12-08 22:50:20',131,'2021-12-08 22:50:20','1');

/*Table structure for table `setoran` */

DROP TABLE IF EXISTS `setoran`;

CREATE TABLE `setoran` (
  `stor_id` int(11) NOT NULL AUTO_INCREMENT,
  `stor_kode` varchar(20) DEFAULT NULL,
  `stor_tgl` date DEFAULT NULL,
  `stor_akun_id` int(11) DEFAULT NULL,
  `stor_bank_id` int(11) DEFAULT NULL,
  `stor_no_ref` varchar(20) DEFAULT NULL,
  `stor_nominal` double DEFAULT 0,
  `stor_created_by` int(11) DEFAULT NULL,
  `stor_created_time` datetime DEFAULT NULL,
  `stor_updated_by` int(11) DEFAULT NULL,
  `stor_updated_time` datetime DEFAULT NULL,
  `stor_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`stor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `setoran` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
