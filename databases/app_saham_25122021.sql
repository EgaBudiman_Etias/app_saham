/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 10.4.14-MariaDB : Database - app_saham
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `akun` */

DROP TABLE IF EXISTS `akun`;

CREATE TABLE `akun` (
  `akun_id` int(11) NOT NULL AUTO_INCREMENT,
  `akun_kode` varchar(10) DEFAULT NULL,
  `akun_seku_id` int(11) NOT NULL,
  `akun_kode_nasabah` varchar(100) NOT NULL,
  `akun_no_sid` varchar(100) NOT NULL,
  `akun_no_ksei_kpei` varchar(100) NOT NULL,
  `akun_bank_rdn` varchar(100) NOT NULL,
  `akun_bank_cab_rdn` varchar(100) DEFAULT NULL,
  `akun_no_rekening_rdn` varchar(100) NOT NULL,
  `akun_email` varchar(100) NOT NULL,
  `akun_created_by` int(11) NOT NULL,
  `akun_created_time` datetime DEFAULT NULL,
  `akun_updated_by` int(11) DEFAULT NULL,
  `akun_updated_time` datetime DEFAULT NULL,
  `akun_is_deleted` enum('0','1') DEFAULT '1' COMMENT '0= non aktif, 1= aktif',
  `akun_balance` double DEFAULT 0,
  `akun_setoran` double DEFAULT 0,
  `akun_penarikan` double DEFAULT 0,
  PRIMARY KEY (`akun_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `bank` */

DROP TABLE IF EXISTS `bank`;

CREATE TABLE `bank` (
  `bank_id` int(11) NOT NULL AUTO_INCREMENT,
  `bank_judul` varchar(100) NOT NULL,
  `bank_nama` varchar(100) NOT NULL,
  `bank_cabang` varchar(100) DEFAULT NULL,
  `bank_no_rekening` varchar(20) NOT NULL,
  `bank_nama_pemilik` varchar(100) NOT NULL,
  `bank_created_by` int(11) DEFAULT NULL,
  `bank_created_time` datetime DEFAULT NULL,
  `bank_updated_by` int(11) DEFAULT NULL,
  `bank_updated_time` datetime DEFAULT NULL,
  `bank_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`bank_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `det_deviden` */

DROP TABLE IF EXISTS `det_deviden`;

CREATE TABLE `det_deviden` (
  `ddev_id` int(11) NOT NULL AUTO_INCREMENT,
  `ddev_devi_id` int(11) DEFAULT NULL,
  `ddev_no_ref` varchar(30) DEFAULT NULL,
  `ddev_sham_id` int(11) DEFAULT 0,
  `ddev_jumlah_lot` int(11) DEFAULT 0,
  `ddev_jumlah_shares` int(11) DEFAULT NULL,
  `ddev_deviden_shares` double DEFAULT 0,
  `ddev_netto` double DEFAULT 0,
  `ddev_pajak_deviden` double DEFAULT 0,
  `ddev_bruto` double DEFAULT 0,
  PRIMARY KEY (`ddev_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `det_pembelian` */

DROP TABLE IF EXISTS `det_pembelian`;

CREATE TABLE `det_pembelian` (
  `dbli_id` int(11) NOT NULL AUTO_INCREMENT,
  `dbli_pmbl_id` int(11) DEFAULT NULL,
  `dbli_no_ref` varchar(30) DEFAULT NULL,
  `dbli_sham_id` int(11) DEFAULT NULL,
  `dbli_jumlah_lot` int(11) DEFAULT 0,
  `dbli_jumlah_shares` double DEFAULT 0,
  `dbli_harga_shares` double DEFAULT 0,
  `dbli_bruto` double DEFAULT 0,
  `dbli_fee_pembelian` double DEFAULT 0,
  `dbli_netto` double DEFAULT 0,
  PRIMARY KEY (`dbli_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `det_penjualan` */

DROP TABLE IF EXISTS `det_penjualan`;

CREATE TABLE `det_penjualan` (
  `dpjl_id` int(11) NOT NULL AUTO_INCREMENT,
  `dpjl_pnjl_id` int(11) DEFAULT NULL,
  `dpjl_no_ref` varchar(30) DEFAULT NULL,
  `dpjl_sham_id` int(11) DEFAULT NULL,
  `dpjl_jumlah_lot` int(11) DEFAULT 0,
  `dpjl_jumlah_shares` int(11) DEFAULT 0,
  `dpjl_harga_shares` double DEFAULT 0,
  `dpjl_bruto` double DEFAULT 0,
  `dpjl_fee_penjualan` double DEFAULT 0,
  `dpjl_netto` double DEFAULT 0,
  `dpjl_harga_rata_rata` double DEFAULT 0,
  PRIMARY KEY (`dpjl_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `deviden` */

DROP TABLE IF EXISTS `deviden`;

CREATE TABLE `deviden` (
  `devi_id` int(11) NOT NULL AUTO_INCREMENT,
  `devi_akun_id` int(11) DEFAULT NULL,
  `devi_tgl_bagi` date DEFAULT NULL,
  `devi_grand_tot` double DEFAULT 0,
  `devi_created_by` int(11) DEFAULT NULL,
  `devi_created_time` datetime DEFAULT NULL,
  `devi_updated_by` int(11) DEFAULT NULL,
  `devi_updated_time` datetime DEFAULT NULL,
  `devi_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`devi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `keuangan` */

DROP TABLE IF EXISTS `keuangan`;

CREATE TABLE `keuangan` (
  `uang_id` int(11) NOT NULL AUTO_INCREMENT,
  `uang_akun_id` int(11) NOT NULL,
  `uang_tgl` date NOT NULL,
  `uang_jns_transaksi` enum('setoran','penarikan','esetoran','epenarikan','deviden') DEFAULT NULL,
  `uang_transaksi_id` int(11) DEFAULT NULL,
  `uang_nominal` double NOT NULL DEFAULT 0,
  `uang_created_by` int(11) NOT NULL,
  `uang_created_time` datetime NOT NULL,
  PRIMARY KEY (`uang_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `keuangan_saham` */

DROP TABLE IF EXISTS `keuangan_saham`;

CREATE TABLE `keuangan_saham` (
  `kshm_id` int(11) NOT NULL AUTO_INCREMENT,
  `kshm_sham_id` int(11) DEFAULT NULL,
  `kshm_tgl_transaksi` date DEFAULT NULL,
  `kshm_jns_transaksi` enum('penjualan','pembelian','epenjualan','epembelian') DEFAULT NULL,
  `kshm_transaksi_id` int(11) DEFAULT NULL,
  `kshm_jumlah` int(11) DEFAULT 0,
  `kshm_harga_rata_rata` double DEFAULT 0,
  `kshm_harga_rata_rata_2` double DEFAULT 0,
  `kshm_harga_netto` double DEFAULT 0,
  `kshm_created_by` int(11) DEFAULT NULL,
  `kshm_created_time` datetime DEFAULT NULL,
  PRIMARY KEY (`kshm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `konfigurasi` */

DROP TABLE IF EXISTS `konfigurasi`;

CREATE TABLE `konfigurasi` (
  `konf_id` int(11) NOT NULL AUTO_INCREMENT,
  `konf_kode` varchar(50) NOT NULL,
  `konf_nama` varchar(100) NOT NULL,
  `konf_tipe_data` varchar(100) NOT NULL,
  `konf_nilai` double NOT NULL,
  `konf_is_deleted` enum('0','1') DEFAULT '1',
  `konf_created_time` datetime DEFAULT NULL,
  `konf_created_by` int(11) DEFAULT NULL,
  `konf_updated_time` datetime DEFAULT NULL,
  `konf_updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`konf_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id_induk` int(11) DEFAULT NULL,
  `menu_kode` varchar(100) NOT NULL,
  `menu_nomor` varchar(20) NOT NULL DEFAULT '0',
  `menu_ikon` varchar(50) DEFAULT NULL,
  `menu_teks` varchar(200) NOT NULL,
  `menu_uri` varchar(200) NOT NULL,
  `menu_is_deleted` enum('0','1') NOT NULL DEFAULT '1',
  `menu_created_time` datetime DEFAULT NULL,
  `menu_created_id` int(11) DEFAULT NULL,
  `menu_updated_time` datetime DEFAULT NULL,
  `menu_updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Table structure for table `pembelian` */

DROP TABLE IF EXISTS `pembelian`;

CREATE TABLE `pembelian` (
  `pmbl_id` int(11) NOT NULL AUTO_INCREMENT,
  `pmbl_akun_id` int(11) DEFAULT NULL,
  `pmbl_tgl_beli` date DEFAULT NULL,
  `pmbl_grand_tot` double DEFAULT 0,
  `pmbl_created_by` int(11) DEFAULT NULL,
  `pmbl_created_time` datetime DEFAULT NULL,
  `pmbl_updated_by` int(11) DEFAULT NULL,
  `pmbl_updated_time` datetime DEFAULT NULL,
  `pmbl_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`pmbl_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `penarikan` */

DROP TABLE IF EXISTS `penarikan`;

CREATE TABLE `penarikan` (
  `trik_id` int(11) NOT NULL AUTO_INCREMENT,
  `trik_tgl` date DEFAULT NULL,
  `trik_akun_id` int(11) DEFAULT NULL,
  `trik_bank_id` int(11) DEFAULT NULL,
  `trik_no_ref` varchar(20) DEFAULT NULL,
  `trik_nominal` double DEFAULT 0,
  `trik_created_by` int(11) DEFAULT NULL,
  `trik_created_time` datetime DEFAULT NULL,
  `trik_updated_by` int(11) DEFAULT NULL,
  `trik_updated_time` datetime DEFAULT NULL,
  `trik_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`trik_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `pengguna` */

DROP TABLE IF EXISTS `pengguna`;

CREATE TABLE `pengguna` (
  `peng_id` int(11) NOT NULL AUTO_INCREMENT,
  `peng_nama` varchar(100) NOT NULL,
  `peng_nama_lengkap` varchar(100) DEFAULT NULL,
  `peng_kata_sandi` varchar(50) NOT NULL,
  `peng_cookie` varchar(50) DEFAULT NULL,
  `peng_is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `peng_created_time` datetime NOT NULL,
  `peng_created_by` int(11) NOT NULL,
  `peng_updated_time` datetime NOT NULL,
  `peng_updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`peng_id`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Table structure for table `pengguna_grup_menu` */

DROP TABLE IF EXISTS `pengguna_grup_menu`;

CREATE TABLE `pengguna_grup_menu` (
  `grup_id` int(11) NOT NULL AUTO_INCREMENT,
  `grup_peng_id` int(11) NOT NULL,
  `grup_menu_id` int(11) NOT NULL,
  `grup_is_deleted` int(4) NOT NULL DEFAULT 1,
  `grup_created_time` datetime DEFAULT NULL,
  `grup_created_by` int(11) DEFAULT NULL,
  `grup_updated_time` datetime DEFAULT NULL,
  `grup_updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`grup_id`)
) ENGINE=InnoDB AUTO_INCREMENT=290 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Table structure for table `penjualan` */

DROP TABLE IF EXISTS `penjualan`;

CREATE TABLE `penjualan` (
  `pnjl_id` int(11) NOT NULL AUTO_INCREMENT,
  `pnjl_akun_id` int(11) DEFAULT NULL,
  `pnjl_tgl_jual` date DEFAULT NULL,
  `pnjl_grand_tot` double DEFAULT 0,
  `pnjl_created_by` int(11) DEFAULT NULL,
  `pnjl_created_time` datetime DEFAULT NULL,
  `pnjl_updated_by` int(11) DEFAULT NULL,
  `pnjl_updated_time` datetime DEFAULT NULL,
  `pnjl_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`pnjl_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `ref_pilihan` */

DROP TABLE IF EXISTS `ref_pilihan`;

CREATE TABLE `ref_pilihan` (
  `plhn_id` int(11) NOT NULL AUTO_INCREMENT,
  `plhn_kategori` varchar(100) NOT NULL,
  `plhn_nama` varchar(100) NOT NULL,
  `plhn_urutan` int(11) NOT NULL,
  `plhn_is_deleted` enum('0','1') NOT NULL DEFAULT '1',
  `plhn_created_time` datetime NOT NULL,
  `plhn_created_by` int(11) NOT NULL,
  `plhn_updated_time` datetime NOT NULL,
  `plhn_updated_by` int(11) NOT NULL,
  PRIMARY KEY (`plhn_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Table structure for table `saham` */

DROP TABLE IF EXISTS `saham`;

CREATE TABLE `saham` (
  `sham_id` int(11) NOT NULL AUTO_INCREMENT,
  `sham_nama` varchar(100) NOT NULL,
  `sham_kode` varchar(10) NOT NULL,
  `sham_sektor` varchar(100) NOT NULL,
  `sham_seku_id` int(11) DEFAULT NULL,
  `sham_kinerja_kategori` varchar(100) NOT NULL,
  `sham_created_by` int(11) DEFAULT NULL,
  `sham_created_time` datetime DEFAULT NULL,
  `sham_updated_by` int(11) DEFAULT NULL,
  `sham_updated_time` datetime DEFAULT NULL,
  `sham_is_deleted` enum('0','1') DEFAULT '1',
  `sham_total_lot` double DEFAULT 0,
  `sham_harga_rata_rata` double DEFAULT 0,
  PRIMARY KEY (`sham_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `saldo_awal` */

DROP TABLE IF EXISTS `saldo_awal`;

CREATE TABLE `saldo_awal` (
  `swal_id` int(11) NOT NULL AUTO_INCREMENT,
  `swal_akun_id` int(11) DEFAULT NULL,
  `swal_tgl_saldo_awal` date DEFAULT NULL,
  `swal_jumlah_saldo` double DEFAULT 0,
  `swal_created_time` datetime DEFAULT NULL,
  `swal_created_by` int(11) DEFAULT NULL,
  `swal_updated_by` int(11) DEFAULT NULL,
  `swal_updated_time` datetime DEFAULT NULL,
  `swal_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`swal_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `saldo_awal_saham` */

DROP TABLE IF EXISTS `saldo_awal_saham`;

CREATE TABLE `saldo_awal_saham` (
  `shal_id` int(11) NOT NULL AUTO_INCREMENT,
  `shal_akun_id` int(11) DEFAULT NULL,
  `shal_sham_id` int(11) DEFAULT NULL,
  `shal_tgl_saldo_awal` date DEFAULT NULL,
  `shal_jumlah_lot` double DEFAULT 0,
  `shal_jumlah_shares` double DEFAULT 0,
  `shal_rata_rata_harga` double DEFAULT 0,
  `shal_total` double DEFAULT 0,
  `shal_created_time` datetime DEFAULT NULL,
  `shal_created_by` int(11) DEFAULT NULL,
  `shal_updated_by` int(11) DEFAULT NULL,
  `shal_updated_time` datetime DEFAULT NULL,
  `shal_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`shal_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `sekuritas` */

DROP TABLE IF EXISTS `sekuritas`;

CREATE TABLE `sekuritas` (
  `seku_id` int(11) NOT NULL AUTO_INCREMENT,
  `seku_nama` varchar(100) NOT NULL,
  `seku_no_telp` varchar(20) NOT NULL,
  `seku_fax` varchar(20) NOT NULL,
  `seku_email` varchar(30) NOT NULL,
  `seku_alamat` text NOT NULL,
  `seku_broker_fee_pembelian` double NOT NULL DEFAULT 0,
  `seku_broker_fee_penjualan` double NOT NULL DEFAULT 0,
  `seku_created_by` int(11) DEFAULT NULL,
  `seku_created_time` datetime DEFAULT NULL,
  `seku_updated_by` int(11) DEFAULT NULL,
  `seku_updated_time` datetime DEFAULT NULL,
  `seku_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`seku_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `setoran` */

DROP TABLE IF EXISTS `setoran`;

CREATE TABLE `setoran` (
  `stor_id` int(11) NOT NULL AUTO_INCREMENT,
  `stor_tgl` date DEFAULT NULL,
  `stor_akun_id` int(11) DEFAULT NULL,
  `stor_bank_id` int(11) DEFAULT NULL,
  `stor_no_ref` varchar(20) DEFAULT NULL,
  `stor_nominal` double DEFAULT 0,
  `stor_created_by` int(11) DEFAULT NULL,
  `stor_created_time` datetime DEFAULT NULL,
  `stor_updated_by` int(11) DEFAULT NULL,
  `stor_updated_time` datetime DEFAULT NULL,
  `stor_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`stor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
