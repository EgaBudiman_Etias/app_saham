/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 10.4.14-MariaDB : Database - app_saham
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `akun` */

DROP TABLE IF EXISTS `akun`;

CREATE TABLE `akun` (
  `akun_id` int(11) NOT NULL AUTO_INCREMENT,
  `akun_kode` varchar(10) DEFAULT NULL,
  `akun_seku_id` int(11) NOT NULL,
  `akun_kode_nasabah` varchar(100) NOT NULL,
  `akun_no_sid` varchar(100) NOT NULL,
  `akun_no_ksei_kpei` varchar(100) NOT NULL,
  `akun_bank_rdn` varchar(100) NOT NULL,
  `akun_bank_cab_rdn` varchar(100) DEFAULT NULL,
  `akun_no_rekening_rdn` varchar(100) NOT NULL,
  `akun_email` varchar(100) NOT NULL,
  `akun_created_by` int(11) NOT NULL,
  `akun_created_time` datetime DEFAULT NULL,
  `akun_updated_by` int(11) DEFAULT NULL,
  `akun_updated_time` datetime DEFAULT NULL,
  `akun_is_deleted` enum('0','1') DEFAULT '1' COMMENT '0= non aktif, 1= aktif',
  `akun_balance` double DEFAULT 0,
  `akun_setoran` double DEFAULT 0,
  `akun_penarikan` double DEFAULT 0,
  PRIMARY KEY (`akun_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Data for the table `akun` */

insert  into `akun`(`akun_id`,`akun_kode`,`akun_seku_id`,`akun_kode_nasabah`,`akun_no_sid`,`akun_no_ksei_kpei`,`akun_bank_rdn`,`akun_bank_cab_rdn`,`akun_no_rekening_rdn`,`akun_email`,`akun_created_by`,`akun_created_time`,`akun_updated_by`,`akun_updated_time`,`akun_is_deleted`,`akun_balance`,`akun_setoran`,`akun_penarikan`) values 
(1,'AK0001',2,'29Yut','1768654','34986321','Bank Jago','Lampung','3450987','egabudiman20@gmail.com',131,'2021-12-08 18:01:32',131,'2021-12-28 16:56:09','0',0,0,0),
(2,'AK0002',1,'4567','4211111','67777','Danamon','Lampung','09999','Diki',131,'2021-12-08 12:52:56',131,'2021-12-08 13:06:31','0',0,0,0),
(5,'AK0003',1,'123','1212','1212','BCA',NULL,'4545','asasas',131,'2021-12-28 16:56:36',131,'2021-12-28 17:08:25','1',0,0,0);

/*Table structure for table `bank` */

DROP TABLE IF EXISTS `bank`;

CREATE TABLE `bank` (
  `bank_id` int(11) NOT NULL AUTO_INCREMENT,
  `bank_judul` varchar(100) NOT NULL,
  `bank_nama` varchar(100) NOT NULL,
  `bank_cabang` varchar(100) DEFAULT NULL,
  `bank_no_rekening` varchar(20) NOT NULL,
  `bank_nama_pemilik` varchar(100) NOT NULL,
  `bank_created_by` int(11) DEFAULT NULL,
  `bank_created_time` datetime DEFAULT NULL,
  `bank_updated_by` int(11) DEFAULT NULL,
  `bank_updated_time` datetime DEFAULT NULL,
  `bank_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`bank_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

/*Data for the table `bank` */

insert  into `bank`(`bank_id`,`bank_judul`,`bank_nama`,`bank_cabang`,`bank_no_rekening`,`bank_nama_pemilik`,`bank_created_by`,`bank_created_time`,`bank_updated_by`,`bank_updated_time`,`bank_is_deleted`) values 
(1,'BNI','BNI',NULL,'00018729','Budiman',NULL,NULL,NULL,NULL,'1'),
(2,'BCA','BCA',NULL,'9800827618765','Budiman',NULL,NULL,131,'2021-12-23 21:32:05','1'),
(3,'MANDIRI','MANDIRI',NULL,'76399717','Budiman',NULL,NULL,NULL,NULL,'1');

/*Table structure for table `det_deviden` */

DROP TABLE IF EXISTS `det_deviden`;

CREATE TABLE `det_deviden` (
  `ddev_id` int(11) NOT NULL AUTO_INCREMENT,
  `ddev_devi_id` int(11) DEFAULT NULL,
  `ddev_no_ref` varchar(30) DEFAULT NULL,
  `ddev_sham_id` int(11) DEFAULT 0,
  `ddev_jumlah_lot` int(11) DEFAULT 0,
  `ddev_jumlah_shares` int(11) DEFAULT NULL,
  `ddev_deviden_shares` double DEFAULT 0,
  `ddev_netto` double DEFAULT 0,
  `ddev_pajak_deviden` double DEFAULT 0,
  `ddev_bruto` double DEFAULT 0,
  PRIMARY KEY (`ddev_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `det_deviden` */

/*Table structure for table `det_pembelian` */

DROP TABLE IF EXISTS `det_pembelian`;

CREATE TABLE `det_pembelian` (
  `dbli_id` int(11) NOT NULL AUTO_INCREMENT,
  `dbli_pmbl_id` int(11) DEFAULT NULL,
  `dbli_no_ref` varchar(30) DEFAULT NULL,
  `dbli_sham_id` int(11) DEFAULT NULL,
  `dbli_jumlah_lot` int(11) DEFAULT 0,
  `dbli_jumlah_shares` double DEFAULT 0,
  `dbli_harga_shares` double DEFAULT 0,
  `dbli_bruto` double DEFAULT 0,
  `dbli_fee_pembelian` double DEFAULT 0,
  `dbli_netto` double DEFAULT 0,
  PRIMARY KEY (`dbli_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4;

/*Data for the table `det_pembelian` */

insert  into `det_pembelian`(`dbli_id`,`dbli_pmbl_id`,`dbli_no_ref`,`dbli_sham_id`,`dbli_jumlah_lot`,`dbli_jumlah_shares`,`dbli_harga_shares`,`dbli_bruto`,`dbli_fee_pembelian`,`dbli_netto`) values 
(16,2,NULL,2,1000,100000,1,100000,800,100800),
(17,1,NULL,1,100,10000,120,1200000,9600,1209600),
(18,1,NULL,2,300,30000,1,30000,240,30240);

/*Table structure for table `det_penjualan` */

DROP TABLE IF EXISTS `det_penjualan`;

CREATE TABLE `det_penjualan` (
  `dpjl_id` int(11) NOT NULL AUTO_INCREMENT,
  `dpjl_pnjl_id` int(11) DEFAULT NULL,
  `dpjl_no_ref` varchar(30) DEFAULT NULL,
  `dpjl_sham_id` int(11) DEFAULT NULL,
  `dpjl_jumlah_lot` int(11) DEFAULT 0,
  `dpjl_jumlah_shares` int(11) DEFAULT 0,
  `dpjl_harga_shares` double DEFAULT 0,
  `dpjl_bruto` double DEFAULT 0,
  `dpjl_fee_penjualan` double DEFAULT 0,
  `dpjl_netto` double DEFAULT 0,
  `dpjl_harga_rata_rata` double DEFAULT 0,
  PRIMARY KEY (`dpjl_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `det_penjualan` */

insert  into `det_penjualan`(`dpjl_id`,`dpjl_pnjl_id`,`dpjl_no_ref`,`dpjl_sham_id`,`dpjl_jumlah_lot`,`dpjl_jumlah_shares`,`dpjl_harga_shares`,`dpjl_bruto`,`dpjl_fee_penjualan`,`dpjl_netto`,`dpjl_harga_rata_rata`) values 
(1,1,NULL,1,1,100,1000,100000,300,99700,0),
(2,1,NULL,2,1,100,12000,1200000,3600,1196400,0);

/*Table structure for table `deviden` */

DROP TABLE IF EXISTS `deviden`;

CREATE TABLE `deviden` (
  `devi_id` int(11) NOT NULL AUTO_INCREMENT,
  `devi_akun_id` int(11) DEFAULT NULL,
  `devi_tgl_bagi` date DEFAULT NULL,
  `devi_grand_tot` double DEFAULT 0,
  `devi_created_by` int(11) DEFAULT NULL,
  `devi_created_time` datetime DEFAULT NULL,
  `devi_updated_by` int(11) DEFAULT NULL,
  `devi_updated_time` datetime DEFAULT NULL,
  `devi_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`devi_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `deviden` */

/*Table structure for table `keuangan` */

DROP TABLE IF EXISTS `keuangan`;

CREATE TABLE `keuangan` (
  `uang_id` int(11) NOT NULL AUTO_INCREMENT,
  `uang_akun_id` int(11) NOT NULL,
  `uang_tgl` date NOT NULL,
  `uang_jns_transaksi` enum('setoran','penarikan','esetoran','epenarikan','deviden','pembelian','penjualan') DEFAULT NULL,
  `uang_transaksi_id` int(11) DEFAULT NULL,
  `uang_nominal` double NOT NULL DEFAULT 0,
  `uang_created_by` int(11) NOT NULL,
  `uang_created_time` datetime NOT NULL,
  PRIMARY KEY (`uang_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;

/*Data for the table `keuangan` */

insert  into `keuangan`(`uang_id`,`uang_akun_id`,`uang_tgl`,`uang_jns_transaksi`,`uang_transaksi_id`,`uang_nominal`,`uang_created_by`,`uang_created_time`) values 
(5,5,'2021-12-28','setoran',0,30000000,131,'2021-12-28 17:08:25'),
(6,5,'2021-12-28','setoran',2,10000000,131,'2021-12-28 17:08:45'),
(12,5,'2021-12-28','setoran',3,30000000,131,'2021-12-28 19:49:13'),
(15,5,'2021-12-28','setoran',4,20000000,131,'2021-12-28 21:10:45'),
(18,5,'2021-12-28','pembelian',2,-100800,131,'2021-12-28 21:25:59'),
(19,5,'2021-12-28','pembelian',1,-1239840,131,'2021-12-28 21:26:04'),
(20,5,'2021-12-28','penjualan',1,1296100,131,'2021-12-28 22:48:46');

/*Table structure for table `keuangan_saham` */

DROP TABLE IF EXISTS `keuangan_saham`;

CREATE TABLE `keuangan_saham` (
  `kshm_id` int(11) NOT NULL AUTO_INCREMENT,
  `kshm_akun_id` int(11) DEFAULT NULL,
  `kshm_sham_id` int(11) DEFAULT NULL,
  `kshm_tgl_transaksi` date DEFAULT NULL,
  `kshm_jns_transaksi` enum('penjualan','pembelian','epenjualan','epembelian') DEFAULT NULL,
  `kshm_transaksi_id` int(11) DEFAULT NULL,
  `kshm_jumlah` int(11) DEFAULT 0,
  `kshm_harga_rata_rata` double DEFAULT 0,
  `kshm_harga_rata_rata_2` double DEFAULT 0,
  `kshm_harga_netto` double DEFAULT 0,
  `kshm_created_by` int(11) DEFAULT NULL,
  `kshm_created_time` datetime DEFAULT NULL,
  PRIMARY KEY (`kshm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;

/*Data for the table `keuangan_saham` */

insert  into `keuangan_saham`(`kshm_id`,`kshm_akun_id`,`kshm_sham_id`,`kshm_tgl_transaksi`,`kshm_jns_transaksi`,`kshm_transaksi_id`,`kshm_jumlah`,`kshm_harga_rata_rata`,`kshm_harga_rata_rata_2`,`kshm_harga_netto`,`kshm_created_by`,`kshm_created_time`) values 
(16,5,2,'2021-12-28','pembelian',2,1000,1,0,100800,131,'2021-12-28 21:25:59'),
(17,5,1,'2021-12-28','pembelian',1,100,120,0,1209600,131,'2021-12-28 21:26:04'),
(18,5,2,'2021-12-28','pembelian',1,300,1,0,30240,131,'2021-12-28 21:26:04'),
(19,NULL,1,'2021-12-28','penjualan',1,-1,-1000,0,-99700,131,'2021-12-28 22:48:46'),
(20,NULL,2,'2021-12-28','penjualan',1,-1,-12000,0,-1196400,131,'2021-12-28 22:48:46');

/*Table structure for table `konfigurasi` */

DROP TABLE IF EXISTS `konfigurasi`;

CREATE TABLE `konfigurasi` (
  `konf_id` int(11) NOT NULL AUTO_INCREMENT,
  `konf_kode` varchar(50) NOT NULL,
  `konf_nama` varchar(100) NOT NULL,
  `konf_tipe_data` varchar(100) NOT NULL,
  `konf_nilai` double NOT NULL,
  `konf_is_deleted` enum('0','1') DEFAULT '1',
  `konf_created_time` datetime DEFAULT NULL,
  `konf_created_by` int(11) DEFAULT NULL,
  `konf_updated_time` datetime DEFAULT NULL,
  `konf_updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`konf_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `konfigurasi` */

insert  into `konfigurasi`(`konf_id`,`konf_kode`,`konf_nama`,`konf_tipe_data`,`konf_nilai`,`konf_is_deleted`,`konf_created_time`,`konf_created_by`,`konf_updated_time`,`konf_updated_by`) values 
(15,'pajak_penjualan','Pajak Penjualan','string',0.7,'1','2021-12-06 10:20:01',NULL,'2021-12-25 11:40:36',131),
(16,'pajak_deviden','Pajak Deviden','string',1.3,'1','2021-12-06 10:20:01',NULL,'2021-12-25 11:40:36',131);

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id_induk` int(11) DEFAULT NULL,
  `menu_kode` varchar(100) NOT NULL,
  `menu_nomor` varchar(20) NOT NULL DEFAULT '0',
  `menu_ikon` varchar(50) DEFAULT NULL,
  `menu_teks` varchar(200) NOT NULL,
  `menu_uri` varchar(200) NOT NULL,
  `menu_is_deleted` enum('0','1') NOT NULL DEFAULT '1',
  `menu_created_time` datetime DEFAULT NULL,
  `menu_created_id` int(11) DEFAULT NULL,
  `menu_updated_time` datetime DEFAULT NULL,
  `menu_updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `menu` */

insert  into `menu`(`menu_id`,`menu_id_induk`,`menu_kode`,`menu_nomor`,`menu_ikon`,`menu_teks`,`menu_uri`,`menu_is_deleted`,`menu_created_time`,`menu_created_id`,`menu_updated_time`,`menu_updated_by`) values 
(42,NULL,'dasbor','010.000','dashboard','Dashboard','/dasboard','1','2021-12-06 08:54:55',131,'0000-00-00 00:00:00',NULL),
(43,NULL,'transaksi','030.000','face-smile','Transaksi','#','1','2021-12-06 08:54:52',131,'0000-00-00 00:00:00',NULL),
(44,43,'setoran','030.010',NULL,'Setoran','/transaksi/setoran','1','2021-12-06 08:54:52',131,'0000-00-00 00:00:00',NULL),
(46,NULL,'master','050.000','harddrive','Data Master','#','1','2021-12-06 08:54:52',131,NULL,NULL),
(47,NULL,'laporan','040.000','money','Laporan','#','1','2021-12-08 17:32:25',131,NULL,NULL),
(49,47,'aruskas','040.010',NULL,'Arus Kas','/laporan/aruskas','1','2021-12-08 17:32:21',131,NULL,NULL),
(50,47,'arussaham','040.020',NULL,'Arus Saham','/laporan/arussaham','1','2021-12-08 17:32:19',131,NULL,NULL),
(52,43,'penarikan','030.050',NULL,'Penarikan','/transaksi/penarikan','1','2021-12-08 17:32:17',131,NULL,NULL),
(53,43,'pembelian','030.020',NULL,'Pembelian Saham','/transaksi/pembelian','1','2021-12-08 17:32:15',131,NULL,NULL),
(54,43,'penjualan','030.030',NULL,'Penjualan Saham','/transaksi/penjualan','1','2021-12-08 17:32:13',131,NULL,NULL),
(55,43,'deviden','030.040',NULL,'Deviden','/transaksi/deviden','1','2021-12-08 17:32:11',131,NULL,NULL),
(56,46,'bank','050.010',NULL,'Bank','/master/bank','1','2021-12-08 17:32:10',131,NULL,NULL),
(57,46,'akun','050.030',NULL,'Akun','/master/akun','1','2021-12-08 17:32:08',131,NULL,NULL),
(58,46,'saham','050.040',NULL,'Saham','/master/saham','1','2021-12-08 17:32:07',131,NULL,NULL),
(59,46,'sekuritas','050.020',NULL,'Sekuritas','/master/sekuritas','1','2021-12-08 17:32:04',131,NULL,NULL),
(60,NULL,'pengaturan','060.000','settings','Pengaturan','#','1','2021-12-08 17:32:02',131,NULL,NULL),
(61,60,'saldoawalrdn','060.030',NULL,'Saldo Awal RDN','/pengaturan/saldoawalrdn','0',NULL,NULL,NULL,NULL),
(62,60,'pajak','060.020',NULL,'Pajak','/pengaturan/pajak','1',NULL,NULL,NULL,NULL),
(63,46,'refpilihan','050.050',NULL,'Ref Pilihan','/master/refpilihan','0',NULL,NULL,NULL,NULL),
(64,60,'saldoawalsaham','060.010',NULL,'Saldo Awal Saham','/pengaturan/saldoawalsaham','1',NULL,NULL,NULL,NULL),
(65,NULL,'','0',NULL,'','','1',NULL,NULL,NULL,NULL);

/*Table structure for table `pembelian` */

DROP TABLE IF EXISTS `pembelian`;

CREATE TABLE `pembelian` (
  `pmbl_id` int(11) NOT NULL AUTO_INCREMENT,
  `pmbl_kode` varchar(20) DEFAULT NULL,
  `pmbl_akun_id` int(11) DEFAULT NULL,
  `pmbl_tgl_beli` date DEFAULT NULL,
  `pmbl_grand_tot` double DEFAULT 0,
  `pmbl_created_by` int(11) DEFAULT NULL,
  `pmbl_created_time` datetime DEFAULT NULL,
  `pmbl_updated_by` int(11) DEFAULT NULL,
  `pmbl_updated_time` datetime DEFAULT NULL,
  `pmbl_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`pmbl_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `pembelian` */

insert  into `pembelian`(`pmbl_id`,`pmbl_kode`,`pmbl_akun_id`,`pmbl_tgl_beli`,`pmbl_grand_tot`,`pmbl_created_by`,`pmbl_created_time`,`pmbl_updated_by`,`pmbl_updated_time`,`pmbl_is_deleted`) values 
(1,'B2021120001',5,'2021-12-28',1239840,131,'2021-12-28 19:13:37',131,'2021-12-28 21:26:04','1'),
(2,'B2021120002',5,'2021-12-29',100800,131,'2021-12-28 21:08:44',131,'2021-12-28 21:25:59','1');

/*Table structure for table `penarikan` */

DROP TABLE IF EXISTS `penarikan`;

CREATE TABLE `penarikan` (
  `trik_id` int(11) NOT NULL AUTO_INCREMENT,
  `trik_tgl` date DEFAULT NULL,
  `trik_akun_id` int(11) DEFAULT NULL,
  `trik_bank_id` int(11) DEFAULT NULL,
  `trik_no_ref` varchar(20) DEFAULT NULL,
  `trik_nominal` double DEFAULT 0,
  `trik_created_by` int(11) DEFAULT NULL,
  `trik_created_time` datetime DEFAULT NULL,
  `trik_updated_by` int(11) DEFAULT NULL,
  `trik_updated_time` datetime DEFAULT NULL,
  `trik_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`trik_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `penarikan` */

/*Table structure for table `pengguna` */

DROP TABLE IF EXISTS `pengguna`;

CREATE TABLE `pengguna` (
  `peng_id` int(11) NOT NULL AUTO_INCREMENT,
  `peng_nama` varchar(100) NOT NULL,
  `peng_nama_lengkap` varchar(100) DEFAULT NULL,
  `peng_kata_sandi` varchar(50) NOT NULL,
  `peng_cookie` varchar(50) DEFAULT NULL,
  `peng_is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `peng_created_time` datetime NOT NULL,
  `peng_created_by` int(11) NOT NULL,
  `peng_updated_time` datetime NOT NULL,
  `peng_updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`peng_id`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `pengguna` */

insert  into `pengguna`(`peng_id`,`peng_nama`,`peng_nama_lengkap`,`peng_kata_sandi`,`peng_cookie`,`peng_is_deleted`,`peng_created_time`,`peng_created_by`,`peng_updated_time`,`peng_updated_by`) values 
(131,'dinata','Ega Budiman','2986032a8c843640542c6dad2e30b8cf',NULL,1,'0000-00-00 00:00:00',0,'2021-12-23 18:47:45',131);

/*Table structure for table `pengguna_grup_menu` */

DROP TABLE IF EXISTS `pengguna_grup_menu`;

CREATE TABLE `pengguna_grup_menu` (
  `grup_id` int(11) NOT NULL AUTO_INCREMENT,
  `grup_peng_id` int(11) NOT NULL,
  `grup_menu_id` int(11) NOT NULL,
  `grup_is_deleted` int(4) NOT NULL DEFAULT 1,
  `grup_created_time` datetime DEFAULT NULL,
  `grup_created_by` int(11) DEFAULT NULL,
  `grup_updated_time` datetime DEFAULT NULL,
  `grup_updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`grup_id`)
) ENGINE=InnoDB AUTO_INCREMENT=290 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `pengguna_grup_menu` */

insert  into `pengguna_grup_menu`(`grup_id`,`grup_peng_id`,`grup_menu_id`,`grup_is_deleted`,`grup_created_time`,`grup_created_by`,`grup_updated_time`,`grup_updated_by`) values 
(268,131,42,1,'2021-12-08 17:44:57',131,NULL,NULL),
(269,131,43,1,'2021-12-08 17:45:01',131,NULL,NULL),
(270,131,44,1,'2021-12-08 17:45:05',131,NULL,NULL),
(271,131,46,1,'2021-12-08 17:45:07',131,NULL,NULL),
(272,131,47,1,'2021-12-08 17:45:08',131,NULL,NULL),
(273,131,48,1,'2021-12-08 17:45:10',131,NULL,NULL),
(274,131,49,1,'2021-12-08 17:45:13',131,NULL,NULL),
(275,131,50,1,'2021-12-08 17:45:14',131,NULL,NULL),
(276,131,51,1,'2021-12-08 17:45:16',131,NULL,NULL),
(277,131,52,1,'2021-12-08 17:45:18',131,NULL,NULL),
(278,131,53,1,'2021-12-08 17:45:20',131,NULL,NULL),
(279,131,54,1,'2021-12-08 17:45:23',131,NULL,NULL),
(280,131,55,1,'2021-12-08 17:45:25',131,NULL,NULL),
(281,131,56,1,'2021-12-08 17:45:27',131,NULL,NULL),
(282,131,57,1,'2021-12-08 17:45:29',131,NULL,NULL),
(283,131,58,1,'2021-12-08 17:45:30',131,NULL,NULL),
(284,131,59,1,'2021-12-08 17:45:33',131,NULL,NULL),
(285,131,60,1,'2021-12-08 17:45:35',131,NULL,NULL),
(286,131,61,1,'2021-12-08 17:45:36',131,NULL,NULL),
(287,131,62,1,'2021-12-08 17:45:38',131,NULL,NULL),
(288,131,63,1,'2021-12-09 04:58:07',131,NULL,NULL),
(289,131,64,1,'2021-12-09 06:15:18',131,NULL,NULL);

/*Table structure for table `penjualan` */

DROP TABLE IF EXISTS `penjualan`;

CREATE TABLE `penjualan` (
  `pnjl_id` int(11) NOT NULL AUTO_INCREMENT,
  `pnjl_kode` varchar(20) DEFAULT NULL,
  `pnjl_akun_id` int(11) DEFAULT NULL,
  `pnjl_tgl_jual` date DEFAULT NULL,
  `pnjl_grand_tot` double DEFAULT 0,
  `pnjl_created_by` int(11) DEFAULT NULL,
  `pnjl_created_time` datetime DEFAULT NULL,
  `pnjl_updated_by` int(11) DEFAULT NULL,
  `pnjl_updated_time` datetime DEFAULT NULL,
  `pnjl_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`pnjl_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `penjualan` */

insert  into `penjualan`(`pnjl_id`,`pnjl_kode`,`pnjl_akun_id`,`pnjl_tgl_jual`,`pnjl_grand_tot`,`pnjl_created_by`,`pnjl_created_time`,`pnjl_updated_by`,`pnjl_updated_time`,`pnjl_is_deleted`) values 
(1,'J2021120001',5,'2021-12-28',1296100,131,'2021-12-28 22:48:45',131,'2021-12-28 22:48:45','1');

/*Table structure for table `ref_pilihan` */

DROP TABLE IF EXISTS `ref_pilihan`;

CREATE TABLE `ref_pilihan` (
  `plhn_id` int(11) NOT NULL AUTO_INCREMENT,
  `plhn_kategori` varchar(100) NOT NULL,
  `plhn_nama` varchar(100) NOT NULL,
  `plhn_urutan` int(11) NOT NULL,
  `plhn_is_deleted` enum('0','1') NOT NULL DEFAULT '1',
  `plhn_created_time` datetime NOT NULL,
  `plhn_created_by` int(11) NOT NULL,
  `plhn_updated_time` datetime NOT NULL,
  `plhn_updated_by` int(11) NOT NULL,
  PRIMARY KEY (`plhn_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `ref_pilihan` */

insert  into `ref_pilihan`(`plhn_id`,`plhn_kategori`,`plhn_nama`,`plhn_urutan`,`plhn_is_deleted`,`plhn_created_time`,`plhn_created_by`,`plhn_updated_time`,`plhn_updated_by`) values 
(1,'kategori-saham','Sangat Baik',1,'1','2021-12-08 19:11:23',131,'0000-00-00 00:00:00',0),
(2,'kategori_saham','Baik',2,'1','2021-12-08 19:11:27',131,'0000-00-00 00:00:00',0),
(3,'kategori-saham','Cukup',3,'1','2021-12-08 19:11:29',131,'0000-00-00 00:00:00',0),
(4,'kategori-saham','Buruk',4,'1','2021-12-08 19:11:31',131,'0000-00-00 00:00:00',0),
(5,'sektor-saham','Telekomunikasi',1,'1','2021-12-08 19:11:33',131,'0000-00-00 00:00:00',0),
(6,'sektor-saham','Minerba',2,'1','2021-12-08 19:11:56',131,'0000-00-00 00:00:00',0),
(7,'sektor-saham','Bank',3,'0','2021-12-08 23:30:34',131,'2021-12-08 23:31:05',131);

/*Table structure for table `saham` */

DROP TABLE IF EXISTS `saham`;

CREATE TABLE `saham` (
  `sham_id` int(11) NOT NULL AUTO_INCREMENT,
  `sham_nama` varchar(100) NOT NULL,
  `sham_kode` varchar(10) NOT NULL,
  `sham_sektor` varchar(100) NOT NULL,
  `sham_seku_id` int(11) DEFAULT NULL,
  `sham_kinerja_kategori` varchar(100) NOT NULL,
  `sham_created_by` int(11) DEFAULT NULL,
  `sham_created_time` datetime DEFAULT NULL,
  `sham_updated_by` int(11) DEFAULT NULL,
  `sham_updated_time` datetime DEFAULT NULL,
  `sham_is_deleted` enum('0','1') DEFAULT '1',
  `sham_total_lot` double DEFAULT 0,
  `sham_harga_rata_rata` double DEFAULT 0,
  PRIMARY KEY (`sham_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `saham` */

insert  into `saham`(`sham_id`,`sham_nama`,`sham_kode`,`sham_sektor`,`sham_seku_id`,`sham_kinerja_kategori`,`sham_created_by`,`sham_created_time`,`sham_updated_by`,`sham_updated_time`,`sham_is_deleted`,`sham_total_lot`,`sham_harga_rata_rata`) values 
(1,'Telkom','TLKM','5',1,'1',1,'2021-12-06 08:35:55',131,'2021-12-08 13:47:13','1',0,0),
(2,'Metro Care','CARE','6',1,'1',1,'2021-12-06 08:47:31',131,'2021-12-08 13:47:04','1',0,0),
(3,'Sidomuncul','SIDO','6',2,'1',131,'2021-12-08 13:40:23',131,'2021-12-08 13:40:23','1',0,0);

/*Table structure for table `saldo_awal` */

DROP TABLE IF EXISTS `saldo_awal`;

CREATE TABLE `saldo_awal` (
  `swal_id` int(11) NOT NULL AUTO_INCREMENT,
  `swal_akun_id` int(11) DEFAULT NULL,
  `swal_tgl_saldo_awal` date DEFAULT NULL,
  `swal_jumlah_saldo` double DEFAULT 0,
  `swal_created_time` datetime DEFAULT NULL,
  `swal_created_by` int(11) DEFAULT NULL,
  `swal_updated_by` int(11) DEFAULT NULL,
  `swal_updated_time` datetime DEFAULT NULL,
  `swal_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`swal_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `saldo_awal` */

insert  into `saldo_awal`(`swal_id`,`swal_akun_id`,`swal_tgl_saldo_awal`,`swal_jumlah_saldo`,`swal_created_time`,`swal_created_by`,`swal_updated_by`,`swal_updated_time`,`swal_is_deleted`) values 
(1,5,'2021-12-28',30000000,'2021-12-28 16:56:36',131,131,'2021-12-28 16:56:36','1');

/*Table structure for table `saldo_awal_saham` */

DROP TABLE IF EXISTS `saldo_awal_saham`;

CREATE TABLE `saldo_awal_saham` (
  `shal_id` int(11) NOT NULL AUTO_INCREMENT,
  `shal_akun_id` int(11) DEFAULT NULL,
  `shal_sham_id` int(11) DEFAULT NULL,
  `shal_tgl_saldo_awal` date DEFAULT NULL,
  `shal_jumlah_lot` double DEFAULT 0,
  `shal_jumlah_shares` double DEFAULT 0,
  `shal_rata_rata_harga` double DEFAULT 0,
  `shal_total` double DEFAULT 0,
  `shal_created_time` datetime DEFAULT NULL,
  `shal_created_by` int(11) DEFAULT NULL,
  `shal_updated_by` int(11) DEFAULT NULL,
  `shal_updated_time` datetime DEFAULT NULL,
  `shal_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`shal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `saldo_awal_saham` */

/*Table structure for table `sekuritas` */

DROP TABLE IF EXISTS `sekuritas`;

CREATE TABLE `sekuritas` (
  `seku_id` int(11) NOT NULL AUTO_INCREMENT,
  `seku_nama` varchar(100) NOT NULL,
  `seku_no_telp` varchar(20) NOT NULL,
  `seku_fax` varchar(20) NOT NULL,
  `seku_email` varchar(30) NOT NULL,
  `seku_alamat` text NOT NULL,
  `seku_broker_fee_pembelian` double NOT NULL DEFAULT 0,
  `seku_broker_fee_penjualan` double NOT NULL DEFAULT 0,
  `seku_created_by` int(11) DEFAULT NULL,
  `seku_created_time` datetime DEFAULT NULL,
  `seku_updated_by` int(11) DEFAULT NULL,
  `seku_updated_time` datetime DEFAULT NULL,
  `seku_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`seku_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

/*Data for the table `sekuritas` */

insert  into `sekuritas`(`seku_id`,`seku_nama`,`seku_no_telp`,`seku_fax`,`seku_email`,`seku_alamat`,`seku_broker_fee_pembelian`,`seku_broker_fee_penjualan`,`seku_created_by`,`seku_created_time`,`seku_updated_by`,`seku_updated_time`,`seku_is_deleted`) values 
(1,'Ajaib','-','-','-','-',0.8,0.3,1,'2021-12-06 07:56:08',131,'2021-12-23 19:00:04','1'),
(2,'BNI Sekuritas','-','-','-','-',0.1,0.7,1,'2021-12-06 07:56:41',131,'2021-12-23 19:07:27','1'),
(3,'Mandiri Sekuritas','-','-','-','-',0,0.9,1,'2021-12-06 08:49:18',131,'2021-12-23 20:19:36','1'),
(5,'Binomo','123456789123','123456789123','asd','asd',3,2,131,'2021-12-08 22:50:20',131,'2021-12-08 22:50:20','1');

/*Table structure for table `setoran` */

DROP TABLE IF EXISTS `setoran`;

CREATE TABLE `setoran` (
  `stor_id` int(11) NOT NULL AUTO_INCREMENT,
  `stor_kode` varchar(20) DEFAULT NULL,
  `stor_tgl` date DEFAULT NULL,
  `stor_akun_id` int(11) DEFAULT NULL,
  `stor_bank_id` int(11) DEFAULT NULL,
  `stor_no_ref` varchar(20) DEFAULT NULL,
  `stor_nominal` double DEFAULT 0,
  `stor_created_by` int(11) DEFAULT NULL,
  `stor_created_time` datetime DEFAULT NULL,
  `stor_updated_by` int(11) DEFAULT NULL,
  `stor_updated_time` datetime DEFAULT NULL,
  `stor_is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`stor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `setoran` */

insert  into `setoran`(`stor_id`,`stor_kode`,`stor_tgl`,`stor_akun_id`,`stor_bank_id`,`stor_no_ref`,`stor_nominal`,`stor_created_by`,`stor_created_time`,`stor_updated_by`,`stor_updated_time`,`stor_is_deleted`) values 
(2,'S2021120001','2021-12-28',5,2,NULL,10000000,131,'2021-12-28 17:08:45',131,'2021-12-28 17:08:45','1'),
(3,'S2021120002','2021-12-28',5,2,NULL,30000000,131,'2021-12-28 19:49:13',131,'2021-12-28 19:49:13','1'),
(4,'S2021120003','2021-12-28',5,3,NULL,20000000,131,'2021-12-28 21:10:45',131,'2021-12-28 21:10:45','1');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
